 //
//  main.m
//  VCab
//
//  Created by HimAnshu on 01/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
//#import "TIMERUIApplication.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
