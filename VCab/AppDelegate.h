//
//  AppDelegate.h
//  VCab
//
//  Created by HimAnshu on 01/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import "defines.h"
#import <CoreLocation/CoreLocation.h>

#import "ProfileViewController.h"
#import "LoginViewController.h"
#import "APIManager.h"
#import "Database.h"

typedef enum RUNNIG_RIDE_STATUS_UPDATE{
    RIDE_STATUS_SEND_REQUEST,//0
    RIDE_STATUS_ACCEPTED,//1
    RIDE_STATUS_ARRIVAL,//2
    RIDE_STATUS_ON_GOING,//3
    RIDE_STATUS_CANCELED,//4
    RIDE_STATUS_FINISHED//5
}RunningRideStatusUpdate;


@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) UIWindow *window;
@property CLLocationManager * locationManager;
@property BOOL isPresentedGPSView;
@property (strong, nonatomic) UINavigationController *globalNavigation;
@property APIManager * apiManager;
@property NSInteger selectedIndex;



-(BOOL)CheckLocationPermission;
-(void)updateDeviceTokenToServer;
- (void)logoutTheUserFromAppWithAlert:(BOOL)shouldShowAlert;
-(void)clearPrevousTripDataFromApp;
-(NSString *)countEstimatedCostWithKM:(float)arrivalKM time:(float)arrivalTime minCharges:(NSString *)minCharges basefare:(NSString *)basefare minutePrice:(NSString *)minutePrice kmPrice:(NSString *)kmPrice;


-(void)updatePromocodeListing;
-(void)updateCarCategoriesListing;
-(void)updateUserProfileDetails;

// Store User Current Location
@property CLLocationCoordinate2D userLocation;
@property (nonatomic, retain) NSMutableArray *arrLanguage;
@property Database *db;

@property NSMutableArray * arrayNearByCars;
@property (nonatomic, retain) NSString *strLanguageID;
@property(nonatomic,retain)NSMutableArray *arrStatusDetails;

//Local preferances
-(void)clearUserDefaults;
@property NSString * loggedUserId;
@property BOOL isUserLoggedIn;
@property BOOL isInitialProfileSetup;
@property NSString * tokenString;
@property NSMutableDictionary * dictUserInfo;
@property NSArray * arrayCarCategories;
@property NSDictionary * dictDefaultBrand;
@property NSString * strDeviceToken;
@property NSDictionary * dictHomeInfo;
@property NSDictionary * dictWorkInfo;
@property NSArray * arrayPaymentTypes;
@property NSDictionary * dictDefaultPayment;
@property NSMutableArray * arrayPromoCodes;
@property NSDictionary * dictLastSelectedLocation;
@property NSDate *sendRequestDate;
@property NSString * currencySymbol;
@property NSString * decimapPoints;
@property NSString * adminEmail;
@property NSString * adminMobile;

//Handle the running trip
@property BOOL isTripRunning; //NSUserDefaults
@property RunningRideStatusUpdate tripStatus;//NSUserDefaults
@property NSString * runningTripId;//NSUserDefaults
@property NSString * tripDriverId;//NSUserDefaults

@property CLLocationCoordinate2D tripStartLocation;//NSUserDefaults
@property NSDictionary * dictTripStartAddress;
@property CLLocationCoordinate2D tripEndLocation;//NSUserDefaults
@property NSDictionary * dictTripEndAddress;

@property float tripDuration;
@property float tripDistance;

@property float driverDuration;
@property float driverDistance;

@property NSDictionary * dictTripDriverDetails;
@property GMSMarker * markerDriverLocation;
-(void)updateDriverMarkerWithAnimation;
// Vishal D
@property (nonatomic,retain)NSTimer *timer1;
@property (nonatomic,retain)NSTimer *timer2;

@property (nonatomic,retain)NSString *strLatitude;
@property (nonatomic,retain)NSString *strLongitude;

-(void)getCurrentLocation;

@end

