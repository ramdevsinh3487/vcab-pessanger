//
//  LeftViewController.m
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import "LeftViewController.h"
#import "AppDelegate.h"
#import "LeftViewCell.h"
#import "MainViewController.h"

@interface LeftViewController ()

@property (strong, nonatomic) NSArray *arrayMenuOptions;
@property (strong, nonatomic) NSMutableArray *arrForActiveImages;
@property (strong, nonatomic) NSMutableArray *arrForInActiveImages;


@end

@implementation LeftViewController

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
        
        self.arrayMenuOptions = @[
                                  @{@"title":(!SET_ALERT_TEXT(@"home")) ? @"Home" : SET_ALERT_TEXT(@"home"),
                                    @"image":[UIImage imageNamed:@"LMHomeIcon"]
                                    },
                                  @{@"title":(!SET_ALERT_TEXT(@"payments")) ? @"Payments" : SET_ALERT_TEXT(@"payments"),
                                    @"image":[UIImage imageNamed:@"LMPaymentIcon"]
                                    },
                                  @{@"title":(!SET_ALERT_TEXT(@"your_trip")) ? @"Your Trips" :SET_ALERT_TEXT(@"your_trip"),
                                    @"image":[UIImage imageNamed:@"LMTripsIcon"]
                                    },
                                  @{@"title":(!SET_ALERT_TEXT(@"promocodes")) ? @"Promocodes" :SET_ALERT_TEXT(@"promocodes"),
                                    @"image":[UIImage imageNamed:@"LMPromocodeIcon"]
                                    },
                                  @{@"title":(!SET_ALERT_TEXT(@"menu_help")) ? @"Want Help ?" : SET_ALERT_TEXT(@"menu_help"),
                                    @"image":[UIImage imageNamed:@"LMHelpIcon"]
                                    },
                                  @{@"title":(!SET_ALERT_TEXT(@"menu_settings")) ? @"Settings" : SET_ALERT_TEXT(@"menu_settings"),
                                    @"image":[UIImage imageNamed:@"LMSettingsIcon"]
                                    },
                                  @{@"title":(!SET_ALERT_TEXT(@"menu_legal")) ? @"Legal" : SET_ALERT_TEXT(@"menu_legal"),
                                    @"image":[UIImage imageNamed:@"LMLegalIcon"]
                                    },];

        APP_DELEGATE.selectedIndex = 0;
        [self.tableView registerClass:[LeftViewCell class] forCellReuseIdentifier:@"cell"];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.contentInset = UIEdgeInsetsMake(44.f, 0.f, 44.f, 0.f);
        self.tableView.showsVerticalScrollIndicator = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationForReloadUserInfo:) name:@"reloadMenuTable" object:nil];
    
    imgView = [[UIImageView alloc] init];
    lblName = [[UILabel alloc] init];
    lblMobile = [[UILabel alloc] init];
    return self;
}

-(IBAction)profileImageClick:(id)sender
{
    NSInteger lastIndex = APP_DELEGATE.selectedIndex;
    APP_DELEGATE.selectedIndex = 0;
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:lastIndex inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^(void)
     {
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
         ProfileViewController *obj = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
         obj.isModeInitialUpdate = false;
         obj.canGoBack = true;
         [APP_DELEGATE.globalNavigation pushViewController:obj animated:true];
     }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    imgView.image = chosenImage;
    imgView.layer.cornerRadius = imgView.frame.size.width / 2;
    imgView.layer.masksToBounds = YES;
    imgView.layer.borderColor = [UIColor whiteColor].CGColor;
    imgView.layer.borderWidth = 2.0;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)notificationForReloadUserInfo:(NSNotification *)notif {
    [self refreshUserDetails];
}

-(void)refreshUserDetails{
    NSDictionary * dictUserData = APP_DELEGATE.dictUserInfo;
    
    lblName.text = [NSString stringWithFormat:@"%@ %@",dictUserData[@"first_name"], dictUserData[@"last_name"]];
    lblMobile.text = [NSString stringWithFormat:@"%@ %@",dictUserData[@"country_code"], dictUserData[@"mobile_no"]];
    
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgView.frame.size.width, imgView.frame.size.height)];
    [imgView addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    [imgView sd_setImageWithURL:[NSURL URLWithString:APP_DELEGATE.dictUserInfo[@"user_image"]] placeholderImage:[UIImage imageNamed:@"PHAddProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
    }];
    
    [self.tableView reloadData];
}

#pragma mark - UITableView DataSource

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH * (IS_IPHONE ? 0.70 : 0.54), IS_IPHONE ? 190 : 250)];
    viewHeader.backgroundColor = [UIColor clearColor];
    
    imgView.frame = CGRectMake(viewHeader.frame.size.width / 2 - (IS_IPHONE ? 70 : 110)/2, 10, IS_IPHONE ? 70 : 110, IS_IPHONE ? 70 : 110);
    
    
//    if (GET_DEFAULT_VALUE(@"user_image")) {
//        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgView.frame.size.width, imgView.frame.size.height)];
//        [imgView addSubview:activity_indicator];
//        [activity_indicator setColor:[UIColor whiteColor]];
//        [activity_indicator startAnimating];
//        
//////        [imgView setImageWithURL:[NSURL URLWithString:GET_DEFAULT_VALUE(@"user_image")] placeholderImage:[UIImage imageNamed:SET_VIEW_NAME(@"upload-image")] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
////            [activity_indicator stopAnimating];
////        }];
//    }
//    else {
//        imgView.image = [UIImage imageNamed:@"chat-profile"];
//    }
    
    imgView.backgroundColor = [UIColor clearColor];
    imgView.layer.cornerRadius = imgView.frame.size.width / 2;
    imgView.layer.borderColor = [UIColor whiteColor].CGColor;
    imgView.layer.borderWidth = 2.0;
    imgView.layer.masksToBounds = YES;
    [viewHeader addSubview:imgView];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = imgView.frame;
    [btn addTarget:self action:@selector(profileImageClick:) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btn];
    
    lblName.frame = CGRectMake(0, IS_IPHONE ? 90 : 130, SCREEN_WIDTH * (IS_IPHONE ? 0.70 : 0.54), IS_IPHONE ? 21 : 40);
    lblName.textColor = [UIColor whiteColor];
    lblName.textAlignment = NSTextAlignmentCenter;
    lblName.font = [UIFont fontWithName:@"Avenir Next" size:IS_IPHONE ? 14.0 : 18.0];
    [viewHeader addSubview:lblName];
    
    lblMobile.frame = CGRectMake(0, IS_IPHONE ? 115 : 170, SCREEN_WIDTH * (IS_IPHONE ? 0.70 : 0.54), IS_IPHONE ? 21 : 40);
    lblMobile.textColor = [UIColor lightGrayColor];
    lblMobile.textAlignment = NSTextAlignmentCenter;
    lblMobile.font = [UIFont fontWithName:@"Avenir Next" size:IS_IPHONE ? 12.0 : 16.0];
    [viewHeader addSubview:lblMobile];
    return viewHeader;
}




-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return IS_IPHONE ? 150 : 210;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenuOptions.count;
}

-(IBAction)btnLogoutClick:(id)sender
{
//    [APP_DELEGATE logoutUser];
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"LeftViewCell";
    
    LeftViewCell *cell = (LeftViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LeftViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    cell.imgView.frame = CGRectMake(35, IS_IPHONE ? 7.5 : 10 , IS_IPHONE ? 35 : 45, IS_IPHONE ? 35 : 45);
    cell.lbltitle.frame = CGRectMake(cell.imgView.frame.origin.x + cell.imgView.frame.size.width + (IS_IPHONE ? 12 : 18), 0, cell.lbltitle.frame.size.width, IS_IPHONE ? 50.0 : 60.0);
    
    cell.lbltitle.textAlignment = NSTextAlignmentLeft;
    
    cell.lbltitle.text = self.arrayMenuOptions[indexPath.row][@"title"];
    
    cell.lbltitle.font = [UIFont fontWithName:@"Avenir Next" size:IS_IPHONE ? 20.0 : 30.0];
    
    cell.imgView.image = self.arrayMenuOptions[indexPath.row][@"image"];
    
    if (APP_DELEGATE.selectedIndex == indexPath.row) {
         cell.lbltitle.textColor = [UIColor whiteColor];
    } else {
        cell.lbltitle.textColor = [UIColor grayColor];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE) return 50;
    else return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger lastIndex = APP_DELEGATE.selectedIndex;
    APP_DELEGATE.selectedIndex = indexPath.row;
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^(void)
     {
     }];
    
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:lastIndex inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView endUpdates];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    if (indexPath.row == 1){
        PaymentViewController *paymentView = (PaymentViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
        [APP_DELEGATE.globalNavigation pushViewController:paymentView animated:true];
    } else if (indexPath.row == 2){
        YourTripsViewController *tripsView = (YourTripsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"YourTripsViewController"];
        [APP_DELEGATE.globalNavigation pushViewController:tripsView animated:true];
    }
    else if (indexPath.row == 3){
        PromocodeViewController * promocodeView = [storyboard instantiateViewControllerWithIdentifier:@"PromocodeViewController"];
        [APP_DELEGATE.globalNavigation pushViewController:promocodeView animated:true];
    }else if (indexPath.row == 4){
        LoadURLViewController * loadURLView = [storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
        loadURLView.pageTitle = SET_ALERT_TEXT(@"help");
        loadURLView.isModeLoadString = true;
        loadURLView.cmsAPIMode = CMS_API_HELP;
        loadURLView.isPushed = true;
        [APP_DELEGATE.globalNavigation pushViewController:loadURLView animated:true];
    } else if (indexPath.row == 5){
        SettingsViewController * settingsView = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
        [APP_DELEGATE.globalNavigation pushViewController:settingsView animated:true];
    } else if (indexPath.row == 6){
        LegalInfoViewController * legalInfoView = [storyboard instantiateViewControllerWithIdentifier:@"LegalInfoViewController"];
        [APP_DELEGATE.globalNavigation pushViewController:legalInfoView animated:true];
    } else {
        //Home
    }
}

@end
