//
//  LeftViewController.h
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

#import "ProfileViewController.h"
#import "YourTripsViewController.h"
#import "FreeRideViewController.h"
#import "LoadURLViewController.h"
#import "SettingsViewController.h"
#import "PaymentViewController.h"
#import "LegalInfoViewController.h"
#import "PromocodeViewController.h"

@interface LeftViewController : UITableViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImageView *imgView;
    
    UILabel *lblName;
    UILabel *lblMobile;
    
}
@property (strong, nonatomic) UIColor *tintColor;
@end
