//
//  defines.h
//  VCab
//
//  Created by Himanshu on 05/01/2017.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AppDelegate.h"

#ifndef VCab_defines_h
#define VCab_defines_h

#define GOOGLE_API_KEY @"AIzaSyCes7i-xDj39u-kN9Plb5Jkokd1vUzE_kk"
#define GOOGLE_STATIC_MAP_API_KEY @"AIzaSyCJDX7nRpMbTESlVnXbcQuPyDPAjFL-8YE"

#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

//for iphone 4 or 5
#define IS_IPHONE ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
#define IS_HEIGHT_GTE_568 ([[UIScreen mainScreen ] bounds].size.height >= 568.0f)
#define IS_IPHONE_5 ( IS_IPHONE && [[UIScreen mainScreen ] bounds].size.height == 568.0f )
#define IS_iOS6  ([[[UIDevice currentDevice] systemVersion] floatValue]==6.0)
#define IS_iOS7 ([[[UIDevice currentDevice] systemVersion] floatValue]==7.0)
#define IS_iOS9 ([[[UIDevice currentDevice] systemVersion] floatValue]==9.0)
#define IS_iOS10 ([[[UIDevice currentDevice] systemVersion] floatValue]>=10.0)

#define IS_IPHONE_5_OR_LESS ([[UIScreen mainScreen ] bounds].size.height <= 568.0f)
#define IS_IPHONE_4_OR_LESS ([[UIScreen mainScreen ] bounds].size.height < 568.0f)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6_GTE_667 (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_6_GTE (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)
#define IS_IPHONE_6P_GTE (IS_IPHONE && SCREEN_MAX_LENGTH >= 736.0)
#define SET_VIEW_NAME(strName) IS_IPHONE?strName:[NSString stringWithFormat:@"%@_iPad",strName]

#define PUSH_VIEW_CONTROLLER(viewName) BOOL isFound = NO;for (UIViewController *viewC in self.navigationController.viewControllers) {if ([viewC isKindOfClass:[viewName class]]) {isFound = YES;[self.navigationController popToViewController:viewC animated:YES];}}if (isFound == NO) {viewName *mviewName = [[viewName alloc] initWithNibName:SET_VIEW_NAME(NSStringFromClass([viewName class])) bundle:nil];[self.navigationController pushViewController:mviewName animated:YES];}

#define GET_DEFAULT_VALUE(string_value) [[NSUserDefaults standardUserDefaults] valueForKey:string_value]

#define SET_DEFAULT_VALUE(object,string_value) [[NSUserDefaults standardUserDefaults] setObject:object forKey:string_value];[[NSUserDefaults standardUserDefaults] synchronize];

#define INCRESE_FONT_SIZE IS_IPHONE_5 ? 0 : (IS_IPHONE_6P_GTE || !IS_IPHONE ? 2 : 1)

//#define START_HUD [[Singleton sharedInstance] showGlobalHUD];
//#define STOP_HUD [[Singleton sharedInstance] dismissGlobalHUD];;

#define START_HUD [[Singleton sharedInstance] shouldShowSVProgressHud:true];
#define STOP_HUD [[Singleton sharedInstance] shouldShowSVProgressHud:false];

#define SHOW_NO_DATA_LABEL [APP_DELEGATE showNoDataLabel];
#define HIDE_NO_DATA_LABEL [APP_DELEGATE hideNoDataLabel];

#define SHOW_TABBAR [APP_DELEGATE.tabBarController.tabBar setHidden:NO];
#define HIDE_TABBAR [APP_DELEGATE.tabBarController.tabBar setHidden:YES];

#define CHECK_INTERNET if (![APP_DELEGATE CheckInternet]){ STOP_HUD; return; }

#define SHOW_ALERT_WITH_CAUTION(message) [[Singleton sharedInstance] showAlertViewWithMessage:message];

#define SHOW_ALERT_WITH_SUCCESS(message) [[Singleton sharedInstance] showAlertViewWithMessageSuccess:message];

#define CHECK_VALID_CONTACT_NUMBER(textField,string,range) [[Singleton sharedInstance] checkValidContactNumber:textField string:string range:range];

#define SET_ACTIVE_VC(vc) APP_DELEGATE.strActiveViewController = NSStringFromClass([vc class]); NSLog(@"Active View Controller - %@",APP_DELEGATE.strActiveViewController);

#define kMainViewController (MainViewController *)[UIApplication sharedApplication].delegate.window.rootViewController

#define SHOW_MENU [kMainViewController showLeftViewAnimated:YES completionHandler:^{}];


#define DATE_FORMAT @"dd-MM-yyyy"

#define TIME_FORMAT @"hh:mm a"
#define GET_Language_Value(key) [[APP_DELEGATE.arrLanguage objectAtIndex:[[APP_DELEGATE.arrLanguage valueForKey:@"field_name"] indexOfObject:key]] valueForKey:[NSString stringWithFormat:@"field_%@",[APP_DELEGATE.strLanguageID isEqualToString:@"1"] ? @"EN" : @"AR"]]

#define SHOW_Alert_With_Language_Value(key) @try{[StaticClass showAlertControllerwithTitle:SET_ALERT_TEXT(key)];} @catch (NSException *exception){}

#define SET_ALERT_TEXT(key) [Singleton setAlertText:key]


#define CHECK_USER_TYPE [APP_DELEGATE.strUserType isEqualToString:@"3"]

#define HEADER_HEIGHT 64

#define PASSWORD_LENGTH 8

#define Password_Alert [NSString stringWithFormat:@"Password must be %d character long", PASSWORD_LENGTH]

#define PULL_TO_REFRESH_COLOR [UIColor lightGrayColor]

#define NO_DATA_FOUND @"No records to display"
#define GREEN_COLOR [[UIColor blackColor] colorWithAlphaComponent:0.5]

#define CUSTOM_NO_DATA_CELL UITableViewCell *cell = [[UITableViewCell alloc] init]; cell.textLabel.textAlignment = NSTextAlignmentCenter; cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:IS_IPHONE ? 12.0 : 14.0]; cell.textLabel.textColor = GREEN_COLOR; cell.textLabel.text = NO_DATA_FOUND; cell.selectionStyle = UITableViewCellSelectionStyleNone; tableView.separatorStyle = UITableViewCellSeparatorStyleNone; return cell;

#define AlertTitle @"VCab"
#define AlertInternet @"Please check your internet connection"
#define AlertNoresult @"No data found"
#define AlertServerError @"Cannot connect to server please try again later"

#define AppFont_Dosis_Medium @"Dosis-Medium"
#define AppFont_Dosis_Regular @"Dosis-Regular"
#define AppFont_OpenSans_Light @"OpenSans-Light"
#define AppFont_OpenSans_Regular @"OpenSans"
#define AppFont_OpenSans_Semibold @"OpenSans-Semibold"
#define AppFont_Roboto_Thin @"Roboto-Thin"

#define backAction() [self.navigationController popViewControllerAnimated:YES];
#endif
