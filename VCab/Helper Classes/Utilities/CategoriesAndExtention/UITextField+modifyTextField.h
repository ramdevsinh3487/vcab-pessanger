//
//  UITextField+modifyTextField.h
//  PeopleScience
//
//  Created by HimAnshu on 07/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (modifyTextField)

//@property (nonatomic) IBInspectable UIColor *placeholderColor;


-(void) setPadding : (float) width;
-(void) setRightPaddingDownArrow;
-(void) setRightPaddingRightArrow;
-(void) setRightPaddingCalender;
- (void) setImageWithArrowInLeftViewWithImage : (NSString *)strImageName;
- (void) setLeftViewWithImage : (NSString *)strImageName;
-(void)setLeftImageToTextField:(UIImage *)imageToSet;

//-(void)setplaceholderColor:(UIColor *)placeholderColor;

@end
