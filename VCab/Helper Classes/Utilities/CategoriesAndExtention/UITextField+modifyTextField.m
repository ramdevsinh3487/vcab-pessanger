//
//  UITextField+modifyTextField.m
//  PeopleScience
//
//  Created by HimAnshu on 07/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "UITextField+modifyTextField.h"

@implementation UITextField (modifyTextField)
//@dynamic placeholderColor;


-(void) setPadding : (float) width
{
    UIView *viewTemp = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, self.frame.size.height)];
    viewTemp.userInteractionEnabled = NO;
    self.leftView = viewTemp;
    self.leftViewMode = UITextFieldViewModeAlways;
}


-(void) setRightPaddingDownArrow
{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown-arrow"]];
    UIView *viewTemp = [[UIView alloc] initWithFrame:CGRectMake((IS_IPHONE_5 ? 0 : 4), 0, imgView.image.size.width * (IS_IPHONE_5 ? 1 : 3), self.frame.size.height)];
    [viewTemp addSubview:imgView];
    imgView.center = CGPointMake(viewTemp.center.x, viewTemp.center.y + 5);
    viewTemp.userInteractionEnabled = NO;
    self.rightView = viewTemp;
    self.rightViewMode = UITextFieldViewModeAlways;
}


-(void) setRightPaddingRightArrow
{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right-arrow"]];
    UIView *viewTemp = [[UIView alloc] initWithFrame:CGRectMake((IS_IPHONE_5 ? 0 : 4), 0, imgView.image.size.width * (IS_IPHONE_5 ? 1 : 3), self.frame.size.height)];
    [viewTemp addSubview:imgView];
    imgView.center = viewTemp.center;
    viewTemp.userInteractionEnabled = NO;
    self.rightView = viewTemp;
    self.rightViewMode = UITextFieldViewModeAlways;
}

-(void) setRightPaddingCalender
{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendar"]];
    UIView *viewTemp = [[UIView alloc] initWithFrame:CGRectMake((IS_IPHONE_5 ? 0 : 4), 0, imgView.image.size.width * (IS_IPHONE_5 ? 1 : 3), self.frame.size.height)];
    [viewTemp addSubview:imgView];
    imgView.center = viewTemp.center;
    viewTemp.userInteractionEnabled = NO;
    self.rightView = viewTemp;
    self.rightViewMode = UITextFieldViewModeAlways;
}


- (void) setImageWithArrowInLeftViewWithImage : (NSString *)strImageName
{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:strImageName]];
    
    UIImageView *imgArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"field-arrow"]];
    
    UIView *viewTemp = [[UIView alloc] initWithFrame:CGRectMake(4, 0, imgView.image.size.width + 5 + imgArrow.frame.size.width + (IS_IPHONE_5 ? 5 : 15), self.frame.size.height)];
    [viewTemp addSubview:imgView];
    [viewTemp addSubview:imgArrow];
    
    imgView.frame = CGRectMake(0, 0, imgView.frame.size.width, imgView.frame.size.height);
    imgView.center = CGPointMake(imgView.center.x, viewTemp.center.y);
    
    imgArrow.frame = CGRectMake(imgView.frame.size.width + imgView.frame.origin.x + 5, 0, imgArrow.frame.size.width, imgArrow.frame.size.height);
    imgArrow.center = CGPointMake(imgArrow.center.x, viewTemp.center.y);
    
    viewTemp.userInteractionEnabled = NO;
    self.leftView = viewTemp;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void)setLeftImageToTextField:(UIImage *)imageToSet{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:imageToSet];
    imgView.frame = CGRectMake(0, 0, 25, 25);
    imgView.layer.cornerRadius = imgView.frame.size.height / 2;
    imgView.layer.masksToBounds = true;
    
    UIImageView *imgArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"field-arrow"]];
    
    UIView *viewTemp = [[UIView alloc] initWithFrame:CGRectMake(4, 0, self.frame.size.height + 5 + imgArrow.frame.size.width + (IS_IPHONE_5 ? 5 : 15), self.frame.size.height)];
    [viewTemp addSubview:imgView];
    [viewTemp addSubview:imgArrow];
    
    imgView.frame = CGRectMake(0, 0, imgView.frame.size.width, imgView.frame.size.height);
    imgView.center = CGPointMake(imgView.center.x, viewTemp.center.y);
    
    imgArrow.frame = CGRectMake(imgView.frame.size.width + imgView.frame.origin.x + 5, 0, imgArrow.frame.size.width, imgArrow.frame.size.height);
    imgArrow.center = CGPointMake(imgArrow.center.x, viewTemp.center.y);
    
    viewTemp.userInteractionEnabled = NO;
    self.leftView = viewTemp;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void) setLeftViewWithImage : (NSString *)strImageName
{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:strImageName]];
    
    UIImage *image = [UIImage imageNamed:@"password"];
    
    float width = imgView.image.size.width < image.size.width ? image.size.width : imgView.image.size.width;
    
    UIView *viewTemp = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width * 3, self.frame.size.height)];
    [viewTemp addSubview:imgView];
    
    imgView.frame = CGRectMake(0, 0, imgView.frame.size.width, imgView.frame.size.height);
    imgView.center = CGPointMake(viewTemp.center.x, viewTemp.center.y + 5);
    
    viewTemp.userInteractionEnabled = NO;
    self.leftView = viewTemp;
    self.leftViewMode = UITextFieldViewModeAlways;
}


//-(void)setplaceholderColor:(UIColor *)placeholderColor{
//    
//    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholderText attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//}


@end
