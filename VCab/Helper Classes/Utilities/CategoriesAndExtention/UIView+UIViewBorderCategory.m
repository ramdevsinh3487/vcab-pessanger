//
//  AppDelegate.h
//  DeeraCofee
//
//  Created by Naman Vaishnav on 23/01/17.
//  Copyright © 2017 Vrinsoft. All rights reserved.
//


#import "UIView+UIViewBorderCategory.h"

@implementation UIView (UIViewBorderCategory)
@dynamic borderColor,borderWidth,cornerRadius,shadowColor;


//- (void) setcolor : (UIColor *)border : (CGFloat) bWidth : (CGFloat) CRadius{
//    
//    self.layer.borderColor = border.CGColor;
//    self.layer.borderWidth = bWidth;
//    self.layer.cornerRadius = CRadius;
//    
//}
- (void) setBorderColor :(UIColor *)border borderWidth : (CGFloat)bWidth boarderRadius : (CGFloat)cRadius{
    
    self.layer.borderColor = border.CGColor;
    self.layer.borderWidth = bWidth;
    self.layer.cornerRadius = cRadius;
    
}
//- (void) setBorderColorForValidation : (UIColor *)borderColor{
//    self.layer.borderColor = borderColor.CGColor;
//}

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

- (void)setShadowColor:(UIColor *)shadowColor{
    self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 5.0f;
    self.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    self.layer.masksToBounds = NO;
    
}


-(void) setWindow:(UIWindow * _Nullable)window{
    
}
- (void)awakeFromNib{
    [super awakeFromNib];
}
@end
