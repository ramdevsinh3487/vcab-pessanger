//
//  Constants.h
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#define DEBUG_MODE true

#pragma mark - NSNotificationCorner
extern NSString * const NOTI_LOCATION_UPDATES;
extern NSString * const NOTI_RUNNUNG_RIDE_STATUS_CHANGED;
extern NSString * const NOTI_CLEAR_HOME_MAP;
extern NSString * const NOTI_UPDATE_CURRENT_LOCATION_FORM_PUSH;
extern NSString * const NOTI_CLOSE_PLACE_PICKER;
extern NSString * const NOTI_FILL_FAVOURITE_PLACE;
extern NSString * const NOTI_APP_STATUS_CHANGED;
extern NSString * const NOTI_PROMOCODE_RESPONCE;
extern NSString * const NOTI_NEARBY_USERS_UPDATED;





#pragma mark - LocalPreferanceCorner
extern NSString * const IS_LOGGED_IN;
extern NSString * const LOGGED_USER_ID;
extern NSString * const IS_INITIAL_PROFILE_SETUP;
extern NSString * const USER_INFO;
extern NSString * const TOKEN_STRING;
extern NSString * const CAR_CATEGORIES;
extern NSString * const DEFAULT_BRAND_ID;
extern NSString * const DEVICE_TOKEN;
extern NSString * const CANCEL_COUNTS;
extern NSString * const HOME_INFO;
extern NSString * const WORK_INFO;
extern NSString * const PAYMENT_TYPES;
extern NSString * const DEFAULT_PAYMENT;
extern NSString * const CURRENY_SYMBOL;
extern NSString * const DECIMAL_POINT;
extern NSString * const ADMIN_EMAIL;
extern NSString * const ADMIN_MOBILE;
extern NSString * const LAST_SELECTED_LOCATION;
extern NSString * const SEND_REQUEST_DATE;

//RunnignTripDetails
extern NSString * const IS_TRIP_RUNNING;
extern NSString * const TRIP_ID;
extern NSString * const TRIP_DRIVER_ID;
extern NSString * const TRIP_STATUS;
extern NSString * const TRIP_START_LOCATION;
extern NSString * const TRIP_START_ADDRESS;
extern NSString * const TRIP_END_LOCATION;
extern NSString * const TRIP_END_ADDRESS;
extern NSString * const SELECTED_PROMOCODE;

#pragma mark - APICorner
extern NSString *const BASE_URL;
extern NSString * const API_LOGIN_REGISTER;
extern NSString * const API_VERIFY_OTP;
extern NSString * const API_UPDATE_PROFILE;
extern NSString * const API_CAR_CATEGORIES;
extern NSString * const API_UPDATE_TOKEN;
extern NSString * const API_UPDATE_CURRENT_LOCATION;
extern NSString * const API_SEND_REQUEST;
extern NSString * const API_CANCEL_REQUEST;
extern NSString * const API_CMS_STRINGS;
extern NSString * const API_TRIPS_HISTORY;
extern NSString * const API_TRIP_DETAILS;
extern NSString * const API_PROMOCODES;
extern NSString * const API_CONFIRM_RIDE_STATUS;
extern NSString * const API_UPDATE_RIDE_DESTINATION;
extern NSString * const API_RATE_YOUR_DRIVER;
extern NSString * const API_DECLINE_RIDE;
extern NSString * const API_USER_PROFILE;


extern NSString * const API_CUSTOMER_CANCEL_FIRST_TIME;




