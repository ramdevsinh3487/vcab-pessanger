//
//  Constants.m
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Constants.h"

#pragma mark - NSNotificationCorner
NSString * const NOTI_LOCATION_UPDATES = @"NOTI_LOCATION_UPDATES";
NSString * const NOTI_RUNNUNG_RIDE_STATUS_CHANGED = @"NOTI_RUNNUNG_RIDE_STATUS_CHANGED";
NSString * const NOTI_CLEAR_HOME_MAP = @"NOTI_CLEAR_HOME_MAP";
NSString * const NOTI_UPDATE_CURRENT_LOCATION_FORM_PUSH = @"NOTI_UPDATE_CURRENT_LOCATION_FORM_PUSH";
NSString * const NOTI_CLOSE_PLACE_PICKER = @"NOTI_CLOSE_PLACE_PICKER";
NSString * const NOTI_FILL_FAVOURITE_PLACE = @"NOTI_FILL_FAVOURITE_PLACE";
NSString * const NOTI_APP_STATUS_CHANGED = @"NOTI_APP_STATUS_CHANGED";
NSString * const NOTI_PROMOCODE_RESPONCE = @"NOTI_PROMOCODE_RESPONCE";
NSString * const NOTI_NEARBY_USERS_UPDATED = @"NOTI_NEARBY_USERS_UPDATED";

#pragma mark - LocalPreferanceCorner
NSString * const IS_LOGGED_IN = @"IS_LOGGED_IN";
NSString * const LOGGED_USER_ID = @"LOGGED_USER_ID";
NSString * const USER_INFO = @"USER_INFO";
NSString * const TOKEN_STRING = @"TOKEN_STRING";
NSString * const IS_INITIAL_PROFILE_SETUP = @"IS_INITIAL_PROFILE_SETUP";
NSString * const CAR_CATEGORIES = @"CAR_CATEGORIES";
NSString * const DEFAULT_BRAND_ID = @"DEFAULT_BRAND_ID";
NSString * const DEVICE_TOKEN = @"DEVICE_TOKEN";
NSString * const CANCEL_COUNTS = @"CANCEL_COUNTS";
NSString * const HOME_INFO = @"HOME_INFO";
NSString * const WORK_INFO = @"WORK_INFO";
NSString * const PAYMENT_TYPES = @"PAYMENT_TYPES";
NSString * const DEFAULT_PAYMENT = @"DEFAULT_PAYMENT";
NSString * const CURRENY_SYMBOL = @"CURRENY_SYMBOL";
NSString * const DECIMAL_POINT = @"DECIMAL_POINT";
NSString * const ADMIN_EMAIL = @"ADMIN_EMAIL";
NSString * const ADMIN_MOBILE = @"ADMIN_MOBILE";
NSString * const LAST_SELECTED_LOCATION = @"LAST_SELECTED_LOCATION";
NSString * const SEND_REQUEST_DATE = @"SEND_REQUEST_DATE";

//RunnignTripDetails
NSString * const IS_TRIP_RUNNING = @"IS_TRIP_RUNNING";
NSString * const TRIP_ID = @"TRIP_ID";
NSString * const TRIP_DRIVER_ID = @"TRIP_DRIVER_ID";
NSString * const TRIP_STATUS = @"TRIP_STATUS";
NSString * const TRIP_START_LOCATION = @"TRIP_START_LOCATION";
NSString * const TRIP_START_ADDRESS = @"TRIP_START_ADDRESS";
NSString * const TRIP_END_LOCATION = @"TRIP_END_LOCATION";
NSString * const TRIP_END_ADDRESS = @"TRIP_END_ADDRESS";
NSString * const SELECTED_PROMOCODE = @"SELECTED_PROMOCODE";

#pragma mark - APICorner
//NSString *const BASE_URL = @"http://www.adsasat.co/vcab/api/";
NSString *const BASE_URL = @"http://www.adsasat.co/volgacabs/api/";
NSString * const API_LOGIN_REGISTER = @"login.php";
NSString * const API_VERIFY_OTP = @"verify-otp.php";
NSString * const API_UPDATE_PROFILE = @"update_profile.php";
NSString * const API_CAR_CATEGORIES = @"car_category_list.php";
NSString * const API_UPDATE_CURRENT_LOCATION = @"passanger_currentlocation.php";
NSString * const API_UPDATE_TOKEN = @"device_register.php";
NSString * const API_SEND_REQUEST = @"ride_request.php";
NSString * const API_CANCEL_REQUEST = @"cust_cancel_ride.php";
NSString * const API_CMS_STRINGS = @"cms.php";
NSString * const API_TRIPS_HISTORY = @"trip_history_passenger.php";
NSString * const API_TRIP_DETAILS = @"trip_status.php";
NSString * const API_PROMOCODES = @"promocode.php";
NSString * const API_CONFIRM_RIDE_STATUS = @"confirm_ride.php";
NSString * const API_UPDATE_RIDE_DESTINATION = @"trip_update.php";
NSString * const API_RATE_YOUR_DRIVER = @"add_driver_rating.php";
NSString * const API_DECLINE_RIDE = @"decline_ride_passanger.php";
NSString * const API_USER_PROFILE = @"user_profile.php";


NSString * const API_CUSTOMER_CANCEL_FIRST_TIME = @"cust_cancel_ride_first.php";








