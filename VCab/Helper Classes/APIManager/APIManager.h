//
//  APIManager.h
//  VCab
//
//  Created by Vishal Gohil on 25/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
@interface APIManager : NSObject {
    AFHTTPSessionManager *manager;
    int clearRequestCounter;
}

-(void)findAllRoutesBeteweenTwoPlaces:(CLLocationCoordinate2D)originPlace destination:(CLLocationCoordinate2D)destination withCallBack:(void (^)(BOOL success, NSArray * result, NSString * error))callback;
-(void)findAddressUsingLocationCoordinate:(CLLocationCoordinate2D)location withCallBack:(void (^)(BOOL success, NSDictionary * result, NSString * error))callback;

-(void)loginOrRegisterMobileWithCountryCode:(NSString *)countryCode mobileNumber:(NSString *)mobileNumber withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback;

-(void)verifyOTPCodeWithCode:(NSString *)otpCode withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessage))callback;

-(void)updateUserProfileWithMobile:(NSString *)mobileNo FirstName:(NSString *)firstName LastName:(NSString *)lastName EmailAddress:(NSString *)email UserProfile:(UIImage *)profileImage withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback;
-(void)getUserProfileDetailsForUserId:(NSString *)userId WithCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback;

-(void)getCarCategoryListingWithCallBack:(void (^) (BOOL success, NSArray * categories, NSString * error))callback;

-(void)updateCustomerCurrentLocation:(NSString *)user_id latitude:(NSString *)currentLatitude longitude:(NSString *)currentLongitude brandCar:(NSString *)strCarBrandId withCallBack:(void (^) (BOOL success, NSArray * response, NSString * errorMessange))callback;

-(void)upfateDeviceTokenWithToken:(NSString *)deviceToken withCallBack:(void (^) (BOOL success, NSString * messange))callback;

-(void)sendReuqestWithCarBrandId:(NSString *)carBrandId PickupAddress:(NSString *)pickupAddress DropAddress:(NSString *)dropAddress PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude DropLattitude:(NSString *)dropLattitude DropLongitude:(NSString *)dropLongitude PickupDateTime:(NSString *)pickupDateTime tripDistance:(NSString *)tripKM tripDuration:(NSString *)duration promoCodeId:(NSString *)promocodeId paymentType:(NSString *)paymentType  withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback;

-(void)cancelTheRideRequestWithRideID:(NSString *)rideId driverId:(NSString *)driverId CancelReason:(NSString *)reasonString withCallBack:(void (^) (BOOL success, NSString * serverMessange))callback;

-(void)getCMSLoadableStringWithMode:(NSString *)apiMode lanuageId:(NSString *)languageId withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback;

-(void)getTripHistoryForCurrentUserWithCallBack:(void (^) (BOOL success, NSArray * response, NSString * errorMessage))callback;
-(void)getTripDetailsFromServerWithTripId:(NSString *)tripId WithCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessage))callback;
-(void)getPromocodeListingFromServerWithCallBack:(void (^) (BOOL success, NSArray * response, NSString * errorMessage))callback;

-(void)confirmRideStatusUpdateWithRideId:(NSString *)ride_id DriverId:(NSString *)driverId isRideCanaceled:(BOOL)isCanceled WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback;

-(void)updateDestinationDetailsWithRideId:(NSString *)rideID destinationAddress:(NSString *)destinationAddress destinationLattitude:(NSString *)destinationLattitude destinationLongitude:(NSString *)destinationLlongitude WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback;

-(void)rateDriverWithDriverId:(NSString *)driverId rideId:(NSString *)rideId rating:(NSString *)rating review:(NSString *)review WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback;

-(void)declineTheRideWithRideId:(NSString *)ride_id WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback;


-(void)CancelRideRequestFirstTime:(NSString *)rideId image:(UIImage *)mapshot withCallBack:(void (^) (BOOL success, NSString * serverMessange))callback;


@end
