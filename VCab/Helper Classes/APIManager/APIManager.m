//
//  APIManager.m
//  VCab
//
//  Created by Vishal Gohil on 25/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "APIManager.h"

@implementation APIManager

-(instancetype)init{
    manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    clearRequestCounter = 0;
    return self;
}

-(void)findAllRoutesBeteweenTwoPlaces:(CLLocationCoordinate2D)originPlace destination:(CLLocationCoordinate2D)destination withCallBack:(void (^)(BOOL success, NSArray * result, NSString * error))callback{
    
    NSDictionary * dictPera = @{
                                @"origin":[NSString stringWithFormat:@"%f,%f", originPlace.latitude, originPlace.longitude],
                                @"destination":[NSString stringWithFormat:@"%f,%f", destination.latitude, destination.longitude],
                                @"mode":@"driving"
                                };
    NSString * serverURL = @"https://maps.googleapis.com/maps/api/directions/json?";
    

    [manager GET:serverURL parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSDictionary* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSArray *routesArray = [response objectForKey:@"routes"];
        if (routesArray != nil && routesArray.count > 0){
            callback(true, routesArray, nil);
        } else {
            callback(false, nil, SET_ALERT_TEXT(@"no_route_found"));
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (DEBUG_MODE){
            NSLog(@"Can not find route : %@", error.localizedDescription);
        }
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)findAddressUsingLocationCoordinate:(CLLocationCoordinate2D)location withCallBack:(void (^)(BOOL success, NSDictionary * result, NSString * error))callback{
    NSString * serverURL = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f",location.latitude, location.longitude];
    
    [manager GET:serverURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSDictionary* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        
        NSDictionary * dictAddressInfo = ((NSArray *)response[@"results"]).firstObject;
        NSString * title =  (((NSArray *)dictAddressInfo[@"address_components"]).firstObject)[@"long_name"];
        
        if ([title length] < 5){
            NSString *title2 = (((NSArray *)dictAddressInfo[@"address_components"])[1])[@"long_name"];
            if (title2 && ![title2 isEqualToString:@""]){
                title = [NSString stringWithFormat:@"%@ %@",title,title2];
            }
        }
        
        NSString * fullAddress = dictAddressInfo[@"formatted_address"];
        
        if (title && ![title isEqualToString:@""] && fullAddress && ![fullAddress isEqualToString:@""]){
            callback(true, @{@"title":title, @"fullAddress":fullAddress}, nil);
        } else {
            callback(false, nil, SET_ALERT_TEXT(@"no_address_found"));
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (DEBUG_MODE){
            NSLog(@"Can not find route : %@", error.localizedDescription);
        }
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)loginOrRegisterMobileWithCountryCode:(NSString *)countryCode mobileNumber:(NSString *)mobileNumber withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback{
    NSDictionary * dictPera = @{
                                @"mobile_number":mobileNumber,
                                @"country_code":countryCode,
                                @"user_type":@"1"
                                };
    [manager GET:[NSString stringWithFormat:@"%@%@",BASE_URL, API_LOGIN_REGISTER] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Login / Register will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, [dictResponse[@"result"] firstObject], nil);
        }
        else if ([dictResponse[@"code"] integerValue] == -3)
        {
            callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
        }
        else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)verifyOTPCodeWithCode:(NSString *)otpCode withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessage))callback {
        NSDictionary * dictPera = @{
                                    @"user_id":APP_DELEGATE.loggedUserId,
                                    @"otp":otpCode
                                    };
        
        [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_VERIFY_OTP] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSError *jsonParserError;
            NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
            NSDictionary * dictResponse = response.firstObject;
            if (DEBUG_MODE){
                NSLog(@"Verify OTP will response : %@",dictResponse);
            }
            
            if ([dictResponse[@"code"] integerValue] == 1){
                callback(true, dictResponse[@"result"] , nil);
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
        }];
}

-(void)updateUserProfileWithMobile:(NSString *)mobileNo FirstName:(NSString *)firstName LastName:(NSString *)lastName EmailAddress:(NSString *)email UserProfile:(UIImage *)profileImage withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback {
    //image=photo.jpg
    NSData* imageData = UIImageJPEGRepresentation(profileImage, 1.0);
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString,
                                @"first_name":firstName,
                                @"last_name":lastName,
                                @"email":email,
                                @"phone":mobileNo
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_UPDATE_PROFILE] parameters:dictPera constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (imageData){
            [formData appendPartWithFileData:imageData name:@"image" fileName:[NSString stringWithFormat:@"USRPRFL_%@%@.jpg", APP_DELEGATE.loggedUserId, [[Singleton sharedInstance] getCurrentDate]] mimeType:@"image/jpeg"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (DEBUG_MODE){
            NSLog(@"Progress : %@",uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update profile will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, [dictResponse[@"result"]firstObject],nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)getUserProfileDetailsForUserId:(NSString *)userId WithCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback{
    NSDictionary * dictPera = @{
                                @"user_id":userId,
                                @"token":APP_DELEGATE.tokenString
                                };
    [manager GET:[NSString stringWithFormat:@"%@%@",BASE_URL, API_USER_PROFILE] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Show Profile will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, dictResponse[@"user_profile"], nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)getCarCategoryListingWithCallBack:(void (^) (BOOL success, NSArray * categories, NSString * error))callback{
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_CAR_CATEGORIES] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Get Car Categories will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, dictResponse[@"result"], nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)updateCustomerCurrentLocation:(NSString *)user_id latitude:(NSString *)currentLatitude longitude:(NSString *)currentLongitude brandCar:(NSString *)strCarBrandId withCallBack:(void (^) (BOOL success, NSArray * response, NSString * errorMessange))callback{

    NSDictionary * dictPera = @{
                                @"user_id":user_id,
                                @"location_lattitude":currentLatitude,
                                @"location_longitude":currentLongitude,
                                @"brand_category_id" :strCarBrandId,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    if (DEBUG_MODE){
        NSLog(@"Current Location API : \n %@%@ \n Parameters : %@",BASE_URL,API_UPDATE_CURRENT_LOCATION,dictPera);
    }
    
    [manager GET:[NSString stringWithFormat:@"%@%@",BASE_URL,API_UPDATE_CURRENT_LOCATION] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        callback(true, response, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)upfateDeviceTokenWithToken:(NSString *)deviceToken withCallBack:(void (^) (BOOL success, NSString * messange))callback{
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"is_phone":@"1",
                                @"device_token":deviceToken
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_UPDATE_TOKEN] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, SET_ALERT_TEXT(@"device_token_updated"));
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, dictResponse[@"message"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)sendReuqestWithCarBrandId:(NSString *)carBrandId PickupAddress:(NSString *)pickupAddress DropAddress:(NSString *)dropAddress PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude DropLattitude:(NSString *)dropLattitude DropLongitude:(NSString *)dropLongitude PickupDateTime:(NSString *)pickupDateTime tripDistance:(NSString *)tripKM tripDuration:(NSString *)duration promoCodeId:(NSString *)promocodeId paymentType:(NSString *)paymentType  withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback {
    
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"car_brand_id":carBrandId,
                                @"pickup_address":pickupAddress,
                                @"drop_address":dropAddress,
                                @"pickup_lat":pickupLattitude,
                                @"pickup_log":pickupLongitude,
                                @"drop_lat":dropLattitude,
                                @"drop_log":dropLongitude,
                                @"pickup_date_time":pickupDateTime,
                                @"trip_km":tripKM,
                                @"trip_duration":duration,
                                @"promocode_id":promocodeId,
                                @"token":APP_DELEGATE.tokenString,
                                @"payment_type" : paymentType,
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_SEND_REQUEST] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, dictResponse, nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)cancelTheRideRequestWithRideID:(NSString *)rideId driverId:(NSString *)driverId CancelReason:(NSString *)reasonString withCallBack:(void (^) (BOOL success, NSString * serverMessange))callback{
    NSDictionary * dictPera = @{
                                @"customer_user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString,
                                @"ride_id":rideId,
                                @"driver_id":driverId,
                                @"ride_status":@"4",
                                @"cancel_reason":reasonString
                                };
    
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_CANCEL_REQUEST] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, SET_ALERT_TEXT(@"your_request_has_been_cancelled"));
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, dictResponse[@"message"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)CancelRideRequestFirstTime:(NSString *)rideId image:(UIImage *)mapshot withCallBack:(void (^) (BOOL success, NSString * serverMessange))callback{
    
    
   // http://www.adsasat.co/vcab/api/cust_cancel_ride_first.php?customer_user_id=1&ride_status=4&ride_id=1&token=12134
    
    
    NSDictionary * dictPera = @{
                                @"customer_user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString,
                                @"ride_id":rideId,
                                @"ride_status":@"4"
                                };
    
    NSData* imageData = UIImageJPEGRepresentation(mapshot, 1.0);
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_CUSTOMER_CANCEL_FIRST_TIME] parameters:dictPera constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (imageData){
            [formData appendPartWithFileData:imageData name:@"image" fileName:[NSString stringWithFormat:@"MAPSHOT%@_%@.jpg", APP_DELEGATE.runningTripId, [[Singleton sharedInstance] getCurrentDate]] mimeType:@"image/jpeg"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (DEBUG_MODE){
            NSLog(@"Progress : %@",uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, SET_ALERT_TEXT(@"your_request_has_been_cancelled"));
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, dictResponse[@"message"]);
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}


-(void)getCMSLoadableStringWithMode:(NSString *)apiMode lanuageId:(NSString *)languageId withCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessange))callback{
    NSDictionary * dictPera = @{
                                @"cms_id":apiMode,
                                @"language_id":languageId
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_CMS_STRINGS] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, dictResponse, nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)getTripHistoryForCurrentUserWithCallBack:(void (^) (BOOL success, NSArray* response, NSString *errorMessage))callback{
    
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_TRIPS_HISTORY] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Trip history API will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, dictResponse[@"result"], nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)getTripDetailsFromServerWithTripId:(NSString *)tripId WithCallBack:(void (^) (BOOL success, NSDictionary * response, NSString * errorMessage))callback{
    NSDictionary * dictPera = @{
                                @"trip_id":tripId
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_TRIP_DETAILS] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Trip Details API will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, dictResponse[@"result"], nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)getPromocodeListingFromServerWithCallBack:(void (^) (BOOL success, NSArray * response, NSString * errorMessage))callback{

    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_PROMOCODES] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Trip history API will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, dictResponse[@"result"], nil);
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, nil, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, nil, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}
-(void)confirmRideStatusUpdateWithRideId:(NSString *)ride_id DriverId:(NSString *)driverId isRideCanaceled:(BOOL)isCanceled WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback{

    NSString * rideStatus;
    if (isCanceled){
        rideStatus = @"4";
    } else {
        rideStatus = @"5";
    }
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"ride_id":ride_id,
                                @"driver_id":driverId,
                                @"ride_status":rideStatus,
                                @"token":APP_DELEGATE.tokenString,
                                @"user_type":@"1"
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_CONFIRM_RIDE_STATUS] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, SET_ALERT_TEXT(@"ride_status_updated"));
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)updateDestinationDetailsWithRideId:(NSString *)rideID destinationAddress:(NSString *)destinationAddress destinationLattitude:(NSString *)destinationLattitude destinationLongitude:(NSString *)destinationLlongitude WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback{
    NSDictionary * dictPera = @{
                                @"ride_id":rideID,
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString,
                                @"destination_location":destinationAddress,
                                @"destination_latitude":destinationLattitude,
                                @"destination_longitude":destinationLlongitude
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_UPDATE_RIDE_DESTINATION] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, SET_ALERT_TEXT(@"destination_location_updated"));
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)rateDriverWithDriverId:(NSString *)driverId rideId:(NSString *)rideId rating:(NSString *)rating review:(NSString *)review WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback{
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString,
                                @"ride_id":rideId,
                                @"driver_id":driverId,
                                @"rating":rating,
                                @"review":review
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_RATE_YOUR_DRIVER] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, SET_ALERT_TEXT(@"driver_rating_submited"));
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

-(void)declineTheRideWithRideId:(NSString *)ride_id WithCallBack:(void (^) (BOOL success, NSString * serverMessage))callback{
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString,
                                @"ride_id":ride_id,
                                @"ride_status":@"6"
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,API_DECLINE_RIDE] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        if ([dictResponse[@"code"] integerValue] == 1){
            callback(true, SET_ALERT_TEXT(@"request_declined"));
        } else {
            if ([dictResponse[@"code"] integerValue] == -1){
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"login_expired_message"));
            } else if ([dictResponse[@"code"] integerValue] == -3) {
                [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                callback(false, SET_ALERT_TEXT(@"your_acount_inactive"));
            } else {
                callback(false, dictResponse[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, SET_ALERT_TEXT(@"something_went_wrong"));
    }];
}

@end
