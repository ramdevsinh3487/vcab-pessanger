//
//  Singleton.h
//  PeopleScience
//
//  Created by HimAnshu on 04/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FCAlertView.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface Singleton : NSObject <FCAlertViewDelegate>

@property (nonatomic,retain) UIActivityIndicatorView *activityIndicator;

+ (Singleton *)sharedInstance;

- (BOOL)checkValidContactNumber : (UITextField *)textField string :(NSString *)string range : (NSRange)range;
- (void)showGlobalHUD;
- (void)resetTumblerHUD;
- (void)dismissGlobalHUD;
- (void)showAlertViewWithMessage : (NSString *)message;
- (void)showAlertViewWithMessageSuccess : (NSString *)message;
- (void)setDynamicHeightOfLabel :(UILabel *)lbl;

- (UIImage *)getThumbImageFromVideoURL : (NSURL *)videoURL;
- (void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews;
- (void) setPasswordButtonToTextField:(UITextField *)txtField button:(UIButton *)btnPassword imageName : (NSString *)imgView;

- (void)setShadowtoUIView : (UIView *)view radius :(NSInteger) radius;

-(void)shouldShowSVProgressHud:(BOOL)shouldShow;
- (NSString *)getCurrentDate;

+(NSString *)setAlertText :(NSString *)key;
@end
