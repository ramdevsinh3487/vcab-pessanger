//
//  Singleton.m
//  PeopleScience
//
//  Created by HimAnshu on 04/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Singleton.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@implementation Singleton

+(Singleton *)sharedInstance{
    static Singleton *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[Singleton alloc]init];
    });
    
    return _sharedInstance;
}

- (void) createActivityIndicatorView
{
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:APP_DELEGATE.window.bounds];
    [_activityIndicator setColor:[UIColor blackColor]];
    _activityIndicator.tintColor = [UIColor colorWithRed:(170.0/255.0) green:(211.0/255.0) blue:(11.0/255.0) alpha:1];
    [APP_DELEGATE.window.rootViewController.view addSubview:_activityIndicator];
}

- (BOOL)checkValidContactNumber : (UITextField *)textField string :(NSString *)string range : (NSRange)range
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    return ([string isEqualToString:filtered] && (newLength <= 12));
}

- (void)showGlobalHUD {

    if (!_activityIndicator) {
        
        [self createActivityIndicatorView];
    }
    
    [_activityIndicator startAnimating];
    APP_DELEGATE.window.userInteractionEnabled = NO;
}
+(NSString *)setAlertText :(NSString *)key
{
    @try {
        return GET_Language_Value(key) ? GET_Language_Value(key) : [[key capitalizedString] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    } @catch (NSException *exception) {
        return [[key capitalizedString] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    }
}


- (void) resetTumblerHUD
{
    [_activityIndicator removeFromSuperview];
    _activityIndicator = nil;
}


- (void)dismissGlobalHUD {
    
    APP_DELEGATE.window.userInteractionEnabled = YES;
    [_activityIndicator stopAnimating];
}


- (void)showAlertViewWithMessage : (NSString *)message
{
    [APP_DELEGATE.window endEditing:YES];
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    [alert makeAlertTypeCaution];
    
    //        alert.blurBackground = YES;
    alert.dismissOnOutsideTouch = YES;
    alert.bounceAnimations = YES;
    alert.hideAllButtons = YES;
    [alert showAlertInWindow:APP_DELEGATE.window
                   withTitle:AlertTitle
                withSubtitle:message
             withCustomImage:nil
         withDoneButtonTitle:nil
                  andButtons:nil];
    alert.delegate = self;
    
    alert.autoHideSeconds = 2.0;
}


- (void)showAlertViewWithMessageSuccess : (NSString *)message
{
    [APP_DELEGATE.window endEditing:YES];
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    [alert makeAlertTypeSuccess];
    
    //        alert.blurBackground = YES;
    alert.dismissOnOutsideTouch = YES;
    alert.bounceAnimations = YES;
    alert.hideAllButtons = YES;
    [alert showAlertInWindow:APP_DELEGATE.window
                   withTitle:AlertTitle
                withSubtitle:message
             withCustomImage:nil
         withDoneButtonTitle:nil
                  andButtons:nil];
    alert.delegate = self;

    alert.autoHideSeconds = 2.0;
}

- (void) setDynamicHeightOfLabel :(UILabel *)lbl
{
    lbl.numberOfLines = 0;
    
    CGRect frame = lbl.frame;
    
    lbl.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y, lbl.frame.size.width, lbl.frame.size.height);
    
    [lbl sizeToFit];
    frame.size.height = lbl.frame.size.height;
    lbl.frame = frame;
}

- (UIImage *) getThumbImageFromVideoURL : (NSURL *)videoURL
{
    AVAsset *asset = [AVAsset assetWithURL:videoURL];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);

    return thumbnail;
}

- (void)setShadowtoUIView : (UIView *)view radius :(NSInteger) radius
{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = radius;
    view.layer.shadowOpacity = 0.6;
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    NSInteger increase = INCRESE_FONT_SIZE;
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:lbl.font.fontName size:[[lbl font] pointSize] + increase]];
    }
    
    else if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        [lbl setFont:[UIFont fontWithName:lbl.font.fontName size:[[lbl font] pointSize] + increase]];
    }
    
    else if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn.titleLabel setFont:[UIFont fontWithName:btn.titleLabel.font.fontName size:btn.titleLabel.font.pointSize + increase]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
}

#pragma mark - Button Password
- (void) setPasswordButtonToTextField:(UITextField *)txtField button:(UIButton *)btnPassword imageName : (NSString *)imgView
{
    
    [btnPassword setImage:[UIImage imageNamed:imgView] forState:UIControlStateNormal];
    btnPassword.frame = CGRectMake(0, 0, txtField.frame.size.width * 0.15, txtField.frame.size.height);
    UIView *viewTemp1 = [[UIView alloc] initWithFrame:CGRectMake(4, 0, btnPassword.frame.size.width, txtField.frame.size.height)];
    [viewTemp1 addSubview:btnPassword];
    btnPassword.center = viewTemp1.center;
    viewTemp1.userInteractionEnabled = YES;
    txtField.rightView = viewTemp1;
    txtField.rightViewMode = UITextFieldViewModeAlways;
}

#pragma mark - Play Video With AVPlayerView -

- (void) playVideoWithLink : (NSString *) strURL
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]];
    
    AVPlayer *player = [AVPlayer playerWithURL:url];
    AVPlayerViewController *playerController = [[AVPlayerViewController alloc] init];
    playerController.player = player;
    
    player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[player currentItem]];
    
    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    playerLayer.frame = APP_DELEGATE.window.bounds;
    //    [self.view.layer addSublayer:playerLayer];
    [player play];
    
    [APP_DELEGATE.window.rootViewController presentViewController:playerController animated:YES completion:nil];
}


- (void) playerItemDidReachEnd:(NSNotification *)notification {

//    AVPlayer *player = [AVPlayer playerWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrContents objectAtIndex:currentIndex] valueForKey:@"content_url"]]]];
//    playerController.player = player;
//    [player seekToTime:kCMTimeZero];
//    [player play];
}

-(void)shouldShowSVProgressHud:(BOOL)shouldShow{
    if (shouldShow){
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    } else {
        [SVProgressHUD dismiss];
    }
}


- (NSString *)getCurrentDate {
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"ddMMyyyyHHmmss"];
    NSLog(@"%@",[dateFormatter stringFromDate:currDate]);
    
    return [dateFormatter stringFromDate:currDate];
}

@end
