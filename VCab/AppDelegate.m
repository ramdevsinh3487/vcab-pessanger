//
//  AppDelegate.m
//  VCab
//
//  Created by HimAnshu on 01/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>
#import "NotificationCell.h"
#import "NoNetworkManager.h"
#import "LanguageSelectionView.h"

@import GoogleMaps;

@interface AppDelegate () <UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate
@synthesize locationManager;
@synthesize arrStatusDetails;
@synthesize strLanguageID;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.apiManager = [[APIManager alloc]init];
    arrStatusDetails =[[NSMutableArray alloc] init];

    for (int i=0; i<6; i++)
    {
        [arrStatusDetails addObject:[NSNumber numberWithInt:0]];
    }
    
    [[IQKeyboardManager sharedManager]setEnable:true];
//    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:false];
   
    [GMSServices provideAPIKey:GOOGLE_API_KEY];
    [GMSPlacesClient provideAPIKey:GOOGLE_API_KEY];
    [GMSServices provideAPIKey:GOOGLE_API_KEY];
    
    APP_DELEGATE.strLatitude = @"0.000000";
    APP_DELEGATE.strLongitude = @"0.000000";
    
     APP_DELEGATE.strLanguageID = @"1";
    
    
    [self registerUserDefaults];
    [self setupPushNoification];
    [self getCurrentLocation];
    [self updateCarCategoriesListing];
    [self updatePromocodeListing];
    [self setupRootViewController];
    
    if (DEBUG_MODE){
        NSLog(@"\nUser Details : %@",APP_DELEGATE.dictUserInfo);
        NSLog(@"\nDevice Token : %@", APP_DELEGATE.strDeviceToken);
        NSLog(@"\nUser Access Token : %@",APP_DELEGATE.tokenString);
        NSLog(@"\nUser Default Brand : %@",APP_DELEGATE.dictDefaultBrand);
        NSLog(@"\nRunning Ride id : %@",APP_DELEGATE.runningTripId);
        NSLog(@"\nDriver Id : %@",APP_DELEGATE.tripDriverId);
    }
    
    //Database Object
    [[Database shareDatabase]createEditableCopyOfDatabaseIfNeeded];
    self.db=[Database shareDatabase];
    
    [NoNetworkManager enableLackOfNetworkTakeover];
    
    if (!APP_DELEGATE.isTripRunning){
        [APP_DELEGATE clearPrevousTripDataFromApp];
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_APP_STATUS_CHANGED object:nil userInfo:@{@"is_mode_background":@true}];
  }



- (void)applicationWillEnterForeground:(UIApplication *)application {
   [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_APP_STATUS_CHANGED object:nil userInfo:@{@"is_mode_background":@false}];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - PushNotificationCorner
- (void) setupPushNoification
{
    
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (sysVer >= 10.0)
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            
            UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
            
        } else {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
        
    }
    
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];

    if (DEBUG_MODE){
        NSLog(@"Device Token : %@",str);
    }

    APP_DELEGATE.strDeviceToken = str;
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
}

-(void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    NSDictionary *aps = (NSDictionary *)[userInfo objectForKey:@"aps"];
    NSString* alertValue = [aps valueForKey:@"badge"];
    NSInteger badgeValue= [alertValue integerValue];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeValue];
}


-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSDictionary *userInfo = (NSDictionary *)[notification.request.content.userInfo objectForKey:@"aps"];
    
    if (DEBUG_MODE){
        NSLog(@"Push notification : %@",userInfo);
    }
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_UPDATE_CURRENT_LOCATION_FORM_PUSH object:nil userInfo:userInfo];
    
    if (APP_DELEGATE.isTripRunning && [APP_DELEGATE.runningTripId isEqualToString:userInfo[@"ride_id"]]){
        completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    } else {
        //Incomming notification for invalid ride id
    }
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    //Called to let your app know which action was selected by the user for a given notification.
    NSDictionary *userInfo = (NSDictionary *)[response.notification.request.content.userInfo objectForKey:@"aps"];
    
    if (DEBUG_MODE){
        NSLog(@"Push notification data : %@",userInfo);
    }
    
     [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_UPDATE_CURRENT_LOCATION_FORM_PUSH object:nil userInfo:userInfo];
}


- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    if (DEBUG_MODE){
        NSLog(@"Failled to remote notification : %@",err.localizedDescription);
    }
}

-(void)updateDeviceTokenToServer{
    [APP_DELEGATE.apiManager upfateDeviceTokenWithToken:APP_DELEGATE.strDeviceToken withCallBack:^(BOOL success, NSString *messange) {
        if (success){
            if (DEBUG_MODE){
                NSLog(@"Device token successfully submited");
            }
        }
    }];
}

#pragma mark - SelfDefinedFucntions
- (void)logoutTheUserFromAppWithAlert:(BOOL)shouldShowAlert{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(shouldShowAlert){
        FCAlertView *alert = [[FCAlertView alloc] init];
        [alert makeAlertTypeCaution];
        alert.blurBackground = false;
        alert.dismissOnOutsideTouch = YES;
        alert.bounceAnimations = YES;
        [alert showAlertInWindow:APP_DELEGATE.window
                       withTitle:AlertTitle
                    withSubtitle:SET_ALERT_TEXT(@"logout_alert_message")
                 withCustomImage:nil
             withDoneButtonTitle:nil
                      andButtons:nil];
        
        [alert setHideDoneButton:YES];
        
        [alert addButton:SET_ALERT_TEXT(@"yes_capital") withActionBlock:^{
            [self clearUserDefaults];
            LoginViewController * loginController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            self.globalNavigation = [[UINavigationController alloc] initWithRootViewController:loginController];
//            APP_DELEGATE.window.rootViewController = self.globalNavigation;
            
            MainViewController *mainViewController = nil;
            mainViewController = [[MainViewController alloc] initWithRootViewController:self.globalNavigation presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
            [mainViewController setLeftViewSwipeGestureEnabled:false];
            [mainViewController setRightViewSwipeGestureEnabled:false];
            
            self.window.rootViewController = mainViewController;
            [self.window makeKeyAndVisible];
            
            [APP_DELEGATE.timer1 invalidate];
            [APP_DELEGATE.timer2 invalidate];
            //Do Logout stuff here
        }];
        
        [alert addButton:SET_ALERT_TEXT(@"no_capital") withActionBlock:^{
            
        }];
    } else {
        [self clearUserDefaults];
        LoginViewController * loginController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        self.globalNavigation = [[UINavigationController alloc] initWithRootViewController:loginController];
        
        MainViewController *mainViewController = nil;
        mainViewController = [[MainViewController alloc] initWithRootViewController:self.globalNavigation presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
        [mainViewController setLeftViewSwipeGestureEnabled:false];
        [mainViewController setRightViewSwipeGestureEnabled:false];
        
        self.window.rootViewController = mainViewController;
        [self.window makeKeyAndVisible];
        
        [APP_DELEGATE.timer1 invalidate];
        [APP_DELEGATE.timer2 invalidate];
    }
}

-(void)setupRootViewController{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LanguageSelectionView *language = [storyboard instantiateViewControllerWithIdentifier:@"LanguageSelectionView"];
    self.globalNavigation = [[UINavigationController alloc] initWithRootViewController:language];
    
    
    APP_DELEGATE.window.rootViewController = self.globalNavigation;
    
    self.globalNavigation.navigationBarHidden = YES;
    MainViewController *mainViewController = nil;
    mainViewController = [[MainViewController alloc] initWithRootViewController:self.globalNavigation presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
    [mainViewController setLeftViewSwipeGestureEnabled:false];
    [mainViewController setRightViewSwipeGestureEnabled:false];
    
    self.window.rootViewController = mainViewController;
    [self.window makeKeyAndVisible];
}

//-(void)setupRootViewController{
//    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    if (self.isUserLoggedIn){
//        HomeViewController * homeView = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//        self.globalNavigation = [[UINavigationController alloc] initWithRootViewController:homeView];
//    } else {
//        LoginViewController * loginController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//        self.globalNavigation = [[UINavigationController alloc] initWithRootViewController:loginController];
//    }
//
//    APP_DELEGATE.window.rootViewController = self.globalNavigation;
//    
//    self.globalNavigation.navigationBarHidden = YES;
//    MainViewController *mainViewController = nil;
//    mainViewController = [[MainViewController alloc] initWithRootViewController:self.globalNavigation presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
//    [mainViewController setLeftViewSwipeGestureEnabled:false];
//    [mainViewController setRightViewSwipeGestureEnabled:false];
//    
//    self.window.rootViewController = mainViewController;
//    [self.window makeKeyAndVisible];
//}

-(void)updateCarCategoriesListing{
    [APP_DELEGATE.apiManager getCarCategoryListingWithCallBack:^(BOOL success, NSArray *categories, NSString *error) {
        if (success){
            APP_DELEGATE.arrayCarCategories = [[NSMutableArray alloc]initWithArray:categories];
            if (categories.count > 0){
                NSDictionary * dictLowescostBrand = [[categories objectAtIndex:0]mutableCopy];
                APP_DELEGATE.dictDefaultBrand = dictLowescostBrand;
            }
           
            
            if (DEBUG_MODE){
                NSLog(@"Car categories updated : %@", APP_DELEGATE.dictDefaultBrand);
            }
        
            [[NSUserDefaults standardUserDefaults]synchronize];
        } else {
            NSLog(@"Error while getting car categories : %@", categories);
            
            APP_DELEGATE.arrayCarCategories = [[NSMutableArray alloc] init];
           
            APP_DELEGATE.dictDefaultBrand = nil;
            
        }
        
         [self updatePromocodeListing];
        
    }];
}

-(void)updatePromocodeListing{
    APP_DELEGATE.arrayPromoCodes = [[NSMutableArray alloc]init];
    [APP_DELEGATE.apiManager getPromocodeListingFromServerWithCallBack:^(BOOL success, NSArray *response, NSString *errorMessage) {
        if (success){
            NSMutableArray * arrayProms = [[NSMutableArray alloc]init];
            for (int i = 0; i < response.count;i++){
                NSMutableDictionary * dictData = [[NSMutableDictionary alloc]initWithDictionary:response[i]];
                
                NSDateFormatter * dateFormater = [[NSDateFormatter alloc]init];
                
                dateFormater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                NSDate * startDate = [dateFormater dateFromString:dictData[@"start_date"]];
                NSDate * endDate = [dateFormater dateFromString:dictData[@"end_date"]];
                dateFormater.dateFormat = @"dd MMM yyyy";
                
                if (DEBUG_MODE){
                    NSLog(@"Start Date : %@",startDate);
                    NSLog(@"End Date : %@",endDate);
                    NSLog(@"Is Started : %d",startDate < [NSDate date]);
                    NSLog(@"Is Expired : %d",endDate < [NSDate date]);
                }
                
                NSString * strStartDate = [dateFormater stringFromDate:startDate];
                NSString * strEndDate = [dateFormater stringFromDate:endDate];
                
                BOOL isExpired = ([endDate timeIntervalSinceDate:[NSDate date]] < 0);
                BOOL isAvailavle = ([startDate timeIntervalSinceDate:[NSDate date]] < 0 && isExpired == false);
                BOOL isValid = (BOOL)[(NSString *)[dictData objectForKey:@"status"] intValue];
                [dictData setObject:strStartDate forKey:@"start_date"];
                [dictData setObject:strEndDate forKey:@"end_date"];
                [dictData setObject:[NSNumber numberWithBool:isExpired] forKey:@"isExpired"];
                [dictData setObject:[NSNumber numberWithBool:isAvailavle] forKey:@"isAvailavle"];
                [dictData setObject:[NSNumber numberWithBool:isValid] forKey:@"isValid"];
                if (!isExpired){
                    [arrayProms addObject:dictData];
                }
            }
            
            APP_DELEGATE.arrayPromoCodes = [[NSMutableArray alloc]initWithArray:arrayProms];
            
            if (DEBUG_MODE){
                NSLog(@"Promocode list updated");
            }
        } else {
            if (DEBUG_MODE){
                NSLog(@"No Promocode list available : %@",response);
            }
        }
        
         [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_PROMOCODE_RESPONCE object:nil userInfo:nil];
        
        
    }];
}

-(void)updateUserProfileDetails {
    [APP_DELEGATE.apiManager getUserProfileDetailsForUserId:APP_DELEGATE.loggedUserId WithCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessange) {
        if (success){
            if (response){
                NSString * countryCode = APP_DELEGATE.dictUserInfo[@"country_code"];
                APP_DELEGATE.dictUserInfo = [[NSMutableDictionary alloc]initWithDictionary:@{
                                                                                             @"user_id":[response objectForKey:@"user_id"],
                                                                                             @"user_type":[response objectForKey:@"user_type"],
                                                                                             @"email":[response objectForKey:@"email"],
                                                                                             @"first_name":[response objectForKey:@"first_name"],
                                                                                             @"last_name":[response objectForKey:@"last_name"],
                                                                                             @"mobile_no":[response objectForKey:@"mobile_no"],
                                                                                             @"user_image":[response objectForKey:@"user_image"],
                                                                                             @"country_code":countryCode
                                                                                             }];
                APP_DELEGATE.adminMobile = [response objectForKey:@"admin_mobile"];
                APP_DELEGATE.adminEmail = [response objectForKey:@"admin_email"];
                APP_DELEGATE.currencySymbol = [response objectForKey:@"currency_symbol"];
                APP_DELEGATE.decimapPoints = [response objectForKey:@"currency_decimal"];
            }
        }
    }];
}

-(void)clearPrevousTripDataFromApp{
    APP_DELEGATE.isTripRunning = false;
    APP_DELEGATE.runningTripId = @"";
     APP_DELEGATE.tripDriverId = @"";
    APP_DELEGATE.tripStatus = RIDE_STATUS_SEND_REQUEST;
    APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(00.00000, 00.00000);
    APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(00.00000, 00.00000);
    APP_DELEGATE.dictTripDriverDetails = nil;
    APP_DELEGATE.markerDriverLocation.map = nil;
    APP_DELEGATE.markerDriverLocation = nil;
    APP_DELEGATE.tripDistance = 0.0;
    APP_DELEGATE.tripDuration = 0.0;
    APP_DELEGATE.arrStatusDetails = [[NSMutableArray alloc]init];
    for (int i=0; i<6; i++)
    {
        [arrStatusDetails addObject:[NSNumber numberWithInt:0]];
    }
    APP_DELEGATE.dictTripStartAddress = @{};
    APP_DELEGATE.dictTripEndAddress = @{};
}

#pragma mark - Check Location Service Permission -

-(BOOL)CheckLocationPermission{
    
    BOOL isValid = FALSE;
    
    if(![CLLocationManager locationServicesEnabled])
    {
        NSString *strLocationAlert = [NSString stringWithFormat:@"Turn On Location Services to Allow \"%@\" to Determine Your Location", AlertTitle];
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:strLocationAlert
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Settings"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                      
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [APP_DELEGATE.globalNavigation presentViewController:alert animated:YES completion:nil];
        
        isValid = FALSE;
        
        
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        NSString *strMessages = [NSString stringWithFormat:@"App explanation for always: For improved pickups and dropoffs, customer support, and safety, \"%@\" collects your location (i) when the app is open and (ii) from the time of trip request. \n\n If you only allow access to your location while your are using the app, some features may not work while this app is in the background.",AlertTitle];
        
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:[NSString stringWithFormat:@"Allow \"%@\" to access your location?",AlertTitle]
                                     message:strMessages
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* whileUsingApp = [UIAlertAction
                                    actionWithTitle:@"Only While Using the App"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        self.locationManager = nil;
                                        [self getCurrentLocation];
                                        
                                        [self.locationManager setDelegate:self];
                                        self.locationManager.desiredAccuracy=kCLLocationAccuracyBestForNavigation;
                                        self.locationManager.distanceFilter=kCLDistanceFilterNone;
                                        
                                        [self.locationManager startMonitoringSignificantLocationChanges];
                                        [self.locationManager startUpdatingLocation];
                                        float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
                                        
                                        if (sysVer >= 8.0)
                                        {
                                            [self.locationManager requestWhenInUseAuthorization];
                                            
                                            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                                                [self.locationManager requestWhenInUseAuthorization];
                                                [self.locationManager requestWhenInUseAuthorization];
                                            }
                                        }
                                        
                                        self.strLatitude =[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
                                        self.strLongitude =[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
                                        
                                        [self.locationManager startUpdatingLocation];
                                        self.userLocation = self.locationManager.location.coordinate;
                                        
                                        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_LOCATION_UPDATES object:nil];
                                    }];
        
        UIAlertAction* AlwaysAllow = [UIAlertAction
                                   actionWithTitle:@"Always Allow"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                       [self.locationManager requestAlwaysAuthorization];
                                       
                                       if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                                           [self.locationManager requestAlwaysAuthorization];
                                           [self.locationManager requestAlwaysAuthorization];
                                       }
                                       
                                   }];
        
        
        UIAlertAction* DoNotAllow = [UIAlertAction
                                      actionWithTitle:@"Don't Allow"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                      }];
        
        
        [alert addAction:whileUsingApp];
        [alert addAction:AlwaysAllow];
        [alert addAction:DoNotAllow];
        
        [APP_DELEGATE.globalNavigation presentViewController:alert animated:YES completion:nil];
        
        
        
        isValid = FALSE;
        
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
       isValid = FALSE;
        
        
    }
    else
    {
       isValid = TRUE;
    }
    
    return isValid;
}


#pragma mark - UserLocationManager
-(void)getCurrentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    if ([CLLocationManager locationServicesEnabled]){
        
        
        [self.window endEditing:YES];
        
        [self.locationManager setDelegate:self];
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBestForNavigation;
        self.locationManager.distanceFilter=kCLDistanceFilterNone;
        
        [self.locationManager startMonitoringSignificantLocationChanges];
        [self.locationManager startUpdatingLocation];
        float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
        
        if (sysVer >= 8.0)
        {
            [self.locationManager requestWhenInUseAuthorization];
            
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
                [self.locationManager requestWhenInUseAuthorization];
            }
        }
    
        self.strLatitude =[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        self.strLongitude =[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [self.locationManager startUpdatingLocation];
        self.userLocation = self.locationManager.location.coordinate;
        
          [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_LOCATION_UPDATES object:nil];
        
        
    }
}


- (void)requestWhenInUseAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        //not aurthorisd
    } else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    // The user has not enabled any location services. Request background authorization.
     if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
   
    
//    if (DEBUG_MODE){
//         NSLog(@"User location updated With Coordinate \n Lattitude : %f \n Longirude : %f",location.coordinate.latitude, location.coordinate.longitude);
//    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (DEBUG_MODE){
        NSLog(@"Some how Location is not updating");
    }
}

-(void)updateDriverMarkerWithAnimation{
    
    if (APP_DELEGATE.markerDriverLocation == nil){
        APP_DELEGATE.markerDriverLocation = [[GMSMarker alloc] init];
        APP_DELEGATE.markerDriverLocation.position = APP_DELEGATE.tripStartLocation;
        APP_DELEGATE.markerDriverLocation.title = @"Your Cab";
        [APP_DELEGATE.markerDriverLocation setDraggable:false];
        APP_DELEGATE.markerDriverLocation.icon = [UIImage imageNamed:@"PHDriverCar"];
    }
    
    CLLocationCoordinate2D oldLocation = APP_DELEGATE.markerDriverLocation.position;
    double newLattitude = [APP_DELEGATE.dictTripDriverDetails[@"driver_latitude"]doubleValue];
    double newLongitude = [APP_DELEGATE.dictTripDriverDetails[@"driver_longitude"]doubleValue];
    
    if (oldLocation.latitude != newLattitude || oldLocation.longitude != newLongitude){
        CLLocationCoordinate2D newLocation = CLLocationCoordinate2DMake(newLattitude, newLongitude);
        [self moveMarker:APP_DELEGATE.markerDriverLocation WithAnimationFromCoordinate:oldLocation toCoordinate:newLocation];
    }
}

-(void)moveMarker:(GMSMarker *)marker WithAnimationFromCoordinate:(CLLocationCoordinate2D)oldLocation toCoordinate:(CLLocationCoordinate2D)newLocation{
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    marker.rotation = [self angleFromCoordinate:oldLocation toCoordinate:newLocation] * (180.0 / M_PI);
    [CATransaction commit];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        marker.position = newLocation;
        [CATransaction commit];
    });
}

- (float)angleFromCoordinate:(CLLocationCoordinate2D)first toCoordinate:(CLLocationCoordinate2D)second {
    
    float deltaLongitude = second.longitude - first.longitude;
    float deltaLatitude = second.latitude - first.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    
    if (deltaLongitude > 0)      return angle;
    else if (deltaLongitude < 0) return angle + M_PI;
    else if (deltaLatitude < 0)  return M_PI;
    
    return 0.0f;
}

-(NSString *)countEstimatedCostWithKM:(float)arrivalKM time:(float)arrivalTime minCharges:(NSString *)minCharges basefare:(NSString *)basefare minutePrice:(NSString *)minutePrice kmPrice:(NSString *)kmPrice{
    float minimumCharge = [minCharges floatValue];
    float basefareCost = [basefare floatValue];
    float minuteCost = [minutePrice floatValue];
    float kmCost = [kmPrice floatValue];
    
    float totalCost = (minimumCharge + basefareCost + (arrivalTime * minuteCost) + (arrivalKM * kmCost));
    if (DEBUG_MODE){
        NSLog(@"Payable amount : %0.1f $",totalCost);
    }
    
    if ([APP_DELEGATE.decimapPoints isEqualToString:@"0"]){
        return [NSString stringWithFormat:@"%@ %0.0f",self.currencySymbol, totalCost];
    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"1"]){
        return [NSString stringWithFormat:@"%@ %0.1f",self.currencySymbol, totalCost];
    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"2"]){
        return [NSString stringWithFormat:@"%@ %0.2f",self.currencySymbol, totalCost];
    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"3"]){
        return [NSString stringWithFormat:@"%@ %0.3f",self.currencySymbol, totalCost];
    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"4"]){
        return [NSString stringWithFormat:@"%@ %0.4f",self.currencySymbol, totalCost];
    } else {
        return [NSString stringWithFormat:@"%@ %0.1f",self.currencySymbol, totalCost];
    }
}

#pragma mark - Setters&Getters and UserDefaults
-(void)registerUserDefaults{
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{IS_LOGGED_IN:@false}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{IS_INITIAL_PROFILE_SETUP:@false}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{LOGGED_USER_ID:@""}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{USER_INFO:@{}}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{TOKEN_STRING:@""}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{CAR_CATEGORIES:@[]}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{DEVICE_TOKEN:@""}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{IS_TRIP_RUNNING:@false}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{TRIP_ID:@""}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{TRIP_STATUS:@0}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{TRIP_START_LOCATION:@{
                                                                     @"lattitude":@00.00000,
                                                                     @"longitude":@00.00000,
                                                                     }}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{TRIP_END_LOCATION:@{
                                                                     @"lattitude":@00.00000,
                                                                     @"longitude":@00.00000,
                                                                     }}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{HOME_INFO:@{
                                                                      @"lattitude":@"00.00000",
                                                                      @"longitude":@"00.00000",
                                                                      @"address":@""}}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{WORK_INFO:@{
                                                                       @"lattitude":@"00.00000",
                                                                       @"longitude":@"00.00000",
                                                                       @"address":@""}}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{SELECTED_PROMOCODE:@{
                                                                     @"id":@"0",
                                                                     @"promo_code":@"Apply Promo Code",
                                                                     @"percentage":@"0",
                                                                     @"status": @"1",
                                                                     @"start_date": @"",
                                                                     @"end_date": @""}}];
    
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{PAYMENT_TYPES:@[@{
                                                                                  @"type":@"cash",
                                                                                  @"payment_type": @"1",
                                                                                  @"title":@"CASH",
                                                                                  @"detail":@"Cash Payment",
                                                                                  @"image":@"PHWalletIcon",
                                                                                  @"card_number":@"",
                                                                                  @"expiry_month":@"",
                                                                                  @"expiry_year":@""
                                                                                  }]}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{DEFAULT_PAYMENT:@{
                                                                     @"type":@"cash",
                                                                     @"title":@"CASH",
                                                                     @"payment_type" : @"1",
                                                                     @"detail":@"Cash Payment",
                                                                     @"image":@"PHWalletIcon",
                                                                     @"card_number":@"",
                                                                     @"expiry_month":@"",
                                                                     @"expiry_year":@""
                                                                     }}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{LAST_SELECTED_LOCATION:@{
                                                                     @"lattitude":@00.00000,
                                                                     @"longitude":@00.00000,
                                                                     }}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{CURRENY_SYMBOL:@""}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{DECIMAL_POINT:@"0"}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{ADMIN_MOBILE:@""}];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{ADMIN_EMAIL:@""}];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)clearUserDefaults{
    APP_DELEGATE.dictUserInfo = [[NSMutableDictionary alloc]initWithDictionary:@{}];
    APP_DELEGATE.isUserLoggedIn = false;
    APP_DELEGATE.isInitialProfileSetup = false;
    APP_DELEGATE.loggedUserId = @"";
    APP_DELEGATE.tokenString = @"";
    
    if (APP_DELEGATE.arrayCarCategories && APP_DELEGATE.arrayCarCategories.count > 0){
        NSDictionary * dictDefaultBrand = APP_DELEGATE.arrayCarCategories.firstObject;
        APP_DELEGATE.dictDefaultBrand = dictDefaultBrand;
    }

    APP_DELEGATE.isTripRunning = false;
    APP_DELEGATE.runningTripId = @"";
    APP_DELEGATE.tripDriverId = @"";
    APP_DELEGATE.tripStatus = RIDE_STATUS_SEND_REQUEST;
    APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(00.00000, 00.00000);
    APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(00.00000, 00.00000);
    APP_DELEGATE.dictTripStartAddress = @{};
    APP_DELEGATE.dictTripEndAddress = @{};
    
    APP_DELEGATE.dictHomeInfo = @{
                                  @"lattitude":@"00.00000",
                                  @"longitude":@"00.00000",
                                  @"address":@""};
    
    APP_DELEGATE.dictWorkInfo = @{
                                  @"lattitude":@"00.00000",
                                  @"longitude":@"00.00000",
                                  @"address":@""};
    
    APP_DELEGATE.dictDefaultPayment = @{
                                        @"type":@"cash",
                                        @"title":@"CASH",
                                        @"payment_type" : @"1",
                                        @"detail":@"Cash Payment",
                                        @"image":@"PHWalletIcon",
                                        @"card_number":@"",
                                        @"expiry_month":@"",
                                        @"expiry_year":@""
                                        };
    
    APP_DELEGATE.arrayPaymentTypes = @[@{
                                           @"type":@"cash",
                                           @"payment_type" : @"1",
                                           @"title":@"CASH",
                                           @"detail":@"Cash Payment",
                                           @"image":@"PHWalletIcon",
                                           @"card_number":@"",
                                           @"expiry_month":@"",
                                           @"expiry_year":@""
                                           }];
    

    APP_DELEGATE.decimapPoints = @"0";
    APP_DELEGATE.adminEmail = @"";
    APP_DELEGATE.adminMobile = @"0";

    APP_DELEGATE.dictLastSelectedLocation = @{
                                 @"lattitude":@"00.00000",
                                 @"longitude":@"00.00000",
                                 @"title":@"",
                                 @"address":@""
                                 };
}

-(void)setLoggedUserId:(NSString *)loggedUserId{
    [[NSUserDefaults standardUserDefaults]setObject:loggedUserId forKey:LOGGED_USER_ID];
}

-(NSString *)loggedUserId{
    return [[NSUserDefaults standardUserDefaults]stringForKey:LOGGED_USER_ID];
}

-(void)setIsUserLoggedIn:(BOOL)isUserLoggedIn{
    [[NSUserDefaults standardUserDefaults]setBool:isUserLoggedIn forKey:IS_LOGGED_IN];
}
-(BOOL)isUserLoggedIn{
    return [[NSUserDefaults standardUserDefaults]boolForKey:IS_LOGGED_IN];
}

-(void)setIsInitialProfileSetup:(BOOL)isInitialProfileSetup{
    [[NSUserDefaults standardUserDefaults]setBool:isInitialProfileSetup forKey:IS_INITIAL_PROFILE_SETUP];
}
-(BOOL)isInitialProfileSetup{
    return [[NSUserDefaults standardUserDefaults]boolForKey:IS_INITIAL_PROFILE_SETUP];
}

-(void)setDictUserInfo:(NSMutableDictionary *)dictUserInfo{
    [[NSUserDefaults standardUserDefaults]setObject:dictUserInfo forKey:USER_INFO];
}
-(NSMutableDictionary *)dictUserInfo{
    return [[NSUserDefaults standardUserDefaults]objectForKey:USER_INFO];
}

-(void)setTokenString:(NSString *)tokenString{
    [[NSUserDefaults standardUserDefaults]setObject:tokenString forKey:TOKEN_STRING];
}
-(NSString *)tokenString{
    return [[NSUserDefaults standardUserDefaults]stringForKey:TOKEN_STRING];
}

-(void)setArrayCarCategories:(NSArray *)arrayCarCategories{
    [[NSUserDefaults standardUserDefaults]setObject:arrayCarCategories forKey:CAR_CATEGORIES];
}
-(NSArray *)arrayCarCategories{
    return [[NSUserDefaults standardUserDefaults]arrayForKey:CAR_CATEGORIES];
}

-(void)setDictDefaultBrand:(NSDictionary *)dictDefaultBrand{
    [[NSUserDefaults standardUserDefaults]setObject:dictDefaultBrand forKey:DEFAULT_BRAND_ID];
}
-(NSDictionary *)dictDefaultBrand{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_BRAND_ID];
}

-(void)setStrDeviceToken:(NSString *)strDeviceToken{
    [[NSUserDefaults standardUserDefaults]setObject:strDeviceToken forKey:DEVICE_TOKEN];
}
-(NSString *)strDeviceToken{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEVICE_TOKEN];
}

-(void)setIsTripRunning:(BOOL)isTripRunning{
    [[NSUserDefaults standardUserDefaults]setBool:isTripRunning forKey:IS_TRIP_RUNNING];
}

-(BOOL)isTripRunning{
    return [[NSUserDefaults standardUserDefaults]boolForKey:IS_TRIP_RUNNING];
}

-(void)setRunningTripId:(NSString *)runningTripId{
    [[NSUserDefaults standardUserDefaults]setObject:runningTripId forKey:TRIP_ID];
}

-(NSString *)runningTripId{
    return [[NSUserDefaults standardUserDefaults]stringForKey:TRIP_ID];
}

-(void)setTripDriverId:(NSString *)tripDriverId{
    [[NSUserDefaults standardUserDefaults]setObject:tripDriverId forKey:TRIP_DRIVER_ID];
}

-(NSString *)tripDriverId{
    return [[NSUserDefaults standardUserDefaults]stringForKey:TRIP_DRIVER_ID];
}

-(void)setTripStatus:(RunningRideStatusUpdate)tripStatus
{
    int valueToStore = 0;
    if (tripStatus == RIDE_STATUS_SEND_REQUEST){
        valueToStore = 0;
    } else if (tripStatus == RIDE_STATUS_ACCEPTED){
        valueToStore = 1;
    } else if (tripStatus == RIDE_STATUS_ARRIVAL){
        valueToStore = 2;
    } else if (tripStatus == RIDE_STATUS_ON_GOING){
        valueToStore = 3;
    } else if (tripStatus == RIDE_STATUS_CANCELED){
        valueToStore = 4;
    } else if (tripStatus == RIDE_STATUS_FINISHED){
        valueToStore = 5;
    }
    [[NSUserDefaults standardUserDefaults]setInteger:valueToStore forKey:TRIP_STATUS];
}

-(RunningRideStatusUpdate)tripStatus{
    int valueToCompar = (int)[[NSUserDefaults standardUserDefaults]integerForKey:TRIP_STATUS];
    if (valueToCompar == 0){
        return RIDE_STATUS_SEND_REQUEST;
    } else if (valueToCompar == 1){
        return RIDE_STATUS_ACCEPTED;
    } else if (valueToCompar == 2){
        return RIDE_STATUS_ARRIVAL;
    } else if (valueToCompar == 3){
        return RIDE_STATUS_ON_GOING;
    } else if (valueToCompar == 4){
        return RIDE_STATUS_CANCELED;
    } else if (valueToCompar == 5){
        return RIDE_STATUS_FINISHED;
    } else {
        return RIDE_STATUS_SEND_REQUEST;
    }
}

-(void)setTripStartLocation:(CLLocationCoordinate2D)tripStartLocation{
    NSDictionary * dictStartLocData = @{
                                        @"lattitude":[NSString stringWithFormat:@"%f",tripStartLocation.latitude],
                                        @"longitude":[NSString stringWithFormat:@"%f",tripStartLocation.longitude]
                                        };
    [[NSUserDefaults standardUserDefaults]setObject:dictStartLocData forKey:TRIP_START_LOCATION];
}
-(CLLocationCoordinate2D)tripStartLocation{
    NSDictionary * dictStartLocData = [[NSUserDefaults standardUserDefaults]dictionaryForKey:TRIP_START_LOCATION];
    double lattitude = [dictStartLocData[@"lattitude"] doubleValue];
    double longitude = [dictStartLocData[@"longitude"] doubleValue];
    return CLLocationCoordinate2DMake(lattitude, longitude);
}

-(void)setDictTripStartAddress:(NSDictionary *)dictTripStartAddress{
    [[NSUserDefaults standardUserDefaults]setObject:dictTripStartAddress forKey:TRIP_START_ADDRESS];
}
-(NSDictionary *)dictTripStartAddress{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:TRIP_START_ADDRESS];
}

-(void)setTripEndLocation:(CLLocationCoordinate2D)tripEndLocation{
    NSDictionary * dictEndLocData = @{
                                        @"lattitude":[NSString stringWithFormat:@"%f",tripEndLocation.latitude],
                                        @"longitude":[NSString stringWithFormat:@"%f",tripEndLocation.longitude]
                                        };
    [[NSUserDefaults standardUserDefaults]setObject:dictEndLocData forKey:TRIP_END_LOCATION];
}
-(CLLocationCoordinate2D)tripEndLocation{
    NSDictionary * dictEndLocData = [[NSUserDefaults standardUserDefaults]dictionaryForKey:TRIP_END_LOCATION];
    double lattitude = [dictEndLocData[@"lattitude"] doubleValue];
    double longitude = [dictEndLocData[@"longitude"] doubleValue];
    return CLLocationCoordinate2DMake(lattitude, longitude);
}

-(void)setDictTripEndAddress:(NSDictionary *)dictTripEndAddress{
    [[NSUserDefaults standardUserDefaults]setObject:dictTripEndAddress forKey:TRIP_END_ADDRESS];
}
-(NSDictionary *)dictTripEndAddress{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:TRIP_END_ADDRESS];
}

-(void)setDictHomeInfo:(NSDictionary *)dictHomeInfo{
    [[NSUserDefaults standardUserDefaults]setObject:dictHomeInfo forKey:HOME_INFO];
}
-(NSDictionary *)dictHomeInfo{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:HOME_INFO];
}

-(void)setDictWorkInfo:(NSDictionary *)dictWorkInfo{
    [[NSUserDefaults standardUserDefaults]setObject:dictWorkInfo forKey:WORK_INFO];
}
-(NSDictionary *)dictWorkInfo{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:WORK_INFO];
}

-(void)setArrayPaymentTypes:(NSArray *)arrayPaymentTypes{
    [[NSUserDefaults standardUserDefaults]setObject:arrayPaymentTypes forKey:PAYMENT_TYPES];
}
-(NSArray *)arrayPaymentTypes{
    return [[NSUserDefaults standardUserDefaults]arrayForKey:PAYMENT_TYPES];
}

-(void)setDictDefaultPayment:(NSDictionary *)dictDefaultPayment{
    [[NSUserDefaults standardUserDefaults]setObject:dictDefaultPayment forKey:DEFAULT_PAYMENT];
}
-(NSDictionary *)dictDefaultPayment{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_PAYMENT];
}

-(void)setCurrencySymbol:(NSString *)currencySymbol{
    [[NSUserDefaults standardUserDefaults]setObject:currencySymbol forKey:CURRENY_SYMBOL];
}
-(NSString *)currencySymbol{
    return [[NSUserDefaults standardUserDefaults]stringForKey:CURRENY_SYMBOL];
}

-(void)setDecimapPoints:(NSString *)decimapPoints{
    [[NSUserDefaults standardUserDefaults]setObject:decimapPoints forKey:DECIMAL_POINT];
}

-(NSString *)decimapPoints{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DECIMAL_POINT];
}

-(void)setAdminEmail:(NSString *)adminEmail{
    [[NSUserDefaults standardUserDefaults]setObject:adminEmail forKey:ADMIN_EMAIL];
}

-(NSString *)adminEmail{
    return [[NSUserDefaults standardUserDefaults]stringForKey:ADMIN_EMAIL];
}

-(void)setAdminMobile:(NSString *)adminMobile{
    [[NSUserDefaults standardUserDefaults]setObject:adminMobile forKey:ADMIN_MOBILE];
}

-(NSString *)adminMobile{
    return [[NSUserDefaults standardUserDefaults]stringForKey:ADMIN_MOBILE];
}

-(void)setDictLastSelectedLocation:(NSDictionary *)dictLastSelectedLocation{
    [[NSUserDefaults standardUserDefaults]setObject:dictLastSelectedLocation forKey:LAST_SELECTED_LOCATION];
}
-(NSDictionary *)dictLastSelectedLocation{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:LAST_SELECTED_LOCATION];
}

-(void)setSendRequestDate:(NSDate *)sendRequestDate{
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    [[NSUserDefaults standardUserDefaults]setObject:timestamp forKey:SEND_REQUEST_DATE];
}
-(NSDate *)sendRequestDate{
    double timeStamp = [[[NSUserDefaults standardUserDefaults]stringForKey:SEND_REQUEST_DATE]doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    return date;
}

@end
