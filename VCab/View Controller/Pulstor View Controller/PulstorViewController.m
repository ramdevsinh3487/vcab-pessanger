//
//  PulstorViewController.m
//  VCab
//
//  Created by Vishal Gohil on 15/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "PulstorViewController.h"

@interface PulstorViewController ()

@end

@implementation PulstorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    self.pulstorContainerHeight.constant = 10;
    self.lableRemainingTimeMessage.text = @"";
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(requestAcceptedNotificationArrived) name:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(applicationStatusChangedNotification:) name:NOTI_APP_STATUS_CHANGED object:nil];
    // Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.imageViewSmallCar.hidden = false;
    self.lableRemainingTimeMessage.hidden = false;
    self.lableRemainingTime.hidden = false;
    
    self.lableTitle.text = SET_ALERT_TEXT(@"request_send");
    self.lableDetails.text = SET_ALERT_TEXT(@"send_request_messahe");
    self.lableRemainingTimeMessage.text = SET_ALERT_TEXT(@"your_request_will_expire_in");

    [timerResndcode invalidate];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTI_APP_STATUS_CHANGED object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    APP_DELEGATE.sendRequestDate = [NSDate date];
    [super viewWillAppear:animated];
    if (APP_DELEGATE.tripStatus == RIDE_STATUS_SEND_REQUEST){
        self.imageViewSmallCar.hidden = false;
        self.lableRemainingTimeMessage.hidden = false;
        self.lableRemainingTime.hidden = false;
        self.cancelButton.hidden = false;
    } else {
        self.imageViewSmallCar.hidden = true;
        self.lableRemainingTimeMessage.hidden = true;
        self.lableRemainingTime.hidden = true;
        self.cancelButton.hidden = true;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (APP_DELEGATE.tripStatus == RIDE_STATUS_SEND_REQUEST){
        totalTimeToWait = 120;
        remainigTime = totalTimeToWait - [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate];
        
        timerResndcode = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerWillTicked) userInfo:nil repeats:true];
         [self animateToBigView];
        
        self.lableTitle.text = SET_ALERT_TEXT(@"request_send");
        self.lableDetails.text = SET_ALERT_TEXT(@"send_request_messahe");
        self.lableRemainingTimeMessage.text = SET_ALERT_TEXT(@"your_request_will_expire_in");
    } else {
        self.lableTitle.text = SET_ALERT_TEXT(@"cab_arrived");
        self.lableDetails.text = SET_ALERT_TEXT(@"driver_arrive_message");
        self.lableRemainingTimeMessage.text = SET_ALERT_TEXT(@"your_request_will_expire_in");

        self.pulstorContainerHeight.constant = SCREEN_WIDTH - 50;
        [UIView animateWithDuration:1.0 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self animateToArrivalCar];
        }];
    }
}

-(void)animateToBigView{
    if (timerResndcode.isValid){
        self.pulstorContainerHeight.constant = SCREEN_WIDTH - 50;
        
        [UIView animateWithDuration:3.0 animations:^{
            [self.view layoutIfNeeded];
        }];
        
        [UIView animateWithDuration:3.0 animations:^{
            self.viewPulstorContainer.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.viewPulstorContainer.alpha = 1.0;
            self.pulstorContainerHeight.constant = 10;
            [self.view layoutIfNeeded];
            [self animateToBigView];
        }];
    }
}

-(void)animateToArrivalCar{
    self.rightSpaceToArrivalCar.constant = -1;
    [UIView animateWithDuration:3.0 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self shouldPlayCarHornSound];
        
        UIBackgroundTaskIdentifier bgTask = 0;
        UIApplication  *app = [UIApplication sharedApplication];
        bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
        }];
        timerResndcode = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(shouldPlayCarHornSound) userInfo:nil repeats:YES];
        
        [[NSRunLoop currentRunLoop]addTimer:timerResndcode forMode:UITrackingRunLoopMode];
        
    }];
}

-(void)shouldPlayCarHornSound{
    if (APP_DELEGATE.tripStatus == RIDE_STATUS_ARRIVAL){
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"SNDCarHorn" ofType:@"mp3"];
        SystemSoundID soundID;
        AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain([NSURL fileURLWithPath: soundPath]), &soundID);
        AudioServicesPlaySystemSound (soundID);
    } else {
        [timerResndcode invalidate];
    }
}

-(void)timerWillTicked{
    
    NSLog(@"Deferance : %f", [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate]);
    if (totalTimeToWait <= [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate]){
        [timerResndcode invalidate];
        [self shouldShowAlertForExpireMode:true];
    } else {
        remainigTime = remainigTime - 1;
        int totalSeconds = remainigTime % 60;
        int totalMinutes = (remainigTime / 60) % 60;
        self.lableRemainingTime.text = [NSString stringWithFormat:@"%02d : %02d %@", totalMinutes, totalSeconds,GET_Language_Value(@"minutes")];
    }
}



-(void)requestAcceptedNotificationArrived{
    [timerResndcode invalidate];
    
    timerResndcode = nil;
    
    [APP_DELEGATE.globalNavigation dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)cancelButtonTapAction:(id)sender {
    [self shouldShowAlertForExpireMode:false];
}


#pragma mark - AlertViewControllerDelegate
-(void)shouldShowAlertForExpireMode:(BOOL)isExpireMode{
    isAlertExpire = isExpireMode;
    
    
    AlertViewController * alertView = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewController"];
    alertView.delegate = self;
    alertView.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    if (isAlertExpire){
        alertView.alertTitle = SET_ALERT_TEXT(@"request_expired");
        alertView.alertMessage = SET_ALERT_TEXT(@"your_request_expire_try_with_another_car_category");
        alertView.okayTitle = SET_ALERT_TEXT(@"okay");
        alertView.cancelTitle = @"";
        alertView.canDismissTap = false;
        alertView.isModeWithTextView = false;
        
        APP_DELEGATE.isTripRunning = false;
        [APP_DELEGATE.apiManager declineTheRideWithRideId:APP_DELEGATE.runningTripId WithCallBack:^(BOOL success, NSString *serverMessage) {
            if (DEBUG_MODE){
                NSLog(@"Your Previous trip Declined");
            }
        }];
        
        self.lableTitle.hidden = true;
        self.lableDetails.hidden = true;
        self.cancelButton.hidden = true;
        self.viewPulstorContainer.hidden = true;
        self.imageViewSmallCar.hidden = true;
    } else { 
        alertView.alertTitle = SET_ALERT_TEXT(@"cancel_trip_questionmark");
        alertView.alertMessage = SET_ALERT_TEXT(@"your_request_will_canceled_permanently");
        alertView.okayTitle = SET_ALERT_TEXT(@"yes_capital");
        alertView.cancelTitle = SET_ALERT_TEXT(@"no_capital");
        alertView.canDismissTap = true;
        alertView.isModeWithTextView = false;
    }
    
    [self presentViewController:alertView animated:true completion:nil];
}
-(void)alertControllerOkayButtonTappedWithTextViewText:(NSString *)textViewText{
    self.lableTitle.hidden = false;
    self.lableDetails.hidden = false;
    self.cancelButton.hidden = false;
    self.viewPulstorContainer.hidden = false;
    
    if (isAlertExpire){
        [self dismissViewControllerAnimated:true completion:^{
            [self.delegate didSelectRetryForRequest];
        }];
    } else {
      //  NSString * reasonString = textViewText;//Will Pass with API later
        START_HUD
        UIImageView * dummyImageView = [[UIImageView alloc]init];
        [self.view addSubview:dummyImageView];
        dummyImageView.hidden = true;
        NSString * serverURL = [[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?markers=size:big|color:black|anchor:bottomright|label:P|%f,%f&scale=2&size=512x256&path=weight:1|color:0x000000&key=%@",APP_DELEGATE.tripStartLocation.latitude, APP_DELEGATE.tripStartLocation.longitude, GOOGLE_STATIC_MAP_API_KEY] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];;
        
        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, dummyImageView.frame.size.width, dummyImageView.frame.size.height)];
        [dummyImageView addSubview:activity_indicator];
        [activity_indicator startAnimating];
        [activity_indicator setColor:[UIColor blackColor]];
        
        [dummyImageView sd_setImageWithURL:[NSURL URLWithString:serverURL] placeholderImage:[UIImage imageNamed:@"PHAddProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (DEBUG_MODE){
                NSLog(@"Image Info %@",image);
            }
            
            [APP_DELEGATE.apiManager CancelRideRequestFirstTime:APP_DELEGATE.runningTripId image:image withCallBack:^(BOOL success, NSString *serverMessange) {
                SHOW_ALERT_WITH_SUCCESS(serverMessange);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [APP_DELEGATE clearPrevousTripDataFromApp];
                    [self dismissViewControllerAnimated:true completion:^{
                        [self.delegate didCancelRequestByUser];
                    }];
                });
                STOP_HUD
            }];
        }];
        
    }
}

-(void)alertControllerCancelButtonTapped{
    //Cancel button
    self.lableTitle.hidden = false;
    self.lableDetails.hidden = false;
    self.cancelButton.hidden = false;
    self.viewPulstorContainer.hidden = false;
}

-(void)applicationStatusChangedNotification:(NSNotification *)notif{
    if(APP_DELEGATE.tripStatus == RIDE_STATUS_ARRIVAL){
        if ([notif.userInfo[@"is_mode_background"]boolValue] == true){
            if (DEBUG_MODE){
                NSLog(@"App Status changed to Background");
            }
            
            UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
            UIApplication *app = [UIApplication sharedApplication];
            bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                [app endBackgroundTask:bgTask];
            }];
            [timerResndcode invalidate];
            timerResndcode = nil;
            
            timerResndcode = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(shouldPlayCarHornSound) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:timerResndcode forMode:NSRunLoopCommonModes];
            //App is Going to background
        } else {
            if (DEBUG_MODE){
                NSLog(@"App Status changed to Foreground");
            }
            [timerResndcode invalidate];
            timerResndcode = nil;
            timerResndcode = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(shouldPlayCarHornSound) userInfo:nil repeats:YES];
            //App is going to foreground
        }
    }
}
@end
