//
//  PulstorViewController.h
//  VCab
//
//  Created by Vishal Gohil on 15/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AlertViewController.h"

@protocol PulstorViewControllerDelegate <NSObject>

@optional
-(void)didSelectRetryForRequest;

@optional
-(void)didCancelRequestByUser;

@optional
-(void)didReciveAcceptNotification;

@optional
-(void)didReciveStartRideNotification;

@end
@interface PulstorViewController : UIViewController <AlertViewControllerDelegate> {
    int remainigTime;
    int totalTimeToWait;
    NSTimer * timerResndcode;
    BOOL isAlertExpire;
}
@property (weak, nonatomic) IBOutlet UIView *viewPulstorContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pulstorContainerHeight;

@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableDetails;
@property (strong, nonatomic) IBOutlet UILabel *lableRemainingTimeMessage;
@property (weak, nonatomic) IBOutlet UILabel *lableRemainingTime;
@property id <PulstorViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSmallCar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightSpaceToArrivalCar;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end
