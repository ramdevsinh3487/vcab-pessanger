//
//  VerificationViewController.h
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "AlertViewController.h"
@interface VerificationViewController : UIViewController <UITextFieldDelegate, AlertViewControllerDelegate>{
    int remainigTime;
    NSTimer * timerResndcode;
    BOOL canMoveUp;
    NSString * timerMessage;
}
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableOTPMessage;
@property (weak, nonatomic) IBOutlet UILabel *lableEneterMebileNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCharOne;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCharTwo;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCharThree;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCharFour;
@property (weak, nonatomic) IBOutlet UILabel *lableTimeMessage;
@property (weak, nonatomic) IBOutlet UILabel *lableRemainingTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonGoBack;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *goBackLeftSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lableBottomSpace;

@property NSMutableDictionary * dictLoginResponse;
@end
