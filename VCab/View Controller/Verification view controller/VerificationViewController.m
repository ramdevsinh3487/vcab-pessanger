//
//  VerificationViewController.m
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "VerificationViewController.h"

@interface VerificationViewController ()

@end

@implementation VerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    self.lableEneterMebileNumber.text = [NSString stringWithFormat:@"%@ %@", self.dictLoginResponse[@"country_code"],self.dictLoginResponse[@"mobile_no"]];
    
    remainigTime = 120;
    self.containerHeight.constant = SCREEN_HEIGHT / 2;
    [self.view layoutIfNeeded];

    [self.navigationController setNavigationBarHidden:true];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureFound:)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.lableRemainingTime.text = @"";
    
    if (DEBUG_MODE){
        NSString * otpCode = [self.dictLoginResponse objectForKey:@"otp_code"];
        for (int i = 0; i < 4; i++){
            if (i == 0){
                self.textFieldCharOne.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
            } else if (i == 1){
                self.textFieldCharTwo.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
            } else if (i == 2){
                self.textFieldCharThree.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
            } else if (i == 3){
                self.textFieldCharFour.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
            }
        }
    }
    
    self.lableOTPMessage.text = SET_ALERT_TEXT(@"lbl_otp_msg");
    timerMessage = SET_ALERT_TEXT(@"we_will_resend_verification_code_after");
}

-(void)tapGestureFound:(UIGestureRecognizer *)recognizer{
    self.lableBottomSpace.constant = 10;
    [self.view layoutIfNeeded];
    
    [self.textFieldCharFour resignFirstResponder];
    [self.textFieldCharThree resignFirstResponder];
    [self.textFieldCharTwo resignFirstResponder];
    [self.textFieldCharOne resignFirstResponder];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.lableTimeMessage setHidden:false];
    [self.lableRemainingTime setHidden:false];
    
    self.lableBottomSpace.constant = 10;
    [self.view layoutIfNeeded];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.containerHeight.constant = SCREEN_HEIGHT;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        timerResndcode = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerWillTicked) userInfo:nil repeats:true];
    }];
}

-(void)timerWillTicked{
    remainigTime = remainigTime - 1;
    if (remainigTime == 118){
        [timerResndcode invalidate];
        [self willVerfyOTPWithServer];
        return;
    }
    
    if (remainigTime <= 0){
        [timerResndcode invalidate];
        [self shouldShowAlertForResendCode];
    } else {
        int totalSeconds = remainigTime % 60;
        int totalMinutes = (remainigTime / 60) % 60;
        self.lableTimeMessage.text = [NSString stringWithFormat:@"%@ %0d : %0d",timerMessage, totalMinutes, totalSeconds];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBackButtonTapAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == 101){
        canMoveUp = true;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    int iValue;
    if ([string isEqualToString:@""] || [[NSScanner scannerWithString:string] scanInt:&iValue]){
        if (DEBUG_MODE){
            NSLog(@"value entered correctly.");
        }
    } else {
        return false;
    }
    
    if ([textField.text isEqualToString:@" "]){
        textField.text = @"";
    }
    if (textField.tag == 101){
        if (![string isEqualToString:@""]){
            textField.text = string;
            [textField resignFirstResponder];
            [_textFieldCharTwo becomeFirstResponder];
            _textFieldCharTwo.text = @" ";
            return false;
        }
    }else if (textField.tag == 102){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [textField resignFirstResponder];
            [_textFieldCharOne becomeFirstResponder];
            return false;
        } else {
            textField.text = string;
            [textField resignFirstResponder];
            [_textFieldCharThree becomeFirstResponder];
            _textFieldCharThree.text = @" ";
            return false;
        }
    } else if (textField.tag == 103){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [textField resignFirstResponder];
            [_textFieldCharTwo becomeFirstResponder];
            return false;
        } else {
            textField.text = string;
            [textField resignFirstResponder];
            [_textFieldCharFour becomeFirstResponder];
            _textFieldCharFour.text = @" ";
            return false;
        }
    } else if (textField.tag == 104){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [textField resignFirstResponder];
            [_textFieldCharThree becomeFirstResponder];
            return false;
        } else {
            if ([textField.text length] > 0){
                return false;
            } else {
                textField.text = string;
                [textField resignFirstResponder];
                return false;
            }
        }
    }
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 104){
        [self willVerfyOTPWithServer];
        [timerResndcode invalidate];
    }
}

-(void)willVerfyOTPWithServer{
    [self.textFieldCharFour resignFirstResponder];
    
    NSString * firstChar = [self.textFieldCharOne.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString * secondChar = [self.textFieldCharTwo.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString * thirdChar = [self.textFieldCharThree.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString * fourthChar = [self.textFieldCharFour.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (firstChar.length == 0 || secondChar.length == 0 || thirdChar.length == 0 || fourthChar.length == 0){
    } else {
        NSString * otpCode = [NSString stringWithFormat:@"%@%@%@%@", firstChar, secondChar, thirdChar, fourthChar];
        START_HUD
        [APP_DELEGATE.apiManager verifyOTPCodeWithCode:otpCode withCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessage) {
            if (success){
                [self.lableTimeMessage setHidden:true];
                [self.lableRemainingTime setHidden:true];
                [APP_DELEGATE updateDeviceTokenToServer];
                
                NSMutableDictionary * dictResponse = [[NSMutableDictionary alloc]initWithDictionary:self.dictLoginResponse];
    
                [dictResponse setObject:response[@"mobile_no"] forKey:@"mobile_no"];
                [dictResponse setObject:response[@"first_name"] forKey:@"first_name"];
                [dictResponse setObject:response[@"last_name"] forKey:@"last_name"];
                [dictResponse setObject:response[@"email"] forKey:@"email"];
                
                APP_DELEGATE.dictUserInfo = dictResponse;
                
                if (response[@"first_name"] && ![response[@"first_name"] isEqualToString:@""] && response[@"last_name"] && ![response[@"last_name"] isEqualToString:@""]){
                    
                   
                    APP_DELEGATE.isInitialProfileSetup = true;
                    if (!APP_DELEGATE.isUserLoggedIn){
                        APP_DELEGATE.isUserLoggedIn = true;
                    }
                    
                    
                    HomeViewController * homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                    APP_DELEGATE.globalNavigation = [[UINavigationController alloc] initWithRootViewController:homeView];
                    
                    MainViewController *mainViewController = nil;
                    mainViewController = [[MainViewController alloc] initWithRootViewController:APP_DELEGATE.globalNavigation presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
                    [mainViewController setLeftViewSwipeGestureEnabled:false];
                    [mainViewController setRightViewSwipeGestureEnabled:false];
                    
                    APP_DELEGATE.window.rootViewController = mainViewController;
                    [APP_DELEGATE.window makeKeyAndVisible];
                    
                } else {
                    ProfileViewController * userDetailsView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                    userDetailsView.isModeInitialUpdate = true;
                    userDetailsView.canGoBack = true;
                    userDetailsView.canGoBack = true;
                    [self.navigationController pushViewController:userDetailsView animated:true];
                }
            } else {
                SHOW_ALERT_WITH_CAUTION(errorMessage)
                timerResndcode = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerWillTicked) userInfo:nil repeats:true];
            }
            STOP_HUD
        }];
    }
}

#pragma mark - UIKeyBoardDelegate
- (void)keyBoardWillShowNotification:(NSNotification*)notification
{
    if (canMoveUp == true){
        NSDictionary* keyboardInfo = [notification userInfo];
        NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
        self.lableBottomSpace.constant = [keyboardFrameBegin CGRectValue].size.height + 20;
        [self.view layoutIfNeeded];
    }
}

#pragma mark - AlertViewControllerDelegate
-(void)shouldShowAlertForResendCode{
    AlertViewController * alertView = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewController"];
    alertView.delegate = self;
    alertView.modalPresentationStyle = UIModalPresentationOverFullScreen;
    alertView.alertTitle = SET_ALERT_TEXT(@"time_out");
    alertView.alertMessage = SET_ALERT_TEXT(@"resent_otp_code");
    alertView.okayTitle = SET_ALERT_TEXT(@"resend_capital");
    alertView.cancelTitle = SET_ALERT_TEXT(@"cancel_capital");
    alertView.canDismissTap = true;
    alertView.isModeWithTextView = false;
    
    [self presentViewController:alertView animated:true completion:nil];
}

-(void)alertControllerOkayButtonTappedWithTextViewText:(NSString *)textViewText {
    self.textFieldCharOne.text = @" ";
    self.textFieldCharTwo.text = @" ";
    self.textFieldCharThree.text = @" ";
    self.textFieldCharFour.text = @" ";
    
    NSString * mobileNumber = @"";
    NSString * countryCode = @"";
    //Will Resend code
    START_HUD
    [APP_DELEGATE.apiManager loginOrRegisterMobileWithCountryCode:countryCode mobileNumber:mobileNumber withCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessange) {
        if (success){
        
            if (DEBUG_MODE){
                NSString * otpCode = [response objectForKey:@"otp_code"];
                for (int i = 0; i < 4; i++){
                    if (i == 0){
                        self.textFieldCharOne.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
                    } else if (i == 1){
                        self.textFieldCharTwo.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
                    } else if (i == 2){
                        self.textFieldCharThree.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
                    } else if (i == 3){
                        self.textFieldCharFour.text = [NSString stringWithFormat:@"%c", [otpCode characterAtIndex:i]];
                    }
                }
            }
            
            remainigTime = 120;
            timerResndcode = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerWillTicked) userInfo:nil repeats:true];
            self.lableTimeMessage.hidden = false;
            self.lableRemainingTime.hidden = false;
        } else {
            SHOW_ALERT_WITH_CAUTION(errorMessange)
        }
        STOP_HUD
    }];
}

-(void)alertControllerCancelButtonTapped{
    [self.navigationController popViewControllerAnimated:true];
}

@end
