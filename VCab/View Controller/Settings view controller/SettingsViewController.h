//
//  SettingsViewController.h
//  VCab
//
//  Created by Vishal Gohil on 22/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlaces/GooglePlaces.h>
#import "AlertViewController.h"
#import "ProfileViewController.h"
#import "RunningRideViewController.h"
@interface SettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, RunningRideViewControllerDelegate, AlertViewControllerDelegate> {
    NSArray * arrayTableData;
    BOOL isModeSetHome;
}
@property (weak, nonatomic) IBOutlet UITableView *tableViewSettingMenu;
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;

@end
