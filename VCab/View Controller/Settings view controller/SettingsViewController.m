//
//  SettingsViewController.m
//  VCab
//
//  Created by Vishal Gohil on 22/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    [self loadDataForTheView];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)loadDataForTheView{
    NSString * mobile = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.dictUserInfo[@"country_code"],APP_DELEGATE.dictUserInfo[@"mobile_no"]];
    NSString * firstName = APP_DELEGATE.dictUserInfo[@"first_name"];
    NSString * lastName = APP_DELEGATE.dictUserInfo[@"last_name"];
    NSString * email = APP_DELEGATE.dictUserInfo[@"email"];
    NSString * userImage = APP_DELEGATE.dictUserInfo[@"user_image"];
    
    
    NSString * homeAddress = APP_DELEGATE.dictHomeInfo[@"address"];
    if ([homeAddress isEqualToString:@""]){
        homeAddress = SET_ALERT_TEXT(@"set_home");
    }
    
    NSString * workAddress = APP_DELEGATE.dictWorkInfo[@"address"];
    if ([workAddress isEqualToString:@""]){
        workAddress = SET_ALERT_TEXT(@"set_work");
    }
    
    self.lableTitle.text = SET_ALERT_TEXT(@"settings");
    
    arrayTableData = @[
                       @{
                           @"header":@"cellUserProfile",
                           @"title":@"Profile",
                           @"data":@[@{ @"user_name":[NSString stringWithFormat:@"%@ %@",firstName,lastName],
                                        @"user_email":email,
                                        @"user_phone":mobile,
                                        @"user_image":userImage}]
                           },
                       @{
                           @"header":@"cellFavorite",
                           @"title":@"Favorite",
                           @"data":@[@{ @"title":SET_ALERT_TEXT(@"home"),
                                        @"detail":homeAddress,
                                        @"image":@"home"},
                                     @{ @"title":SET_ALERT_TEXT(@"work"),
                                        @"detail":workAddress,
                                        @"image":@"work"}]
                           },
                       @{
                           @"header":@"cellDefault",
                           @"title":@"",
                           @"data":@[@{ @"title":SET_ALERT_TEXT(@"sign_out")}]
                           }];
    self.tableViewSettingMenu.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableViewSettingMenu reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTapAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UITableViewDataSource&Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrayTableData.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray *)arrayTableData[section][@"data"]).count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * cellIdentifier = arrayTableData[indexPath.section][@"header"];
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (indexPath.section == 0){
        UIImageView * imageVeiwProfile = [cell viewWithTag:101];
        imageVeiwProfile.layer.cornerRadius = imageVeiwProfile.frame.size.height / 2;
        imageVeiwProfile.layer.borderColor = [UIColor lightGrayColor].CGColor;
        imageVeiwProfile.layer.borderWidth = 1.0;
        imageVeiwProfile.layer.masksToBounds = true;
        
        UILabel * lableUserName = [cell viewWithTag:102];
        UILabel * lableUserPhone = [cell viewWithTag:103];
        UILabel * lableUserEmail = [cell viewWithTag:104];
        
        lableUserName.text = arrayTableData[indexPath.section][@"data"][indexPath.row][@"user_name"];
        lableUserEmail.text = arrayTableData[indexPath.section][@"data"][indexPath.row][@"user_email"];
        lableUserPhone.text = arrayTableData[indexPath.section][@"data"][indexPath.row][@"user_phone"];
        
        NSString * userImageURL = arrayTableData[indexPath.section][@"data"][indexPath.row][@"user_image"];
        
        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imageVeiwProfile.frame.size.width, imageVeiwProfile.frame.size.height)];
        [imageVeiwProfile addSubview:activity_indicator];
        [activity_indicator startAnimating];
        [activity_indicator setColor:[UIColor blackColor]];
        
        [imageVeiwProfile sd_setImageWithURL:[NSURL URLWithString:userImageURL] placeholderImage:[UIImage imageNamed:@"PHAddProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activity_indicator stopAnimating];
            [activity_indicator removeFromSuperview];
        }];
    } else if (indexPath.section == 1){
        UIImageView * imageViewIcon = [cell viewWithTag:101];
        UILabel * lableTitle = [cell viewWithTag:102];
        UILabel * lableDetail = [cell viewWithTag:103];
        
        lableTitle.text = arrayTableData[indexPath.section][@"data"][indexPath.row][@"title"];
        lableDetail.text = arrayTableData[indexPath.section][@"data"][indexPath.row][@"detail"];
        imageViewIcon.image = [UIImage imageNamed:arrayTableData[indexPath.section][@"data"][indexPath.row][@"image"]];
        
        UIButton * deleteButton = [cell viewWithTag:104];
        if (![lableDetail.text isEqualToString:SET_ALERT_TEXT(@"set_home")] && ![lableDetail.text isEqualToString:SET_ALERT_TEXT(@"set_work")]){
            if ([[lableTitle.text lowercaseString] isEqualToString:@"home"]){
                [deleteButton setRestorationIdentifier:@"HOME"];
            } else if ([[lableTitle.text lowercaseString] isEqualToString:@"work"]){
                [deleteButton setRestorationIdentifier:@"WORK"];
            }
            deleteButton.hidden = false;
        } else {
            deleteButton.hidden = true;
        }
    } else {
        UILabel * lableTitle = [cell viewWithTag:101];
        lableTitle.text = arrayTableData[indexPath.section][@"data"][indexPath.row][@"title"];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    viewHeader.backgroundColor = self.tableViewSettingMenu.backgroundColor;
    UILabel * lableTitle = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, SCREEN_WIDTH - 10, 30)];
    lableTitle.textColor = [UIColor blackColor];
    [lableTitle setFont:[UIFont fontWithName:@"Avenir Next" size:15]];
    lableTitle.text = arrayTableData[section][@"title"];
    [viewHeader addSubview:lableTitle];
    return viewHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return 100;
    } else if (indexPath.section == 1){
        return 65;
    } else {
        return 50;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if (indexPath.section == 0){
        ProfileViewController *obj = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        obj.isModeInitialUpdate = false;
        obj.canGoBack = true;
        [self.navigationController pushViewController:obj animated:true];
    } else if (indexPath.section == 1){
        
        BOOL isLocationServiceOn = [APP_DELEGATE CheckLocationPermission];
        if (isLocationServiceOn)
        {
            RunningRideViewController * runningRideView = [self.storyboard instantiateViewControllerWithIdentifier:@"RunningRideViewController"];
            runningRideView.isModeSearchLocation = true;
            runningRideView.delegate = self;
            if (indexPath.row == 0){
                runningRideView.searchLocationMode= SLM_SET_HOME_LOCATION;
                isModeSetHome= true;
            } else {
                runningRideView.searchLocationMode= SLM_SET_WORK_LOCATION;
                isModeSetHome= false;
            }
            [APP_DELEGATE.globalNavigation presentViewController:runningRideView animated:true completion:nil];
        
        }
            
      
    } else if (indexPath.section == 2){
        [APP_DELEGATE logoutTheUserFromAppWithAlert:true];
    }
}

- (IBAction)deleteButtonTapped:(UIButton *)sender {
    if ([sender.restorationIdentifier isEqualToString:@"HOME"]){
        [self shouldShowAlertForDeleteHome:true];
    } else {
        [self shouldShowAlertForDeleteHome:false];
    }
}

#pragma mark - AlertViewControllerDelegate
-(void)shouldShowAlertForDeleteHome:(BOOL)isModeDeleteHome{
    AlertViewController * alertView = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewController"];
    alertView.delegate = self;
    alertView.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    if (isModeDeleteHome){
        isModeSetHome = true;
        
        alertView.alertMessage = SET_ALERT_TEXT(@"delete_message_home");
    } else {
        isModeSetHome = false;
        alertView.alertMessage = SET_ALERT_TEXT(@"delete_message_work");
    }
    
    alertView.alertTitle = SET_ALERT_TEXT(@"are_you_sure");
    alertView.okayTitle = SET_ALERT_TEXT(@"yes_capital");
    alertView.cancelTitle = SET_ALERT_TEXT(@"no_capital");;
    alertView.canDismissTap = true;
    alertView.isModeWithTextView = false;
    [self presentViewController:alertView animated:true completion:nil];
}
-(void)alertControllerOkayButtonTappedWithTextViewText:(NSString *)textViewText{
    if (isModeSetHome){
        APP_DELEGATE.dictHomeInfo = @{
                                      @"lattitude":@"00.00000",
                                      @"longitude":@"00.00000",
                                      @"address":@""};
    } else {
        APP_DELEGATE.dictWorkInfo = @{
                                      @"lattitude":@"00.00000",
                                      @"longitude":@"00.00000",
                                      @"address":@""};
    }
    [self loadDataForTheView];
}

-(void)alertControllerCancelButtonTapped{
    //Cancel button
}


#pragma mark - RunningRideViewControllerDelegate
-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo{
    if (DEBUG_MODE){
        NSLog(@"Selected Location : %@",locationInfo);
    }
    
    if (isModeSetHome){
        APP_DELEGATE.dictHomeInfo = locationInfo;
    } else {
        APP_DELEGATE.dictWorkInfo = locationInfo;
    }
    [self loadDataForTheView];
}
@end
