//
//  PaymentViewController.h
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CardIO/CardIO.h>

#import "AlertViewController.h"

@protocol PaymentViewControllerDelegate <NSObject>
@optional
-(void)selectedPaymentMethodWithData:(NSDictionary *)dictPaymentMethod;
@end

@interface PaymentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CardIOPaymentViewControllerDelegate, AlertViewControllerDelegate>{
    NSMutableArray * arrayTableData;
    int selectedIndex;
    BOOL isModeReplaceCard;
    NSDictionary * dictNewCardDetails;
}

@property (weak, nonatomic) IBOutlet UILabel *lableTitle;

@property (weak, nonatomic) IBOutlet UITableView *tableViewPayments;
@property BOOL isModeSelection;
@property id <PaymentViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *buttonDoneSelection;
@property (weak, nonatomic) IBOutlet UIButton *buttonAddNewCard;
@end
