//
//  PaymentViewController.m
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "PaymentViewController.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    arrayTableData = [[NSMutableArray alloc]initWithArray:APP_DELEGATE.arrayPaymentTypes];
    
    selectedIndex = (int)[arrayTableData indexOfObject:APP_DELEGATE.dictDefaultPayment];
    
    if (!selectedIndex || selectedIndex > arrayTableData.count){
        selectedIndex = 0;
    }
    
    self.buttonDoneSelection.hidden = !self.isModeSelection;
    self.tableViewPayments.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableViewPayments reloadData];
    
    self.lableTitle.text = SET_ALERT_TEXT(@"payment");
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTapAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)doneSelectionButtonTapAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
    [self.delegate selectedPaymentMethodWithData:arrayTableData[selectedIndex]];
    APP_DELEGATE.dictDefaultPayment = arrayTableData[selectedIndex];
}
- (IBAction)addNewCardButtonTapped:(id)sender {
    
    [UIImagePickerController obtainPermissionForMediaSourceType:UIImagePickerControllerSourceTypeCamera withSuccessHandler:^{
        CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
        scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:scanViewController animated:YES completion:nil];
    } andFailure:^{
        UIAlertController *alertController= [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:NSLocalizedString(SET_ALERT_TEXT(@"you_have_disable_camara_access"), nil)
                                             preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Open Settings", @"Camera access denied: open the settings app to change privacy settings")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action) {
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
                                    }]
         ];
        
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:NSLocalizedString(SET_ALERT_TEXT(@"enter_manually"), SET_ALERT_TEXT(@"enter_manually"))
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * _Nonnull action) {
                                        CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
                                        scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
                                        [self presentViewController:scanViewController animated:YES completion:nil];
                                    }]];
        
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:NSLocalizedString(SET_ALERT_TEXT(@"cancel"), SET_ALERT_TEXT(@"cancel"))
                                    style:UIAlertActionStyleDefault
                                    handler:NULL]
         
         ];
        
        [self presentViewController:alertController animated:YES completion:^{}];
    }];
    
    
}
#pragma mark - UITableViewDataSource&Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayTableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellDetails"];
    
    UIImageView * imageViewIcon = [cell viewWithTag:101];
    UILabel * lableTitle = [cell viewWithTag:102];
    UILabel * lableDetails = [cell viewWithTag:103];
    UIImageView * imageViewCheckMArk = [cell viewWithTag:104];
    
    
    lableTitle.text = arrayTableData[indexPath.row][@"title"];
    if (indexPath.row == 0){
        lableDetails.text = @"";
    } else {
        lableDetails.text = arrayTableData[indexPath.row][@"detail"];
    }
    
    imageViewIcon.image = [UIImage imageNamed:arrayTableData[indexPath.row][@"image"]];
    
    if (self.isModeSelection){
        if (indexPath.row == selectedIndex){
            imageViewCheckMArk.image = [UIImage imageNamed:@"checked"];
        } else {
            imageViewCheckMArk.image = [UIImage imageNamed:@"unchecked"];
        }
    } else if (indexPath.row > 0){
        imageViewCheckMArk.image = [UIImage imageNamed:@"PHDelete"];
    } else {
        imageViewCheckMArk.hidden = true;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    viewHeader.backgroundColor = self.tableViewPayments.backgroundColor;
    UILabel * lableTitle = [[UILabel alloc]initWithFrame:CGRectMake(5, 20, SCREEN_WIDTH - 10, 30)];
    lableTitle.textColor = [UIColor blackColor];
    [lableTitle setFont:[UIFont fontWithName:@"Avenir Next" size:15]];
    lableTitle.text = SET_ALERT_TEXT(@"payment_method");
    
    [viewHeader addSubview:lableTitle];
    return viewHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    int oldIndex = selectedIndex;
    selectedIndex = (int)indexPath.row;
    
    if (self.isModeSelection) {
        [self.tableViewPayments reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:oldIndex inSection:0], indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (indexPath.row > 0){
        [self shouldShowAlertWithModeReplaceCard:false];
    }
}

#pragma mark - CardIOPaymentViewControllerDelegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    if (DEBUG_MODE){
        NSLog(@"Scan succeeded with info: %@",info.cardNumber);
        NSLog(@"Scan succeeded with info: %@",info.cvv);
        NSLog(@"Scan succeeded with info: %lu",(unsigned long)info.expiryYear);
        NSLog(@"Scan succeeded with info: %ld",(long)info.redactedCardNumber);
        NSLog(@"Scan succeeded with info: %@",info.redactedCardNumber);
    }
    
    dictNewCardDetails = @{
                           @"type":@"card",
                           @"payment_type" : @"2",
                           @"title":info.redactedCardNumber,
                           @"detail":[NSString stringWithFormat:@"Will Expire on %lu / %lu",(unsigned long)info.expiryMonth, (unsigned long)info.expiryYear],
                           @"image":@"cardPayment",
                           @"card_number":info.cardNumber,
                           @"expiry_month":[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryMonth],
                           @"expiry_year":[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear]
                           };
    
    NSString * cardNumber = info.cardNumber;
    BOOL isFound = false;
    for (NSDictionary * cards in arrayTableData){
        
        NSString * existingNumber = cards[@"card_number"];
        if ([existingNumber isEqualToString:cardNumber]){
            selectedIndex = (int)[arrayTableData indexOfObject:cards];
            [self dismissViewControllerAnimated:YES completion:^{
                [self shouldShowAlertWithModeReplaceCard:true];
            }];
            isFound = true;
            break;
        }
    }
    
    if (isFound == false){
        [arrayTableData addObject:dictNewCardDetails];
        APP_DELEGATE.arrayPaymentTypes = arrayTableData;
        [self.tableViewPayments reloadData];
        [self dismissViewControllerAnimated:YES completion:^{ }];
    }
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    if (DEBUG_MODE){
        NSLog(@"User cancelled scan");
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AlertViewControllerDelegate
-(void)shouldShowAlertWithModeReplaceCard:(BOOL)isModeReplace {
    AlertViewController * alertView = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewController"];
    alertView.delegate = self;
    alertView.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    if (isModeReplace){
        alertView.alertTitle = SET_ALERT_TEXT(@"this_card_already_added");
        alertView.alertMessage = SET_ALERT_TEXT(@"replace_card_message");
        alertView.okayTitle = SET_ALERT_TEXT(@"replace_capital");
        alertView.cancelTitle = SET_ALERT_TEXT(@"cancel_capital");
        alertView.canDismissTap = false;
        alertView.isModeWithTextView = false;
        isModeReplaceCard = true;
    } else {
        alertView.alertTitle = SET_ALERT_TEXT(@"are_you_sure");
        alertView.alertMessage = SET_ALERT_TEXT(@"are_you_sure_to_delete_payment_method_permanent");
        alertView.okayTitle = SET_ALERT_TEXT(@"delete_capital");
        alertView.cancelTitle = SET_ALERT_TEXT(@"cancel_capital");
        alertView.canDismissTap = true;
        alertView.isModeWithTextView = false;
        isModeReplaceCard = false;
    }
    
    [self presentViewController:alertView animated:true completion:nil];
}
-(void)alertControllerOkayButtonTappedWithTextViewText:(NSString *)textViewText{
    
    if (isModeReplaceCard){
        if (selectedIndex < arrayTableData.count){
            [arrayTableData replaceObjectAtIndex:selectedIndex withObject:dictNewCardDetails];
        }
        
        APP_DELEGATE.arrayPaymentTypes = arrayTableData;
        [self.tableViewPayments reloadData];
    } else {
        NSDictionary * dictRemovableDict = arrayTableData[selectedIndex];
        if (dictRemovableDict == APP_DELEGATE.dictDefaultPayment){
            APP_DELEGATE.dictDefaultPayment = arrayTableData[0];
        }
        
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        [arrayTableData removeObjectAtIndex:indexPath.row];
        APP_DELEGATE.arrayPaymentTypes = arrayTableData.mutableCopy;
        [self.tableViewPayments deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
}

-(void)alertControllerCancelButtonTapped{
    if (isModeReplaceCard){
        [self dismissViewControllerAnimated:YES completion:^{ }];
    }
}

@end
