//
//  PaymentViewController.h
//  VCab
//
//  Created by Vishal Gohil on 19/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripDetailViewController.h"
@interface YourTripsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray * arrayPastTrips;
    NSMutableArray * arrayUpcomingTrips;
    NSArray * arrayTableResource;
    BOOL isModePastTrips;
    BOOL isGettingList;
    int initialRequestCounts;
}
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentDisplayModes;
@property (weak, nonatomic) IBOutlet UILabel *lableNoRecordMessage;
@property (weak, nonatomic) IBOutlet UITableView *tableViewTripList;
@end
