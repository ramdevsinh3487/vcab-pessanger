//
//  PaymentViewController.m
//  VCab
//
//  Created by Vishal Gohil on 19/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "YourTripsViewController.h"

@interface YourTripsViewController ()

@end

@implementation YourTripsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    initialRequestCounts = 0;
    isGettingList = true;
    isModePastTrips = true;
    arrayPastTrips = [[NSMutableArray alloc]init];
    arrayUpcomingTrips = [[NSMutableArray alloc]init];
    arrayTableResource = [[NSArray alloc]init];
    self.tableViewTripList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.segmentDisplayModes setTitle:SET_ALERT_TEXT(@"history") forSegmentAtIndex:0];
    [self.segmentDisplayModes setTitle:SET_ALERT_TEXT(@"upcoming") forSegmentAtIndex:1];
    self.lableTitle.text = SET_ALERT_TEXT(@"your_trip");
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self updateHistoryTripList];
}

-(void)updateHistoryTripList{
    
    if (initialRequestCounts <= 2){
        [self.tableViewTripList reloadData];
        [APP_DELEGATE.apiManager getTripHistoryForCurrentUserWithCallBack:^(BOOL success, NSArray *response, NSString *errorMessage)
         {
             
             [arrayPastTrips removeAllObjects];
             if (success){
                 arrayPastTrips = [[NSMutableArray alloc]initWithArray:response];
                 
                 if (arrayPastTrips.count > 0){
                     isGettingList = false;
                     arrayTableResource = arrayPastTrips.mutableCopy;
                     [self.tableViewTripList reloadData];
                 } else {
                     initialRequestCounts = initialRequestCounts + 1;
                     [self updateHistoryTripList];
                 }
                 if (DEBUG_MODE){
                     NSLog(@"Trip History is reloaded : %@",arrayTableResource);
                 }
             }
         }];
    } else {
        isGettingList = false;
        [arrayPastTrips removeAllObjects];
        arrayTableResource = [[NSArray alloc]init];
        [self.tableViewTripList reloadData];
    }
    
    
    [self.tableViewTripList reloadData];
    [APP_DELEGATE.apiManager getTripHistoryForCurrentUserWithCallBack:^(BOOL success, NSArray *response, NSString *errorMessage)
     {
        [arrayPastTrips removeAllObjects];
        if (success){
            arrayPastTrips = [[NSMutableArray alloc]initWithArray:response];
            arrayTableResource = arrayPastTrips.mutableCopy;
            if (DEBUG_MODE){
                NSLog(@"Trip History is reloaded : %@",arrayTableResource);
            }
        }
        isGettingList = false;
        [self.tableViewTripList reloadData];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)segmentValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0){
        arrayTableResource = arrayPastTrips;
        isModePastTrips = true;
    } else {
        arrayTableResource = arrayUpcomingTrips;
        isModePastTrips = false;
    }
    [self.tableViewTripList reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (arrayTableResource.count == 0){
        [self.tableViewTripList setHidden:true];
        [self.lableNoRecordMessage setHidden:false];
        
        if (isGettingList){
            self.lableNoRecordMessage.text = SET_ALERT_TEXT(@"loading_data_please_wait");
        } else {
            if (self.segmentDisplayModes.selectedSegmentIndex == 0){
                self.lableNoRecordMessage.text = SET_ALERT_TEXT(@"no_data_found");
            } else {
                self.lableNoRecordMessage.text = SET_ALERT_TEXT(@"coming_soon");
            }
        }
    } else {
        [self.tableViewTripList setHidden:false];
        [self.lableNoRecordMessage setHidden:true];
    }
    return arrayTableResource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellTrip"];
    UILabel * lableDate = [cell viewWithTag:101];
    UILabel * lableCar = [cell viewWithTag:102];
    UIImageView * imageViewMap = [cell viewWithTag:103];
    UILabel * lableCharges = [cell viewWithTag:104];
    UILabel * lableRideStatus = [cell viewWithTag:105];
    UILabel * lableMapshotStatus = [cell viewWithTag:106];
    
    NSString * dateToConvert = arrayTableResource[indexPath.row][@"trip_starttime"];
    
    NSDateFormatter * dateFormater = [[NSDateFormatter alloc]init];
    dateFormater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate * startDate = [dateFormater dateFromString:dateToConvert];
    dateFormater.dateFormat = @"dd MMM yyyy HH:mm";
    NSString * strStartDate = [dateFormater stringFromDate:startDate];
    
    lableDate.text = strStartDate;
    lableCar.text = arrayTableResource[indexPath.row][@"brand_name"];
    
    NSString * totalCost = arrayTableResource[indexPath.row][@"trip_totalcost"];
    lableCharges.text = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol, totalCost];
    
    
    NSString * rideStatus = arrayTableResource[indexPath.row][@"ride_status"];
    if ([rideStatus isEqualToString:@"1"]){
        lableRideStatus.text = SET_ALERT_TEXT(@"accepted");
        lableMapshotStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    } else if ([rideStatus isEqualToString:@"2"]){
        lableRideStatus.text = SET_ALERT_TEXT(@"arrived");
        lableMapshotStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    } else if ([rideStatus isEqualToString:@"3"]){
        lableRideStatus.text = SET_ALERT_TEXT(@"on_going");
        lableMapshotStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    } else if ([rideStatus isEqualToString:@"4"]){
        lableRideStatus.text = SET_ALERT_TEXT(@"cancelled");
        lableMapshotStatus.text = SET_ALERT_TEXT(@"loading");
    } else if ([rideStatus isEqualToString:@"5"]){
        lableRideStatus.text = SET_ALERT_TEXT(@"finished");
        lableMapshotStatus.text = SET_ALERT_TEXT(@"loading");
    } else if ([rideStatus isEqualToString:@"6"]){
        lableRideStatus.text = SET_ALERT_TEXT(@"declined");
        lableMapshotStatus.text = SET_ALERT_TEXT(@"no_preview_available");
        lableCar.text = @"";
    } else {
        lableRideStatus.text = SET_ALERT_TEXT(@"request_send");
        lableMapshotStatus.text = SET_ALERT_TEXT(@"no_preview_available");
        lableCar.text = @"";
    }
    
    NSString * imageURL= arrayTableResource[indexPath.row][@"map_screenshot"];
    lableMapshotStatus.text = SET_ALERT_TEXT(@"loading");
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imageViewMap.frame.size.width, imageViewMap.frame.size.height)];
    [imageViewMap addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    [imageViewMap sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"PHMapshot"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        
        if (!image){
            lableMapshotStatus.text = SET_ALERT_TEXT(@"no_preview_available");
        } else {
            lableMapshotStatus.text = @"";
        }
    }];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    TripDetailViewController * tripDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"TripDetailViewController"];
    tripDetails.trip_id = arrayTableResource[indexPath.row][@"id"];
    tripDetails.isModeSubmit = false;
    UINavigationController * navController =[[UINavigationController alloc]initWithRootViewController:tripDetails];
    [self presentViewController:navController animated:true completion:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 235;
}

@end
