//
//  RunningRideViewController.h
//  VCab
//
//  Created by Vishal Gohil on 14/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>



@protocol RunningRideViewControllerDelegate <NSObject>

@optional
-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo;

@end

typedef enum SEARCH_LOCATION_MODE{
    SLM_SET_START_LOCATION,//0
    SLM_SET_END_LOCATION,//1
    SLM_SET_HOME_LOCATION,//2
    SLM_SET_WORK_LOCATION,//3
    SLM_CHANGE_DESTINATION,//4
}SearchLocationMode;

@interface RunningRideViewController : UIViewController <GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, UISearchBarDelegate>{
    
    NSString * beginLocationTitle;
    NSString * beginLocationDetails;
    
    NSString * destinationLocationTitle;
    NSString * destinationLocationDetails;
    
    CLLocationCoordinate2D dropedPinLocation;
    NSTimer * timerFindAddress;
    
    NSTimer * timerToShowViews;
    
    BOOL isAnimationStarted;
    BOOL isModeSetHomeLocal;
    BOOL isModeSetWorkLocal;
    
    BOOL isFromAutoComplete;
}

@property id <RunningRideViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewDestinationDropPin;
@property (weak, nonatomic) IBOutlet UIButton *buttonTrackMyLocation;
@property (weak, nonatomic) IBOutlet UIView *viewTrackMyLocationShadow;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarSearchForPlace;

@property (weak, nonatomic) IBOutlet GMSMapView *viewGMSMapView;
@property (weak, nonatomic) IBOutlet UIButton *buttonMyLocation;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UIView *viewRdieDetailsBanner;
@property (weak, nonatomic) IBOutlet UIView *viewRunningRideInfoContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRunningIcon;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverRating;
@property (weak, nonatomic) IBOutlet UILabel *lableCarBrandModelName;
@property (weak, nonatomic) IBOutlet UILabel *lableCarPlateNumber;

@property (weak, nonatomic) IBOutlet UIButton *buttonTripDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *runningRideBannerBottomSpace;

@property (weak, nonatomic) IBOutlet UIView *viewHomeWorkBanner;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *homeWorkBannerBottomSpace;
@property (weak, nonatomic) IBOutlet UIButton *buttonHome;
@property (weak, nonatomic) IBOutlet UILabel *lableHome;

@property (weak, nonatomic) IBOutlet UIButton *buttonWork;
@property (weak, nonatomic) IBOutlet UILabel *lableWork;

@property (weak, nonatomic) IBOutlet UIView *viewChangeDestinationBanne;
@property (weak, nonatomic) IBOutlet UILabel *lableChangeDestinationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableChangeDestinationDetails;
@property (weak, nonatomic) IBOutlet UIButton *buttonChangeDestinationDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonChangeDestinationCancel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *changeDestinationBottomSpace;

@property BOOL isModeSearchLocation;
@property SearchLocationMode searchLocationMode;
@end
