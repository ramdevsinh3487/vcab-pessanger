//
//  RunningRideViewController.m
//  VCab
//
//  Created by Vishal Gohil on 14/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "RunningRideViewController.h"

@interface RunningRideViewController ()

@end

@implementation RunningRideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    isAnimationStarted = false;
    
    [self loadMapView];
    
    self.searchBarSearchForPlace.placeholder = SET_ALERT_TEXT(@"search");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self restoreGoogleMapWithMarkersAndRoutes];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(rideStatusUpdatedNotification) name:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
    
    
    [self shouldShowSelectWorkHome:(self.searchLocationMode != SLM_SET_WORK_LOCATION && self.searchLocationMode != SLM_SET_HOME_LOCATION)];
    
    double homeLattitude = [APP_DELEGATE.dictHomeInfo[@"lattitude"]doubleValue];
    double homeLongitude = [APP_DELEGATE.dictHomeInfo[@"longitude"]doubleValue];
    
    double workLattitude = [APP_DELEGATE.dictWorkInfo[@"lattitude"]doubleValue];
    double workLongitude = [APP_DELEGATE.dictWorkInfo[@"longitude"]doubleValue];
    
    if (homeLattitude != 00.00000 && homeLongitude != 00.00000){
        self.lableHome.text = SET_ALERT_TEXT(@"home");

    } else {
        self.lableHome.text = SET_ALERT_TEXT(@"add_home");
    }
    
    if (workLattitude != 00.00000 && workLongitude != 00.00000){
        self.lableWork.text = @"Work";
    } else {
        self.lableWork.text = SET_ALERT_TEXT(@"add_work");
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initializeRunningRideBannerView];
    
    [self manageViewInitially];
}

-(void)rideStatusUpdatedNotification{
    if (self.isModeSearchLocation){
        if (APP_DELEGATE.tripStatus == RIDE_STATUS_CANCELED || APP_DELEGATE.tripStatus == RIDE_STATUS_FINISHED){
            [self dismissViewControllerAnimated:true completion:^{
                if (self.searchLocationMode != SLM_SET_HOME_LOCATION && self.searchLocationMode != SLM_SET_WORK_LOCATION){
                    [self dismissViewControllerAnimated:true completion:nil];
                } else {
                    [APP_DELEGATE.globalNavigation popViewControllerAnimated:true];
                }
            }];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)trackMyLOcationTApped:(id)sender {
    [self.viewGMSMapView animateToLocation: self.viewGMSMapView.myLocation.coordinate];
}

- (IBAction)backButtonTapAction:(id)sender {
//    [self.navigationController popViewControllerAnimated:true];
    [self dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - RunningRideDetailsBanner
-(void)initializeRunningRideBannerView {
    
    self.viewRunningRideInfoContainer.layer.cornerRadius = self.viewRunningRideInfoContainer.frame.size.height / 2;
    self.viewRunningRideInfoContainer.layer.masksToBounds = true;
    
    self.imageViewRunningIcon.layer.cornerRadius = self.imageViewRunningIcon.frame.size.height / 2;
    self.imageViewRunningIcon.layer.masksToBounds = true;
}

-(void)shouldShowRunningRideInfoBannerShouldShow:(BOOL)shouldShow{
    if (shouldShow){
        
        if (APP_DELEGATE.dictTripDriverDetails.count <= 0 || APP_DELEGATE.dictTripDriverDetails == nil || [APP_DELEGATE.dictTripDriverDetails isKindOfClass:[NSNull class]]){
            return;
        }
        
        NSString * driverImageURL = APP_DELEGATE.dictTripDriverDetails[@"driver_user_image"];
        
        if ([self.imageViewRunningIcon viewWithTag:1111]){
            [(UIActivityIndicatorView *)[self.imageViewRunningIcon viewWithTag:1111]stopAnimating];
            [(UIActivityIndicatorView *)[self.imageViewRunningIcon viewWithTag:1111] removeFromSuperview];
        }
        
        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.imageViewRunningIcon.frame.size.width, self.imageViewRunningIcon.frame.size.height)];
        activity_indicator.tag = 1111;
        [self.imageViewRunningIcon addSubview:activity_indicator];
        [activity_indicator startAnimating];
        [activity_indicator setColor:[UIColor blackColor]];
        [self.imageViewRunningIcon sd_setImageWithURL:[NSURL URLWithString:driverImageURL] placeholderImage:[UIImage imageNamed:@"PHProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activity_indicator stopAnimating];
            [activity_indicator removeFromSuperview];
        }];
        
        self.lableDriverName.text = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictTripDriverDetails[@"driver_first_name"],APP_DELEGATE.dictTripDriverDetails[@"driver_last_name"]];
        self.lableCarBrandModelName.text = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictDefaultBrand[@"car_brand_name"],APP_DELEGATE.dictTripDriverDetails[@"car_model"]];
        self.lableCarPlateNumber.text = APP_DELEGATE.dictTripDriverDetails[@"car_plat_no"];
        NSString * driverRating = APP_DELEGATE.dictTripDriverDetails[@"driver_ratings"];
        if ([driverRating isEqualToString:@"0"]){
            self.lableDriverRating.text = @"";
        } else {
            self.lableDriverRating.text = [NSString stringWithFormat:@"%@ ★", APP_DELEGATE.dictTripDriverDetails[@"driver_ratings"]];
        }
    
        if(self.runningRideBannerBottomSpace.constant < 1){
            self.runningRideBannerBottomSpace.constant = 1;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
        }
    } else {
        if(self.runningRideBannerBottomSpace.constant > -75){
            self.runningRideBannerBottomSpace.constant = -75;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
        }
    }
    [self handleViewShadow];
}

- (IBAction)buttonShowRunningRideDetailsTap:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        UIViewController *controller = APP_DELEGATE.globalNavigation.viewControllers.lastObject;
        if (![controller isKindOfClass:[HomeViewController class]]) {
            if (self.isModeSearchLocation && self.searchLocationMode ==SLM_CHANGE_DESTINATION){
                [APP_DELEGATE.globalNavigation dismissViewControllerAnimated:true completion:nil];
            } else {
                [APP_DELEGATE.globalNavigation popViewControllerAnimated:true];
            }
        }
    }];
}

- (IBAction)contactButtonTapAction:(id)sender {
    NSString * mobileNumber = APP_DELEGATE.dictTripDriverDetails[@"driver_mobile_no"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",mobileNumber]]];
}


#pragma mark - GoogleSDKSection
#pragma mark ******************
#pragma mark - GoogleMapViewInitializeAndUserOperations
- (void)loadMapView {
    GMSCameraPosition *camera;
    
    double lastSelectedLattitude = [APP_DELEGATE.dictLastSelectedLocation[@"lattitude"] doubleValue];
    double lastSelectedLongitude = [APP_DELEGATE.dictLastSelectedLocation[@"longitude"] doubleValue];
    if (lastSelectedLattitude != 00.00000 && lastSelectedLongitude != 00.00000){
        camera = [GMSCameraPosition cameraWithLatitude:lastSelectedLattitude longitude:lastSelectedLongitude zoom:18];
        
        self.searchBarSearchForPlace.text = APP_DELEGATE.dictLastSelectedLocation[@"title"];
        self.lableChangeDestinationDetails.text = APP_DELEGATE.dictLastSelectedLocation[@"address"];
        isFromAutoComplete = true;
    } else if (APP_DELEGATE.userLocation.latitude != 00.00000 && APP_DELEGATE.userLocation.longitude != 00.00000){
        camera = [GMSCameraPosition cameraWithLatitude:APP_DELEGATE.userLocation.latitude longitude:APP_DELEGATE.userLocation.longitude zoom:18];
    } else {
        camera = [GMSCameraPosition cameraWithLatitude:23.0752 longitude:72.5257 zoom:18];
    }
    
    self.viewGMSMapView.camera = camera;
    if (APP_DELEGATE.userLocation.latitude != 0 && APP_DELEGATE.userLocation.longitude != 0){
        self.viewGMSMapView.myLocationEnabled = true;
    } else {
        self.viewGMSMapView.myLocationEnabled = false;
    }
    
    self.viewGMSMapView.delegate = self;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationReceivedForUserLocationUpdates:) name:NOTI_LOCATION_UPDATES object:nil];
}

-(void)restoreGoogleMapWithMarkersAndRoutes{
    if (self.searchLocationMode ==SLM_CHANGE_DESTINATION){
        [self.viewGMSMapView clear];
        if (APP_DELEGATE.tripStartLocation.latitude > 00.000 && APP_DELEGATE.tripStartLocation.longitude > 00.000){
            [self findAddressForCoordinate:APP_DELEGATE.tripStartLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
                MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
                if (MKMetersBetweenMapPoints(point1, point2) < 15.0){
                    beginLocationTitle = SET_ALERT_TEXT(@"my_location");
                } else {
                    beginLocationTitle = SET_ALERT_TEXT(@"pickup_location");
                }
                beginLocationDetails = dictAddress[@"fullAddress"];
                [self addStartMarkerWithTitle:beginLocationTitle];
            }];
        }
        
        if (self.isModeSearchLocation && self.searchLocationMode == SLM_CHANGE_DESTINATION){
            if (APP_DELEGATE.tripEndLocation.latitude > 00.000 && APP_DELEGATE.tripEndLocation.longitude > 00.000){
                [self.viewGMSMapView animateToLocation:APP_DELEGATE.tripEndLocation];
                [self findAddressForCoordinate:APP_DELEGATE.tripEndLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                    destinationLocationTitle = SET_ALERT_TEXT(@"destination_location");
                    destinationLocationDetails = dictAddress[@"fullAddress"];
                }];
            }
        } else {
            if (APP_DELEGATE.tripEndLocation.latitude > 00.000 && APP_DELEGATE.tripEndLocation.longitude > 00.000){
                [self findAddressForCoordinate:APP_DELEGATE.tripEndLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                    destinationLocationTitle = SET_ALERT_TEXT(@"destination_location");
                    destinationLocationDetails = dictAddress[@"fullAddress"];
                    [self addEndMarkerWithTitle:destinationLocationTitle];
                }];
            }
        }
        
        if (APP_DELEGATE.tripStartLocation.latitude > 00.000 && APP_DELEGATE.tripStartLocation.longitude > 00.000 && APP_DELEGATE.tripEndLocation.latitude > 00.000 && APP_DELEGATE.tripEndLocation.longitude > 00.000){
            [self adjustMapviewToViewAllMarkers];
            [self drawRouteWithMapViewForOrigin:APP_DELEGATE.tripStartLocation Destination:APP_DELEGATE.tripEndLocation withCallback:^(BOOL success) {
                //Draw root completed
            }];
        }
    }
}

//-(void)addLocationMarkerWithTitle:(NSString *)title atLocation:(CLLocationCoordinate2D)location{
//    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
//    markerToAdd.position = location;
//    markerToAdd.title = title;
////    if (self.searchLocationMode == SLM_SET_HOME_LOCATION || ){
////        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerHome"];
////    } else {
////        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerWork"];
////    }
//    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerWork"];
//    markerToAdd.map = self.viewGMSMapView;
//}

-(void)addStartMarkerWithTitle:(NSString *)title{
    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
    markerToAdd.position = APP_DELEGATE.tripStartLocation;
    markerToAdd.title = title;
    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerPickup"];
    markerToAdd.map = self.viewGMSMapView;
}

-(void)addEndMarkerWithTitle:(NSString *)title{
    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
    markerToAdd.position = APP_DELEGATE.tripEndLocation;
    markerToAdd.title = title;
    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerDestination"];
    markerToAdd.map = self.viewGMSMapView;
}

-(void)notificationReceivedForUserLocationUpdates:(NSNotification *)notif{
    [self.viewGMSMapView setMyLocationEnabled:true];
    self.viewGMSMapView.myLocationEnabled = true;
}

-(void)adjustMapviewToViewAllMarkers{
    GMSMutablePath *pathMapPath = [GMSMutablePath path];
    if (APP_DELEGATE.tripStartLocation.latitude > 00.000 && APP_DELEGATE.tripStartLocation.longitude > 00.000){
        [pathMapPath addCoordinate:APP_DELEGATE.tripStartLocation];
    }
    
    if ( APP_DELEGATE.tripEndLocation.latitude > 00.000 && APP_DELEGATE.tripEndLocation.longitude > 00.000){
        [pathMapPath addCoordinate:APP_DELEGATE.tripEndLocation];
    }
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathMapPath];
    [self.viewGMSMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
}

-(void)findAddressForCoordinate:(CLLocationCoordinate2D)location WithCallback:(void (^) (BOOL success, NSDictionary * dictAddress))callback{
    [APP_DELEGATE.apiManager findAddressUsingLocationCoordinate:location withCallBack:^(BOOL success, NSDictionary *result, NSString *error) {
        if (success){
            NSString *title;
            if (isFromAutoComplete){
                title = self.searchBarSearchForPlace.text;
            } else{
                title = result[@"title"];
            }
            callback(true, @{@"title":title,@"fullAddress":result[@"fullAddress"]});
        } else {
            [[GMSGeocoder geocoder] reverseGeocodeCoordinate:location completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
                if (response.results.count > 0){
                    
                    NSLog(@"%@",response);
                    
                    GMSAddress* addressObj =  response.firstResult;
                    NSString * fullAddress = @"";
                    for (NSString * title in addressObj.lines){
                        fullAddress = [fullAddress stringByAppendingString:title];
                    }
                    
                    NSLog(@"%@", addressObj.lines);
                    
                    NSString *title;
                    if (isFromAutoComplete){
                        title = self.searchBarSearchForPlace.text;
                    } else if (addressObj.thoroughfare){
                        title = addressObj.thoroughfare;
                    } else{
                        title = addressObj.addressLine1;
                    }
                    
                    callback(true, @{@"title":title,@"fullAddress":fullAddress});
                } else {
                    callback(false, @{@"title":@"Current Location",@"fullAddress":@""});
                }
            }];
        }
    }];
}

-(void)drawRouteWithMapViewForOrigin:(CLLocationCoordinate2D)origin Destination:(CLLocationCoordinate2D)destination withCallback:(void (^) (BOOL success))callback{
    START_HUD
    [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:origin destination:destination withCallBack:^(BOOL success, NSArray *result, NSString * error) {
        if (success){
            for (NSDictionary *routeDict in result){
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                
                GMSPolyline *polyline = [GMSPolyline polylineWithPath: [GMSPath pathFromEncodedPath: points]];
                
                polyline.map = self.viewGMSMapView;
                polyline.strokeColor = [UIColor blackColor];
                
                polyline.strokeWidth = 2.0f;
                
                APP_DELEGATE.tripDistance = [[routeDict objectForKey:@"legs"][0][@"distance"][@"value"]floatValue] / 1000;
                APP_DELEGATE.tripDuration = [[routeDict objectForKey:@"legs"][0][@"duration"][@"value"]floatValue] / 60;
            }
            callback(success);
        } else {
            SHOW_ALERT_WITH_CAUTION(error);
            callback(false);
        }
        STOP_HUD
    }];
}

-(void)findAddressForDropedPinLocation {
    
    CGPoint pointToSet = CGPointMake(self.imageViewDestinationDropPin.center.x,self.imageViewDestinationDropPin.center.y + 25);
    
    dropedPinLocation = [self.viewGMSMapView.projection coordinateForPoint:pointToSet];
    
    [self findAddressForCoordinate:dropedPinLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
        if (success){
            if (self.imageViewDestinationDropPin.isHidden == false){
                [self shouldShowChangeDestinationBanner:true];
            }
            
            if (self.searchLocationMode == SLM_SET_HOME_LOCATION || isModeSetHomeLocal){
                self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"add_home");
                
            } else if (self.searchLocationMode == SLM_SET_WORK_LOCATION || isModeSetWorkLocal){
                self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"add_work");
            } else if (self.searchLocationMode == SLM_CHANGE_DESTINATION){
                self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"destination");
            } else if (self.searchLocationMode == SLM_SET_START_LOCATION){
                self.lableChangeDestinationTitle.text = @"Pickup";
            } else if (self.searchLocationMode == SLM_SET_END_LOCATION){
                self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"destination");
            }
        
            
            if (!isFromAutoComplete){
                self.searchBarSearchForPlace.text = dictAddress[@"title"];
                self.lableChangeDestinationDetails.text = dictAddress[@"fullAddress"];
            } else {
                isFromAutoComplete = false;
                self.lableChangeDestinationDetails.text =[NSString stringWithFormat:@"%@, %@",dictAddress[@"title"], dictAddress[@"fullAddress"]];
            }
        }
    }];
}

#pragma mark - GoogleMapDelegate
-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    if (self.isModeSearchLocation){
        [self shouldShowChangeDestinationBanner:false];
        [self shouldShowRunningRideInfoBannerShouldShow:false];
        [self shouldShowSelectWorkHome:(self.searchLocationMode != SLM_SET_WORK_LOCATION && self.searchLocationMode != SLM_SET_HOME_LOCATION)];
        
        dropedPinLocation = [mapView.projection coordinateForPoint:mapView.center];
        
        
        NSLog(@"-----Center Latitude%f",dropedPinLocation.latitude);
        NSLog(@"------Center Longitude%f",dropedPinLocation.longitude);
        
        
        [timerFindAddress invalidate];
        timerFindAddress = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(findAddressForDropedPinLocation) userInfo:nil repeats:false];
        
        [timerToShowViews invalidate];
        timerToShowViews = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(showNessesoryViewsForMapView) userInfo:nil repeats:false];
    } else {
        
    }
    
    GMSVisibleRegion region = self.viewGMSMapView.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    if (![bounds containsCoordinate:APP_DELEGATE.userLocation]) {
        if(isAnimationStarted == false){
            isAnimationStarted = true;
            [self buttonBlinkAnimationShouldStart];
        }
    } else {
        if (isAnimationStarted == true){
            isAnimationStarted = false;
        }
    }
}

-(void)buttonBlinkAnimationShouldStart{
    self.viewTrackMyLocationShadow.backgroundColor = [UIColor whiteColor];
    if (isAnimationStarted == true){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.viewTrackMyLocationShadow.backgroundColor = [UIColor cyanColor];
        });
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self buttonBlinkAnimationShouldStart];
        });
    } else {
        self.viewTrackMyLocationShadow.alpha = 1.0;
        self.viewTrackMyLocationShadow.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - GooglePlacePickerDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:false completion:^{
        self.imageViewDestinationDropPin.hidden = false;
    }];
    return false;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    
    dropedPinLocation = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
    self.searchBarSearchForPlace.text = place.name;
    isFromAutoComplete = true;
    
    [self.viewGMSMapView animateToLocation:dropedPinLocation];
    [self dismissViewControllerAnimated:false completion:nil];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:false completion:nil];
    // handle the error.
    if (DEBUG_MODE){
        NSLog(@"Error: %@", [error description]);
    }
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:false completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


#pragma mark - ChangeDestinationBanner
-(void)shouldShowChangeDestinationBanner:(BOOL)shouldShow{
    if (shouldShow){
        if (self.changeDestinationBottomSpace.constant < 1){
            self.changeDestinationBottomSpace.constant = 1;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    } else {
        if (self.changeDestinationBottomSpace.constant > -150){
            self.changeDestinationBottomSpace.constant = -150;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    }
    
}

- (IBAction)cancelButtonTapped:(id)sender {
    if (isModeSetHomeLocal || isModeSetWorkLocal){
        isModeSetWorkLocal = false;
        isModeSetHomeLocal = false;
        
        if (self.searchLocationMode == SLM_SET_START_LOCATION){
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerPickup"];
        } else if (self.searchLocationMode == SLM_SET_END_LOCATION){
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerDestination"];
        }
    }
    [self shouldShowChangeDestinationBanner:false];
}

- (IBAction)doneButtonTapped:(UIButton *)sender {
    if (self.isModeSearchLocation){
        NSString * lattitude = [NSString stringWithFormat:@"%f",dropedPinLocation.latitude];
        NSString * longitude = [NSString stringWithFormat:@"%f",dropedPinLocation.longitude];
        NSString * fullAddress = self.lableChangeDestinationDetails.text;
        NSString * title = self.searchBarSearchForPlace.text;
        
        
        NSLog(@"-----Center Latitude%f",dropedPinLocation.latitude);
        NSLog(@"------Center Longitude%f",dropedPinLocation.longitude);
        
        
        
        NSDictionary * dictInfo = @{
                                    @"lattitude":lattitude,
                                    @"longitude":longitude,
                                    @"address": fullAddress,
                                    @"title":title
                                    };
        APP_DELEGATE.dictLastSelectedLocation = dictInfo;
        
        if (isModeSetHomeLocal || isModeSetWorkLocal){
            [self shouldShowChangeDestinationBanner:true];

            if (isModeSetHomeLocal){
                APP_DELEGATE.dictHomeInfo = dictInfo;
                self.lableHome.text = SET_ALERT_TEXT(@"home");
            } else {
                APP_DELEGATE.dictWorkInfo = dictInfo;
                self.lableWork.text = SET_ALERT_TEXT(@"work");;
            }
            
            [self shouldShowChangeDestinationBanner:false];
            isModeSetHomeLocal = false;
            isModeSetWorkLocal = false;
            
            [self.buttonChangeDestinationDone setTitle:SET_ALERT_TEXT(@"done") forState:UIControlStateNormal];
            [self manageViewInitially];
        } else {
            self.imageViewDestinationDropPin.hidden = true;
            [self shouldShowChangeDestinationBanner:false];
            [self dismissViewControllerAnimated:true completion:^{
                [self.delegate didChangeLocationWithLocationInfo:dictInfo];
            }];
        }
    } else {
        self.changeDestinationBottomSpace.constant = -150;
        self.homeWorkBannerBottomSpace.constant = -75;
        if (!APP_DELEGATE.isTripRunning){
            self.runningRideBannerBottomSpace.constant = -75;
        }
        [self.view layoutIfNeeded];
    }
}

#pragma mark - SelectSearchLocationMethodView
-(void)handleViewShadow{
    if (self.changeDestinationBottomSpace.constant >= 0){
        self.viewChangeDestinationBanne.layer.shadowOpacity = 1.0;
        self.viewHomeWorkBanner.layer.shadowOpacity = 0.0;
        self.viewRdieDetailsBanner.layer.shadowOpacity = 0.0;
     } else if (self.homeWorkBannerBottomSpace.constant >= 0){
         self.viewHomeWorkBanner.layer.shadowOpacity = 1.0;
         self.viewChangeDestinationBanne.layer.shadowOpacity = 0.0;
         self.viewRdieDetailsBanner.layer.shadowOpacity = 0.0;
     } else {
        self.viewHomeWorkBanner.layer.shadowOpacity = 0.0;
        self.viewChangeDestinationBanne.layer.shadowOpacity = 0.0;
        self.viewRdieDetailsBanner.layer.shadowOpacity = 1.0;
    }
}

-(void)shouldShowSelectWorkHome:(BOOL)shouldShow{
    if (shouldShow){
        if (self.homeWorkBannerBottomSpace.constant < 1){
            self.homeWorkBannerBottomSpace.constant = 1;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    } else {
        if (self.homeWorkBannerBottomSpace.constant > -75 && self.changeDestinationBottomSpace.constant >= 0){
            self.homeWorkBannerBottomSpace.constant = -75;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    }
    [self handleViewShadow];
}

- (IBAction)searchLocationMethodSelected:(UIButton *)sender {
    if (sender.tag == 101) {
        self.imageViewDestinationDropPin.hidden = false;
    } else {
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
        acController.delegate = self;
        [self presentViewController:acController animated:YES completion:^{
            self.imageViewDestinationDropPin.hidden = false;
        }];
    }
}

#pragma mark - ManagerViewShowHideFunctions
-(void)manageViewInitially{
    if (self.isModeSearchLocation){
        self.imageViewDestinationDropPin.hidden = false;
        [self shouldShowRunningRideInfoBannerShouldShow:APP_DELEGATE.isTripRunning];
        [self shouldShowChangeDestinationBanner:false];
        [self shouldShowSelectWorkHome:(self.searchLocationMode != SLM_SET_WORK_LOCATION && self.searchLocationMode != SLM_SET_HOME_LOCATION)];
        
        if (self.searchLocationMode == SLM_CHANGE_DESTINATION || self.searchLocationMode == SLM_SET_END_LOCATION){
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerDestination"];
        } else  if (self.searchLocationMode == SLM_SET_START_LOCATION){
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerPickup"];
        }else {
            if (self.searchLocationMode == SLM_SET_HOME_LOCATION || isModeSetHomeLocal){
                self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"add_home");
                self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerHome"];
            } else if (self.searchLocationMode == SLM_SET_WORK_LOCATION || isModeSetWorkLocal) {
                self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"add_work");
                self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerWork"];
            }
        }
    } else {
        self.homeWorkBannerBottomSpace.constant = -75;
        self.changeDestinationBottomSpace.constant = -150;
        [self.view layoutIfNeeded];
    }
}

-(void)showNessesoryViewsForMapView{
    [self shouldShowSelectWorkHome:(self.searchLocationMode != SLM_SET_WORK_LOCATION && self.searchLocationMode != SLM_SET_HOME_LOCATION)];
    [self shouldShowRunningRideInfoBannerShouldShow:APP_DELEGATE.isTripRunning];
}

- (IBAction)homeOrWorkButtonTapped:(UIButton *)sender {
    if (sender.tag == 101){
        //Set with home
        double homeLattitude = [APP_DELEGATE.dictHomeInfo[@"lattitude"]doubleValue];
        double homeLongitude = [APP_DELEGATE.dictHomeInfo[@"longitude"]doubleValue];
        
        if (homeLattitude == 00.00000 && homeLongitude == 00.00000){
            //Will Set as Home
            isModeSetHomeLocal = true;
            isModeSetWorkLocal = false;
            self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"add_home");
            [self.buttonChangeDestinationDone setTitle:SET_ALERT_TEXT(@"done_capital") forState:UIControlStateNormal];
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerHome"];
        } else {
            //Will Center the Map at home
            CLLocationCoordinate2D homeLocation = CLLocationCoordinate2DMake(homeLattitude, homeLongitude);
            isFromAutoComplete = true;
            self.searchBarSearchForPlace.text = APP_DELEGATE.dictHomeInfo[@"title"];
            [self.viewGMSMapView animateToLocation:homeLocation];
        }
    } else {
        //Set with Work
        double workLattitude = [APP_DELEGATE.dictWorkInfo[@"lattitude"]doubleValue];
        double workLongitude = [APP_DELEGATE.dictWorkInfo[@"longitude"]doubleValue];
        
        if (workLattitude == 00.00000 && workLongitude == 00.00000){
            //Will Set as work
            isModeSetWorkLocal = true;
            isModeSetHomeLocal = false;
            self.lableChangeDestinationTitle.text = SET_ALERT_TEXT(@"add_work");
            [self.buttonChangeDestinationDone setTitle:SET_ALERT_TEXT(@"done_capital") forState:UIControlStateNormal];
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerWork"];
        } else {
            //Will Center the Map at work
            isFromAutoComplete = true;
            self.searchBarSearchForPlace.text = APP_DELEGATE.dictWorkInfo[@"title"];
            CLLocationCoordinate2D workLocation = CLLocationCoordinate2DMake(workLattitude, workLongitude);
            [self.viewGMSMapView animateToLocation:workLocation];
        }
    }
}



@end
