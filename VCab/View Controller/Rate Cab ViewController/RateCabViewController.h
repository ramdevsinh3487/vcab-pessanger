//
//  RateCabViewController.h
//  VCab
//
//  Created by Vishal Gohil on 16/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RateView/RateView.h>

#import "TripDetailViewController.h"

@protocol RateCabViewControllerDelegate <NSObject>
@optional
-(void)didSubmitDriverRating;
@optional
-(void)didCancelDriverRating;
@end

@interface RateCabViewController : UIViewController <RateViewDelegate, UITextViewDelegate>{
    float keyboardHeight;
}
@property id <RateCabViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *viewRateAlertContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet RateView *viewRaterContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverName;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDriverImage;
@property (weak, nonatomic) IBOutlet UILabel *lableRateValue;
@property (weak, nonatomic) IBOutlet UILabel *lableReviewMessage;
@property (weak, nonatomic) IBOutlet UITextView *textViewReview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewTextViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;

@property NSDictionary * dictDriverDetails;
@end
