//
//  RateCabViewController.m
//  VCab
//
//  Created by Vishal Gohil on 16/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "RateCabViewController.h"

@interface RateCabViewController ()

@end

@implementation RateCabViewController

- (void)viewDidLoad {
    self.lableDriverName.backgroundColor = [UIColor clearColor];
    [super viewDidLoad];
    //    self.viewRateAlertContainer.hidden = true;
    [self.navigationController setNavigationBarHidden:true];
    
    if (DEBUG_MODE){
        NSLog(@"Ride Driver Info : %@",self.dictDriverDetails);
    }
  
    
    self.lableTitle.text = SET_ALERT_TEXT(@"rate_your_driver");
    self.lableReviewMessage.text = SET_ALERT_TEXT(@"write_your_review");
    
    self.lableDriverName.text = [NSString stringWithFormat:@"%@ %@",self.dictDriverDetails[@"driver_first_name"],self.dictDriverDetails[@"driver_last_name"]];
    
    NSString * driverImageURL = self.dictDriverDetails[@"driver_user_image"];
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.imageViewDriverImage.frame.size.width, self.imageViewDriverImage.frame.size.height)];
    [self.imageViewDriverImage addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    [self.imageViewDriverImage sd_setImageWithURL:[NSURL URLWithString:driverImageURL] placeholderImage:[UIImage imageNamed:@"PHProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
    }];
    
    self.viewRaterContainer.starFillColor = [UIColor yellowColor];
    self.viewRaterContainer.starNormalColor = [UIColor lightGrayColor];
    self.viewRaterContainer.starBorderColor = [UIColor yellowColor];
    self.lableRateValue.text = @"0.0";
    self.buttonSubmit.hidden = true;
    self.viewRaterContainer.rating  = 0.0;
    self.viewRaterContainer.canRate= true;
    self.viewRaterContainer.delegate = self;
    self.viewRaterContainer.tintColor = [UIColor blackColor];
    self.viewRaterContainer.starFillColor = [UIColor blackColor];
    self.viewRaterContainer.starBorderColor = [UIColor blackColor];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.imageViewDriverImage.layer.borderWidth = 2.0;
    self.imageViewDriverImage.layer.borderColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.5].CGColor;
    self.imageViewDriverImage.layer.cornerRadius = self.imageViewDriverImage.frame.size.height / 2;
    self.imageViewDriverImage.layer.masksToBounds = true;
    
    self.lableDriverName.layer.cornerRadius = self.lableDriverName.frame.size.height / 2;
    self.lableDriverName.layer.masksToBounds = true;
    
    

    
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureFound:)];
    [self.view addGestureRecognizer:tapGesture];
    [self.viewRateAlertContainer addGestureRecognizer:tapGesture];
    
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager]setEnable:false];
    
    [self.viewRaterContainer setStep:0.5];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShowNotification:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHideNotification:) name:UIKeyboardDidHideNotification object:nil];
    
    self.reviewTextViewHeight.constant = SCREEN_HEIGHT - 360;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        self.lableDriverName.backgroundColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.5];
    }];

    self.viewRateAlertContainer.hidden = false;
}

-(void)viewDidDisappear:(BOOL)animated{
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager]setEnable:true];
}

-(void)tapGestureFound:(UIGestureRecognizer *)recognizer{
    [self.textViewReview resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)rateCabButtonTapped:(id)sender {
    NSString * driverId = self.dictDriverDetails[@"driver_id"];
    NSString * rating = self.lableRateValue.text;
    NSString * review = self.textViewReview.text;
    NSString * ride_id = self.dictDriverDetails[@"ride_id"];
    START_HUD
    [APP_DELEGATE.apiManager rateDriverWithDriverId:driverId rideId:ride_id rating:rating review:review WithCallBack:^(BOOL success, NSString *serverMessage) {
        if (success){
            SHOW_ALERT_WITH_SUCCESS(serverMessage)
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:true completion:^{
                    [self.delegate didSubmitDriverRating];
                }];
            });
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessage)
        }
        STOP_HUD
    }];
}
- (IBAction)cancelButtonTAppeds:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        [self.delegate didCancelDriverRating];
    }];
}

#pragma TextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    CGFloat newHeight = [self requiredHeight:[textView.text stringByAppendingString:text]];
    
    if (DEBUG_MODE){
        NSLog(@"New Height : %f",newHeight);
    }
    
    if ((newHeight + 60) < (keyboardHeight - 45)){
        CGFloat missingHeight = (keyboardHeight - self.reviewTextViewHeight.constant) - 60;
        
        //        if (newHeight == 0){
        //
        //            newHeight = missingHeight + 40;
        //        }
        
        self.textViewBottomSpace.constant =missingHeight + newHeight + 40;
        
        [UIView animateWithDuration:0.1 animations:^{
            [self.view layoutIfNeeded];
        }];
    }

    return true;
}

- (CGFloat)requiredHeight:(NSString*)textfieldString{

    UIFont *font = [UIFont fontWithName:@"Avenir Next" size:16.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, CGFLOAT_MAX)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = font;
    label.text = textfieldString;
    [label sizeToFit];
    return label.frame.size.height;
}
#pragma mark - RatinViewDelegate
-(void)rateView:(RateView*)rateView didUpdateRating:(float)rating{
    if (rating > 0){
        self.buttonSubmit.hidden = false;
    } else {
        self.buttonSubmit.hidden = true;
    }
    self.lableRateValue.text = [NSString stringWithFormat:@"%.01f",rating];
    
}

- (void)keyBoardWillShowNotification:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    
    if (keyboardHeight == 0) {
        keyboardHeight = [keyboardFrameBegin CGRectValue].size.height + 44;
    } else if (keyboardHeight != ([keyboardFrameBegin CGRectValue].size.height + 44)){
        keyboardHeight= [keyboardFrameBegin CGRectValue].size.height;
    }
    
    CGFloat missingHeight = (keyboardHeight - self.reviewTextViewHeight.constant) - 60;
    //    if (missingHeight >= 0){
    //        self.textViewBottomSpace.constant = missingHeight;
    //    }
    
    self.textViewBottomSpace.constant = missingHeight;
    
    CGFloat newHeight = [self requiredHeight:self.textViewReview.text];
    if (newHeight > 20 && self.textViewBottomSpace.constant < keyboardHeight){
        self.textViewBottomSpace.constant = self.textViewBottomSpace.constant + newHeight;
    } else if (newHeight <= 0){
        self.textViewBottomSpace.constant = self.textViewBottomSpace.constant + 40;
    }
    
    [UIView animateWithDuration:0.1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyBoardWillHideNotification:(NSNotification*)notification{
    self.textViewBottomSpace.constant = 20;
}

@end
