//
//  PromocodeViewController.m
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "PromocodeViewController.h"

@interface PromocodeViewController ()

@end

@implementation PromocodeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:true];
    
    arrayPromocodes = [[NSMutableArray alloc]init];
    
    self.tableViewCodeList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    arrayPromocodes = [[NSMutableArray alloc]initWithArray:APP_DELEGATE.arrayPromoCodes];
    [self.tableViewCodeList reloadData];
    selectedIndex = (int)arrayPromocodes.count;
    for (int i = 0; i < arrayPromocodes.count; i++){
        if ([self.promocodeId isEqualToString:arrayPromocodes[i][@"id"]]){
            selectedIndex = i;
            break;
        }
    }
    
    if (self.delegate == nil || [self.delegate isKindOfClass:[NSNull class]]){
        self.buttonDone.hidden = true;
        self.buttonClose.hidden = false;
    } else {
        self.buttonDone.hidden = false;
        self.buttonClose.hidden = true;
    }
    
    self.lableTitle.text = SET_ALERT_TEXT(@"promocodes");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeButtonTapAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)addButtonTapAction:(id)sender {
//    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Add promocode" message:nil preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* doneAction = [UIAlertAction actionWithTitle:@"Done" style: UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//        UITextField *textField = alert.textFields[0];
//        if (DEBUG_MODE){
//            NSLog(@"text was %@", textField.text);
//        }
//    }];
//    
//    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { }];
//    
//    [alert addAction:doneAction];
//    [alert addAction:cancelAction];
//    
//    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"Enter Promocode";
//    }];
//    
//    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)doneButtonTapped:(id)sender {
    if (self.delegate){
        if (selectedIndex < arrayPromocodes.count){
            dictSelectedCode = [[NSDictionary alloc]initWithDictionary:arrayPromocodes[selectedIndex]];
            [self dismissViewControllerAnimated:true completion:^{
                [self.delegate didSelectPromocodeToApply:dictSelectedCode];
            }];
        } else {
            [self dismissViewControllerAnimated:true completion:^{
                [self.delegate didSelectPromocodeToApply:@{}];
            }];
        }
    } else {
        [self.navigationController popViewControllerAnimated:true];
    }
}


#pragma mark - UITableViewDataSource&Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayPromocodes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellDetails"];
    UILabel * lableTitle = [cell viewWithTag:101];
    UILabel * lableDetails = [cell viewWithTag:102];
    UILabel * lableStatus = [cell viewWithTag:103];
    lableDetails.text = [arrayPromocodes[indexPath.row][@"promo_code"]uppercaseString];
    lableTitle.text = [NSString stringWithFormat:@"%@ %% %@",arrayPromocodes[indexPath.row][@"percentage"],SET_ALERT_TEXT(@"cashback")];
    UIImageView * imageViewCheckMArk = [cell viewWithTag:104];
    imageViewCheckMArk.hidden = (self.delegate == nil || [self.delegate isKindOfClass:[NSNull class]]);
    
    BOOL isExpired = [((NSNumber *)arrayPromocodes[indexPath.row][@"isExpired"]) boolValue];
    BOOL isAvailavle = [((NSNumber *)arrayPromocodes[indexPath.row][@"isAvailavle"]) boolValue];
    BOOL isValid = [((NSNumber *)arrayPromocodes[indexPath.row][@"isValid"]) boolValue];
    
    if (isExpired){
        lableStatus.text = [NSString stringWithFormat:@"%@ on %@",SET_ALERT_TEXT(@"expired_on"),arrayPromocodes[indexPath.row][@"end_date"]];
        lableStatus.textColor = [UIColor blackColor];
    } else if (!isValid){
        lableStatus.text = SET_ALERT_TEXT(@"invalid");
        lableStatus.textColor = [UIColor grayColor];
    } else if (isAvailavle){
        lableStatus.text = [NSString stringWithFormat:@"%@ %@", SET_ALERT_TEXT(@"available_till"),arrayPromocodes[indexPath.row][@"end_date"]];
        lableStatus.textColor = self.buttonClose.tintColor;
    } else {
        lableStatus.text = [NSString stringWithFormat:@"%@ %@", SET_ALERT_TEXT(@"valid_from"),arrayPromocodes[indexPath.row][@"start_date"]];
        lableStatus.textColor = [UIColor lightGrayColor];
    }
    
    if (imageViewCheckMArk.isHidden == false){
        if (indexPath.row == selectedIndex) {
            imageViewCheckMArk.image = [UIImage imageNamed:@"checked"];
        } else {
            imageViewCheckMArk.image = [UIImage imageNamed:@"unchecked"];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isExpired = [((NSNumber *)arrayPromocodes[indexPath.row][@"isExpired"]) boolValue];
    BOOL isAvailavle = [((NSNumber *)arrayPromocodes[indexPath.row][@"isAvailavle"]) boolValue];
    BOOL isValid = [((NSNumber *)arrayPromocodes[indexPath.row][@"isValid"]) boolValue];
    if (isExpired == false && isAvailavle == true && isValid == true){
        [tableView deselectRowAtIndexPath:indexPath animated:true];
        if (self.delegate != nil && ![self.delegate isKindOfClass:[NSNull class]]){
            int oldIndex = selectedIndex;
            if (oldIndex >= arrayPromocodes.count){
                oldIndex = 0;
            }
            
            if (selectedIndex == indexPath.row){
                selectedIndex = arrayPromocodes.count;
                [self.tableViewCodeList reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            } else {
                selectedIndex = (int)indexPath.row;
                [self.tableViewCodeList reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:oldIndex inSection:0], indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
    } else {
        [tableView deselectRowAtIndexPath:indexPath animated:false];
    }
    
}


@end
