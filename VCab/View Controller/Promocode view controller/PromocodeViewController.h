//
//  PromocodeViewController.h
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PromocodeViewControllerDelegate <NSObject>
@optional
-(void)didSelectPromocodeToApply:(NSDictionary *)dictPromocode;
@end

@interface PromocodeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray * arrayPromocodes;
    NSDictionary * dictSelectedCode;
    int selectedIndex;
}

@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewCodeList;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;
@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property NSString * promocodeId;
@property id <PromocodeViewControllerDelegate> delegate;

@end
