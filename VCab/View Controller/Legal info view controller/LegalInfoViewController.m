//
//  LegalInfoViewController.m
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "LegalInfoViewController.h"

@interface LegalInfoViewController ()

@end

@implementation LegalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    arrayTableResource = @[
                           @{@"title":SET_ALERT_TEXT(@"copyright"), @"URL":@"9"},
                           @{@"title":SET_ALERT_TEXT(@"terms"), @"URL":@"2"},
                           @{@"title":SET_ALERT_TEXT(@"privacy"), @"URL":@"3"},
                           @{@"title":SET_ALERT_TEXT(@"contact_us"), @"URL":@"5"},
                           @{@"title":SET_ALERT_TEXT(@"about_us"), @"URL":@"1"},
                           ];
    
    self.tableViewList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableViewList reloadData];
    // Do any additional setup after loading the view.
}
- (IBAction)closeButtonTapAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource&Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayTableResource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellDetails"];
    UILabel * lableTitle = [cell viewWithTag:101];
    lableTitle.text = arrayTableResource[indexPath.row][@"title"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];

    LoadURLViewController * loadURLView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
    loadURLView.pageTitle = arrayTableResource[indexPath.row][@"title"];
    loadURLView.isModeLoadString = true;
    
    NSString * apiMode = arrayTableResource[indexPath.row][@"URL"];
    
    if ([apiMode isEqualToString:@"1"]){
        loadURLView.cmsAPIMode = CMS_API_ABOUT_US;
    }else if ([apiMode isEqualToString:@"2"]){
        loadURLView.cmsAPIMode = CMS_API_TERMS;
    } else if ([apiMode isEqualToString:@"3"]){
        loadURLView.cmsAPIMode = CMS_API_PRIVERCY;
    } else if ([apiMode isEqualToString:@"5"]){
        [self shoulsShowContactUsMessageCompose];
        return;
    } else if ([apiMode isEqualToString:@"9"]){
        loadURLView.cmsAPIMode = CMS_API_COPYRIGHT;
    }
    loadURLView.isModeLoadString = true;
    loadURLView.isPushed = false;
    [self presentViewController:loadURLView animated:true completion:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

#pragma mark - AlertViewControllerDelegate
-(void)shoulsShowContactUsMessageCompose {
    MFMailComposeViewController * mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"Contact Us"];
    [mailComposer setToRecipients:@[APP_DELEGATE.adminEmail]];
    [self presentViewController:mailComposer animated:true completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result) {
        case MFMailComposeResultSent:
            if (DEBUG_MODE){
                NSLog(@"You sent the email.");
            }
            break;
        case MFMailComposeResultSaved:
            if (DEBUG_MODE){
                NSLog(@"You saved a draft of this email");
            }
            break;
        case MFMailComposeResultCancelled:
            if (DEBUG_MODE){
                NSLog(@"You cancelled sending this email.");
            }
            break;
        case MFMailComposeResultFailed:
            if (DEBUG_MODE){
                NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            }
            break;
        default:
            if (DEBUG_MODE){
                NSLog(@"An error occurred when trying to compose this email");
            }
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
