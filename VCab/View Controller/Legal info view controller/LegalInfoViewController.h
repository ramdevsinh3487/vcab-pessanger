//
//  LegalInfoViewController.h
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadURLViewController.h"
#import "AlertViewController.h"
#import <MessageUI/MessageUI.h>
@interface LegalInfoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, AlertViewControllerDelegate,MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>{
    NSArray * arrayTableResource;
}
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewList;

@end
