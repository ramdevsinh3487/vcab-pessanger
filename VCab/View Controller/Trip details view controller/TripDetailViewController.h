//
//  TripDetailViewController.h
//  VCab
//
//  Created by Vishal Gohil on 19/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "RateCabViewController.h"

@interface TripDetailViewController : UIViewController <UITableViewDelegate,UITableViewDataSource, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>{
    NSMutableArray * arrayReciptTableData;
    NSMutableArray * arraySupportTableData;
    NSMutableArray * arrayTableResources;
    NSDictionary * dictTripDetails;
    BOOL isModeRecieptData;
}
@property BOOL isModeSubmit;
@property NSString * trip_id;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitButtonHeight;


@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMapPreview;
@property (weak, nonatomic) IBOutlet UILabel *lableMapImageStatus;

@property (weak, nonatomic) IBOutlet UILabel *lableDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lableCarData;
@property (weak, nonatomic) IBOutlet UILabel *lableStartLocation;
@property (weak, nonatomic) IBOutlet UILabel *lableDestinationLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDriverProfile;
@property (weak, nonatomic) IBOutlet UIView *viewDriverDetailsContsainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverContainerHeight;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverName;
@property (weak, nonatomic) IBOutlet UIButton *buttonDriverRating;

@property (weak, nonatomic) IBOutlet UILabel *labelPaymentType;

@property (weak, nonatomic) IBOutlet UIButton *buttonSupprts;
@property (weak, nonatomic) IBOutlet UILabel *lableSupports;
@property (weak, nonatomic) IBOutlet UIButton *buttonReciept;
@property (weak, nonatomic) IBOutlet UILabel *lableReciept;

@property (weak, nonatomic) IBOutlet UITableView *tableViewReceipt;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recieptTableViewHeight;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableCharges;
@property (weak, nonatomic) IBOutlet UILabel *lableRideStatus;

@end
