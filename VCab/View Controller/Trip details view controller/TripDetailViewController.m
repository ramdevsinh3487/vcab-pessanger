//
//  TripDetailViewController.m
//  VCab
//
//  Created by Vishal Gohil on 19/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "TripDetailViewController.h"

@interface TripDetailViewController ()

@end

@implementation TripDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
//    self.buttonSubmit.hidden = !self.isModeSubmit;
//    self.buttonBack.hidden = self.isModeSubmit;
    isModeRecieptData = true;
    
    if (self.trip_id && ![self.trip_id isEqualToString:@""]){
        START_HUD
        [APP_DELEGATE.apiManager getTripDetailsFromServerWithTripId:self.trip_id WithCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessage) {
            if (success){
                dictTripDetails = [[NSDictionary alloc]initWithDictionary:response];
                [self loadTripDetailsFromServer];
                self.scrollViewContainer.hidden = false;
            } else {
                SHOW_ALERT_WITH_CAUTION(errorMessage);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self dismissViewControllerAnimated:true completion:nil];
                });
            }
            STOP_HUD
        }];
    }
    self.lableTitle.text = SET_ALERT_TEXT(@"trip_detail");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.buttonDriverRating setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.imageViewDriverProfile.layer.cornerRadius = self.imageViewDriverProfile.frame.size.height / 2;
    self.imageViewDriverProfile.layer.borderWidth = 2.0;
    self.imageViewDriverProfile.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.imageViewDriverProfile.layer.masksToBounds = true;
    
    self.tableViewReceipt.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(void)loadTripDetailsFromServer{
    
    [self updateTripDetails];
    [self updateDriverDetails];
    [self updateRecieptData];
}

-(void)updateTripDetails{
    NSString * rideStatus = dictTripDetails[@"ride_status"];
    if ([rideStatus isEqualToString:@"1"]){
        self.lableRideStatus.text = SET_ALERT_TEXT(@"accepted");
         self.lableMapImageStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    } else if ([rideStatus isEqualToString:@"2"]){
        self.lableRideStatus.text = SET_ALERT_TEXT(@"arrived");
         self.lableMapImageStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    } else if ([rideStatus isEqualToString:@"3"]){
        self.lableRideStatus.text = SET_ALERT_TEXT(@"on_going");
         self.lableMapImageStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    } else if ([rideStatus isEqualToString:@"4"]){
        self.lableRideStatus.text = SET_ALERT_TEXT(@"cancelled");
         self.lableMapImageStatus.text = SET_ALERT_TEXT(@"loading");
    } else if ([rideStatus isEqualToString:@"5"]){
        self.lableRideStatus.text = SET_ALERT_TEXT(@"finished");
         self.lableMapImageStatus.text = SET_ALERT_TEXT(@"loading");
    } else if ([rideStatus isEqualToString:@"6"]){
        self.lableRideStatus.text = SET_ALERT_TEXT(@"declined");
         self.lableMapImageStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    } else {
        self.lableRideStatus.text = SET_ALERT_TEXT(@"request_send");
         self.lableMapImageStatus.text = SET_ALERT_TEXT(@"no_preview_available");
    }
    
    NSString * mapViewImage= dictTripDetails[@"map_screenshot"];
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.imageViewMapPreview.frame.size.width, self.imageViewMapPreview.frame.size.height)];
    [self.imageViewMapPreview addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    [self.imageViewMapPreview sd_setImageWithURL:[NSURL URLWithString:mapViewImage] placeholderImage:[UIImage imageNamed:@"PHMapshot"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        
        if (!image){
            self.lableMapImageStatus.text = SET_ALERT_TEXT(@"no_preview_available");
        } else {
            self.lableMapImageStatus.text = @"";
        }
    }];
    
    NSDateFormatter * dateFormater = [[NSDateFormatter alloc]init];
    dateFormater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate * startDate = [dateFormater dateFromString:dictTripDetails[@"trip_starttime"]];
    if (!startDate){
        startDate = [dateFormater dateFromString:dictTripDetails[@"pickup_date_time"]];
    }
    
    dateFormater.dateFormat = @"dd MMM yyyy HH:mm";
    NSString * strStartDate = [dateFormater stringFromDate:startDate];
    self.lableDateTime.text = strStartDate;
    
    self.lableStartLocation.text = dictTripDetails[@"source_location"];
    self.lableDestinationLocation.text = dictTripDetails[@"destination_location"];
    
    self.lableCarData.text = [NSString stringWithFormat:@"%@ %@", dictTripDetails[@"car_brand_name"],dictTripDetails[@"car_model"]];
    
    NSString * totalCost = dictTripDetails[@"trip_totalcost"];
    if ([totalCost isEqualToString:@""]){
        self.lableCharges.text = [NSString stringWithFormat:@"%@ 0", APP_DELEGATE.currencySymbol];
    } else {
        self.lableCharges.text = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol, totalCost];
    }
    
    if([dictTripDetails[@"payment_type"] isEqualToString:@"1"]){
        self.labelPaymentType.text = SET_ALERT_TEXT(@"cash_capital");
    } else if ([dictTripDetails[@"payment_type"] isEqualToString:@""]) {
        self.labelPaymentType.text = @"";
    } else {
        self.labelPaymentType.text = SET_ALERT_TEXT(@"card_capital");
    }
}

-(void)updateDriverDetails {
    NSString * driverId = dictTripDetails[@"driver_id"];
    if (driverId && ![driverId isEqualToString:@""]){
        NSString * driverImage= dictTripDetails[@"driver_image_url"];
        UIActivityIndicatorView *activity_indicatorDriver = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.imageViewDriverProfile.frame.size.width, self.imageViewDriverProfile.frame.size.height)];
        [self.imageViewDriverProfile addSubview:activity_indicatorDriver];
        [activity_indicatorDriver startAnimating];
        [activity_indicatorDriver setColor:[UIColor blackColor]];
        [self.imageViewDriverProfile sd_setImageWithURL:[NSURL URLWithString:driverImage] placeholderImage:[UIImage imageNamed:@"PHProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activity_indicatorDriver stopAnimating];
            [activity_indicatorDriver removeFromSuperview];
        }];
        
        self.lableDriverName.text = [NSString stringWithFormat:@"%@ %@ %@", SET_ALERT_TEXT(@"your_trip_with"), dictTripDetails[@"driver_f_name"],dictTripDetails[@"driver_l_name"]];
        float driverRating = [dictTripDetails[@"driver_ratings"] floatValue];
        
        if (!self.isModeSubmit && [dictTripDetails[@"ride_status"] isEqualToString:@"5"]){
            if (driverRating > 0){
                [self.buttonDriverRating setTitle:[NSString stringWithFormat:@"%@ ★",dictTripDetails[@"driver_ratings"]] forState:UIControlStateNormal];
                [self.buttonDriverRating setEnabled:false];
            } else {
                
                [self.buttonDriverRating setTitle:SET_ALERT_TEXT(@"rate_now") forState:UIControlStateNormal];
                [self.buttonDriverRating setEnabled:true];
            }
        }else {
            self.buttonDriverRating.hidden = true;
        }
        
    } else {
        isModeRecieptData = false;
        self.driverContainerHeight.constant = 0;
        self.viewDriverDetailsContsainer.hidden = true;
    }
}

-(void)updateRecieptData{

    arrayReciptTableData = [[NSMutableArray alloc]init];
    
    NSString * rideStatus = dictTripDetails[@"ride_status"];
    
    NSString * time = dictTripDetails[@"trip_duration"];
    NSArray * arrayComponents = [time componentsSeparatedByString:@":"];
    
    double hrs = [arrayComponents.firstObject doubleValue];
    
    NSString * tripDuration;
    //    = [NSString stringWithFormat:@"%@ Mins", [time substringToIndex:[time length] - 3]];
    if (hrs <= 0){
        //Will Show Minut, secs
        tripDuration = [NSString stringWithFormat:@"%@ %@", [time substringFromIndex:3], SET_ALERT_TEXT(@"minutes")];
        
    } else {
        //Will Show Hours
        tripDuration = [NSString stringWithFormat:@"%@ %@", [time substringToIndex:[time length] - 3], SET_ALERT_TEXT(@"hours")];
    }
                               
    NSString * promoPercentage = dictTripDetails[@"percentage"];
    NSString * taxAmount = dictTripDetails[@"trip_tax"];
    
    BOOL canAddPromo;
    if (promoPercentage && ![promoPercentage isEqualToString:@""] && ![promoPercentage isEqualToString:@"0"]){
        promoPercentage = [NSString stringWithFormat:@"(%@ %%)",promoPercentage];
        canAddPromo = true;
    } else if ([rideStatus isEqualToString:@"5"]){
        promoPercentage = @"0 %";
        canAddPromo = false;
    } else {
        promoPercentage = @"0";
        canAddPromo = false;
    }
    
    BOOL canAddTax;
    
    if (taxAmount && ![taxAmount isEqualToString:@""] && ![taxAmount isEqualToString:@"0"]){
        canAddTax = true;
    } else {
        canAddTax = false;
    }
    
    
    [self.buttonReciept setTitle:SET_ALERT_TEXT(@"receipt") forState:UIControlStateNormal];
    [self.buttonSupprts setTitle:SET_ALERT_TEXT(@"support") forState:UIControlStateNormal];
    
    
    if ([rideStatus isEqualToString:@"5"]){
        [arrayReciptTableData addObject:@{
                                          @"title":SET_ALERT_TEXT(@"base_fare"),
                                          @"detail":[NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol,dictTripDetails[@"trip_fare"]]
                                          }];
        
        [arrayReciptTableData addObject:@{
                                          @"title":SET_ALERT_TEXT(@"distance"),
                                          @"detail":[NSString stringWithFormat:@"%@ KM",dictTripDetails[@"trip_km"]]
                                          }];
        
        [arrayReciptTableData addObject:@{
                                          @"title":SET_ALERT_TEXT(@"time"),
                                          @"detail":[NSString stringWithFormat:@"%@", tripDuration]
                                          }];
        
//        [arrayReciptTableData addObject:@{
//                                          @"title":SET_ALERT_TEXT(@"subtotal"),
//                                          @"detail":[NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol,dictTripDetails[@"trip_fare"]]
//                                          }];
        
        if (canAddTax){
            [arrayReciptTableData addObject:@{
                                              @"title":[NSString stringWithFormat:@"%@ (0 %%)",SET_ALERT_TEXT(@"tax")],
                                              @"detail":[NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol,dictTripDetails[@"trip_tax"]]
                                              }];
        }
        
        if (canAddPromo){
            [arrayReciptTableData addObject:@{
                                              @"title":[NSString stringWithFormat:@"%@ %@",SET_ALERT_TEXT(@"promotion"),promoPercentage],
                                              @"detail":[NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol, dictTripDetails[@"trip_discount"]]
                                              }];
            
        }
        
        
        
        [arrayReciptTableData addObject:@{ @"title":SET_ALERT_TEXT(@"total"),
                                           @"detail":[NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol,dictTripDetails[@"trip_totalcost"]]
                                           }];
        
        [arrayReciptTableData addObject:@{ @"title":SET_ALERT_TEXT(@"cash_capital"),
                                           @"detail":[NSString stringWithFormat:@"%@ %@", APP_DELEGATE.currencySymbol,dictTripDetails[@"trip_totalcost"]]
                                           }];
    } else {
        [arrayReciptTableData addObject:@{@"title":SET_ALERT_TEXT(@"distance"), @"detail":[NSString stringWithFormat:@"%@ KM",dictTripDetails[@"trip_km"]]}];
        [arrayReciptTableData addObject:@{@"title":SET_ALERT_TEXT(@"time"), @"detail":[NSString stringWithFormat:@"%@", tripDuration]}];
        if (canAddPromo){
            [arrayReciptTableData addObject:@{@"title":SET_ALERT_TEXT(@"promotion"), @"detail":[NSString stringWithFormat:@"%@", promoPercentage]}];
        }
    }
    
    arraySupportTableData = [[NSMutableArray alloc]init];
    [arraySupportTableData addObjectsFromArray:@[
                                                 @{
                                                     @"title":SET_ALERT_TEXT(@"issue_related_to_payment"),
                                                     @"detail":@""
                                                     },
                                                 @{
                                                     @"title":SET_ALERT_TEXT(@"issue_related_to_driver"),
                                                     @"detail":@""
                                                     },
                                                 @{
                                                     @"title":SET_ALERT_TEXT(@"issue_related_to_ride"),
                                                     @"detail":@""
                                                     },
                                                 @{
                                                     @"title":SET_ALERT_TEXT(@"issue_related_to_vehicle"),
                                                     @"detail":@""
                                                     },
                                                 @{
                                                     @"title":SET_ALERT_TEXT(@"other_issue"),
                                                     @"detail":@""
                                                     }
                                                 ]];

    
    self.submitButtonHeight.constant = self.isModeSubmit ? 50 : 0;
    [self loadTableViewAccordingtoMode];
}

-(void)loadTableViewAccordingtoMode {
    if (isModeRecieptData){
        arrayTableResources = [[NSMutableArray alloc]initWithArray:arrayReciptTableData];
        
        [self.buttonReciept setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.lableReciept.backgroundColor = [UIColor blackColor];
        
        [self.buttonSupprts setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.lableSupports.backgroundColor = [UIColor lightGrayColor];
    } else {
        arrayTableResources = [[NSMutableArray alloc]initWithArray:arraySupportTableData];
        
        [self.buttonReciept setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.lableReciept.backgroundColor = [UIColor lightGrayColor];
        
        [self.buttonSupprts setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.lableSupports.backgroundColor = [UIColor blackColor];
    }
    
    NSString * totalCost = dictTripDetails[@"trip_totalcost"];
    if ([totalCost doubleValue] > 0){
        self.recieptTableViewHeight.constant = arrayTableResources.count * 50;
    } else {
        self.recieptTableViewHeight.constant = 0;
    }
    
    [self.tableViewReceipt setAllowsSelection:!isModeRecieptData];
    [self.view layoutIfNeeded];
    [self.tableViewReceipt reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeButtonTapped:(id)sender {
    if (self.isModeSubmit){
        START_HUD
        NSDictionary * dictDriverDetails = APP_DELEGATE.dictTripDriverDetails.mutableCopy;
        [APP_DELEGATE.apiManager confirmRideStatusUpdateWithRideId:APP_DELEGATE.runningTripId DriverId:APP_DELEGATE.tripDriverId isRideCanaceled:false WithCallBack:^(BOOL success, NSString *serverMessage) {
            if (success){
                [APP_DELEGATE clearPrevousTripDataFromApp];
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_CLEAR_HOME_MAP object:nil];
                [self dismissViewControllerAnimated:true completion:^{
                    RateCabViewController * rateYourCab = [self.storyboard instantiateViewControllerWithIdentifier:@"RateCabViewController"];
                    rateYourCab.modalPresentationStyle = UIModalPresentationOverFullScreen;
                    rateYourCab.dictDriverDetails = dictDriverDetails;
                    rateYourCab.delegate = self;
                    
                    [APP_DELEGATE.globalNavigation presentViewController:rateYourCab animated:true completion:^{
                    }];
                }];
            } else {
                SHOW_ALERT_WITH_CAUTION(serverMessage);
            }
            STOP_HUD
        }];
    } else {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

#pragma mark - UITableViewDelegate&UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString * totalCost = dictTripDetails[@"trip_totalcost"];
    if ([totalCost doubleValue] > 0){
        self.buttonReciept.hidden = false;
        self.buttonSupprts.hidden = false;
        self.lableReciept.hidden = false;
        self.lableSupports.hidden = false;
        return arrayTableResources.count;
    } else {
        self.buttonReciept.hidden = true;
        self.buttonSupprts.hidden = true;
        self.lableReciept.hidden = true;
        self.lableSupports.hidden = true;
        return 0;
    }
    return arrayTableResources.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cellReceipt = [tableView dequeueReusableCellWithIdentifier:@"cellReceipt"];
    UILabel * title = [cellReceipt viewWithTag:101];
    UILabel * detail = [cellReceipt viewWithTag:102];
    title.text = arrayTableResources[indexPath.row][@"title"];
    detail.text = arrayTableResources[indexPath.row][@"detail"];
    
    if (!isModeRecieptData){
        cellReceipt.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        cellReceipt.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cellReceipt;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    MFMailComposeViewController * mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:arraySupportTableData[indexPath.row][@"title"]];
    [mailComposer setToRecipients:@[APP_DELEGATE.adminEmail]];
    [self presentViewController:mailComposer animated:true completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result) {
        case MFMailComposeResultSent:
            if (DEBUG_MODE){
                NSLog(@"You sent the email.");
            }
            break;
        case MFMailComposeResultSaved:
            if (DEBUG_MODE){
                NSLog(@"You saved a draft of this email");
            }
            break;
        case MFMailComposeResultCancelled:
            if (DEBUG_MODE){
                NSLog(@"You cancelled sending this email.");
            }
            break;
        case MFMailComposeResultFailed:
            if (DEBUG_MODE){
                NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            }
            break;
        default:
            if (DEBUG_MODE){
                NSLog(@"An error occurred when trying to compose this email");
            }
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}



- (IBAction)submitButtonTappedAction:(id)sender {
    START_HUD
    NSDictionary * dictDriverDetails = APP_DELEGATE.dictTripDriverDetails.mutableCopy;
    [APP_DELEGATE.apiManager confirmRideStatusUpdateWithRideId:APP_DELEGATE.runningTripId DriverId:APP_DELEGATE.tripDriverId isRideCanaceled:false WithCallBack:^(BOOL success, NSString *serverMessage) {
        if (success){
            [APP_DELEGATE clearPrevousTripDataFromApp];
            [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_CLEAR_HOME_MAP object:nil];
            [self dismissViewControllerAnimated:true completion:^{
                RateCabViewController * rateYourCab = [self.storyboard instantiateViewControllerWithIdentifier:@"RateCabViewController"];
                rateYourCab.modalPresentationStyle = UIModalPresentationOverFullScreen;
                rateYourCab.dictDriverDetails = dictDriverDetails;
                rateYourCab.delegate = self;
                
                [APP_DELEGATE.globalNavigation presentViewController:rateYourCab animated:true completion:^{
                }];
            }];
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessage);
        }
        STOP_HUD
    }];
}

- (IBAction)tableViewDataModeChanged:(UIButton *)sender {
    if (sender.tag == 101){
        isModeRecieptData = false;
    } else {
        isModeRecieptData = true;
    }
    [self loadTableViewAccordingtoMode];
}

- (IBAction)addDriverRating:(id)sender {
    //Will Open river Rating
    RateCabViewController * rateYourCab = [self.storyboard instantiateViewControllerWithIdentifier:@"RateCabViewController"];
    
    NSDictionary * dictDriverDetails = @{
                                         @"ride_id":dictTripDetails[@"id"],
                                         @"driver_id":dictTripDetails[@"driver_id"],
                                         @"driver_first_name":dictTripDetails[@"driver_f_name"],
                                         @"driver_last_name":dictTripDetails[@"driver_l_name"],
                                         @"driver_user_image":dictTripDetails[@"driver_image_url"],
                                         };
    
    rateYourCab.dictDriverDetails = dictDriverDetails;
    rateYourCab.delegate = self;
    [self presentViewController:rateYourCab animated:true completion:nil];
}

#pragma mark - RateCabViewControllerDelegate
-(void)didSubmitDriverRating{
    if (self.trip_id && ![self.trip_id isEqualToString:@""]){
        START_HUD
        [APP_DELEGATE.apiManager getTripDetailsFromServerWithTripId:self.trip_id WithCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessage) {
            if (success){
                dictTripDetails = [[NSDictionary alloc]initWithDictionary:response];
                [self loadTripDetailsFromServer];
                self.scrollViewContainer.hidden = false;
            } else {
                SHOW_ALERT_WITH_CAUTION(errorMessage);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self dismissViewControllerAnimated:true completion:nil];
                });
            }
            STOP_HUD
        }];
    }
}
-(void)didCancelDriverRating{
    //Code on cancel rating
}

@end
