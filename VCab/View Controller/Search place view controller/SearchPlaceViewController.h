//
//  SearchPlaceViewController.h
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchPlaceViewController : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    NSArray * arraySearchedList;
}
@property (weak, nonatomic) IBOutlet UITableView *tableViewSearchList;

@end
