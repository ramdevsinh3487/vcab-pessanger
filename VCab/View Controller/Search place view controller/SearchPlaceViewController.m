//
//  SearchPlaceViewController.m
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "SearchPlaceViewController.h"

@interface SearchPlaceViewController ()

@end

@implementation SearchPlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    arraySearchedList = @[
                          @{@"title":@"Vrinsoft",@"detail":@"Ganesh meridian"},
                          @{@"title":@"Iskon Temple",@"detail":@"Iskon cross road"},
                          @{@"title":@"Prahlaad nagar",@"detail":@"Prahlaadnagar, S G Highway"},
                          @{@"title":@"Makarba",@"detail":@"Makarba lake, S G Highway"},
                          @{@"title":@"Sarkhej",@"detail":@"Sarkhej, Ahmedabad"},
                          ];
    self.tableViewSearchList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableViewSearchList reloadData];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate&DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arraySearchedList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cellSearchPlace = [tableView dequeueReusableCellWithIdentifier:@"cellSearchPlace"];
    UILabel * placeName = [cellSearchPlace viewWithTag:101];
    placeName.text = arraySearchedList[indexPath.row][@"title"];
    
    UILabel * placeDetails = [cellSearchPlace viewWithTag:102];
    placeDetails.text = arraySearchedList[indexPath.row][@"detail"];
    return cellSearchPlace;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}
- (IBAction)closeButtonTapAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
