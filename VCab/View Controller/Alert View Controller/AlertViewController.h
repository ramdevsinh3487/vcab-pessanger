//
//  AlertViewController.h
//  VCab
//
//  Created by Vishal Gohil on 26/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IQKeyboardManager/IQKeyboardManager.h>

@protocol AlertViewControllerDelegate <NSObject>
@optional
-(void)alertControllerOkayButtonTappedWithTextViewText:(NSString *)textViewText;
@optional
-(void)alertControllerCancelButtonTapped;
@end

@interface AlertViewController : UIViewController <UITextViewDelegate>{
    BOOL isTextViewEditing;
}
@property id <AlertViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lableMessageTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableMessangeDetails;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIButton *buttonOkay;
    @property NSString * alertTitle;
    @property NSString * alertMessage;
    @property NSString * okayTitle;
    @property NSString * cancelTitle;
    @property BOOL canDismissTap;
    @property BOOL isModeWithTextView;
@property (weak, nonatomic) IBOutlet UITextView *textViewCancelReason;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reasonTextViewHeight;

//-(void)initializeAlertWithTitle:(NSString *)title messange:(NSString *)message OkayButton:(NSString *)okayTitle CancelButton:(NSString *)cancelTitle canDismissOnInteract:(BOOL)canDismiss;
@end
