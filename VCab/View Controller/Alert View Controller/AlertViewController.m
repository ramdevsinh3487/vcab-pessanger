//
//  AlertViewController.m
//  VCab
//
//  Created by Vishal Gohil on 26/06/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AlertViewController.h"

@interface AlertViewController ()

@end

@implementation AlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    self.lableMessageTitle.text = self.alertTitle;
    self.lableMessangeDetails.text = self.alertMessage;
    self.textViewCancelReason.delegate = self;
    
    if (self.okayTitle && ![self.okayTitle isEqualToString:@""]){
        [self.buttonOkay setTitle:self.okayTitle forState:UIControlStateNormal];
        self.buttonOkay.hidden = false;
    } else {
        self.buttonOkay.hidden = true;
    }
    
    if (self.cancelTitle && ![self.cancelTitle isEqualToString:@""]){
        [self.buttonCancel setTitle:self.cancelTitle forState:UIControlStateNormal];
        self.buttonCancel.hidden = false;
    } else {
        self.buttonCancel.hidden = true;
    }
    
    if (self.canDismissTap){
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureFoundOnView:)];
        [self.view addGestureRecognizer:tapGesture];
    }
    
    if (self.isModeWithTextView){
        self.reasonTextViewHeight.constant = 100;
    } else {
        self.reasonTextViewHeight.constant = 0;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (self.isModeWithTextView){
            [self.textViewCancelReason becomeFirstResponder];
        }
    }];
    
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:true];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:false];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)tapGestureFoundOnView:(UIGestureRecognizer *)gesture{
    if (isTextViewEditing == true){
        [self.textViewCancelReason resignFirstResponder];
    } else {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        [self.delegate alertControllerCancelButtonTapped];
    }];
}

- (IBAction)okayButtonTapped:(id)sender {
    if (self.isModeWithTextView == true && [self.textViewCancelReason.text isEqualToString:@""]){
        self.textViewCancelReason.layer.borderColor = self.textViewCancelReason.layer.shadowColor;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.textViewCancelReason.layer.borderColor = [UIColor lightGrayColor].CGColor;
        });
    } else {
        [self dismissViewControllerAnimated:true completion:^{
            if (self.isModeWithTextView){
                [self.delegate alertControllerOkayButtonTappedWithTextViewText:self.textViewCancelReason.text];
            } else {
                [self.delegate alertControllerOkayButtonTappedWithTextViewText:@""];
            }
        }];
    }
}


#pragma mark - UItextViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView{
    isTextViewEditing = true;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    isTextViewEditing = false;
}

- (void)textViewDidChange:(UITextView *)textView
{
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newSize.height > textView.frame.size.height && newSize.height > 100){
        self.reasonTextViewHeight.constant = newSize.height;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

@end
