//
//  ProfileViewController.m
//  VCab
//
//  Created by Vishal Gohil on 19/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "ProfileViewController.h"



@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    // Do any additional setup after loading the view.
    self.lableMobileNumber.text = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.dictUserInfo[@"country_code"],APP_DELEGATE.dictUserInfo[@"mobile_no"]];
    self.textFieldFirstName.text = APP_DELEGATE.dictUserInfo[@"first_name"];
    self.textFieldLastName.text = APP_DELEGATE.dictUserInfo[@"last_name"];
    self.textFieldEmail.text = APP_DELEGATE.dictUserInfo[@"email"];
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.buttonProfile.frame.size.width, self.buttonProfile.frame.size.height)];
    [self.buttonProfile addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    UIImageView * imgView = [[UIImageView alloc]init];
    [self.buttonProfile setImage:[UIImage imageNamed:@"PHAddProfile"] forState:UIControlStateNormal];
    [imgView sd_setImageWithURL:[NSURL URLWithString:APP_DELEGATE.dictUserInfo[@"user_image"]] placeholderImage:[UIImage imageNamed:@"PHAddProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [self.buttonProfile setImage:imgView.image forState:UIControlStateNormal];
    }];
    
    self.lableTermsInfo.hidden = !self.isModeInitialUpdate;
    self.buttonTerms.hidden = !self.isModeInitialUpdate;
    self.buttonPrivacyPolicy.hidden = !self.isModeInitialUpdate;

    self.buttonClose.hidden = !self.canGoBack;
    
    self.lableMobileNumberMessage.text = SET_ALERT_TEXT(@"your_mobile_no_is");
    self.textFieldFirstName.placeholder = SET_ALERT_TEXT(@"first_name");
    self.textFieldLastName.placeholder = SET_ALERT_TEXT(@"last_name");
    self.textFieldEmail.placeholder = SET_ALERT_TEXT(@"email_optional");
    [self.buttonUpdate setTitle:self.isModeInitialUpdate ? SET_ALERT_TEXT(@"next_capital") : SET_ALERT_TEXT(@"update_capital") forState:UIControlStateNormal];
    
    NSLog(@"%@", SET_ALERT_TEXT(@"next_capital"));
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.buttonProfile.layer.cornerRadius = self.buttonProfile.frame.size.height / 2;
    self.buttonProfile.layer.borderWidth = 2.0;
    self.buttonProfile.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.buttonProfile.layer.masksToBounds = true;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UItextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag == 101){
        if ([self isValidText:textField.text isModeEmail:false]) {
            [self.textFieldLastName becomeFirstResponder];
            return true;
        } else {
            self.textFieldFirstName.textColor = [UIColor redColor];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.textFieldFirstName.textColor = [UIColor blackColor];
            });
            return false;
        }
    } else if (textField.tag == 102){
        if ([self isValidText:textField.text isModeEmail:false]) {
            [self.textFieldEmail becomeFirstResponder];
            return true;
        } else {
            self.textFieldLastName.textColor = [UIColor redColor];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.textFieldLastName.textColor = [UIColor blackColor];
            });
            return false;
        }
    } else {
        if ([textField.text length] > 0){
            if ([self isValidText:textField.text isModeEmail:true]) {
                [textField resignFirstResponder];
                return true;
            } else {
                self.textFieldEmail.textColor = [UIColor redColor];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.textFieldEmail.textColor = [UIColor blackColor];
                });
                return false;
            }
        } else {
            return true;
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag == 101 || textField.tag == 102)
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        return YES;
    }
    
    return YES;
}


-(BOOL)isValidText:(NSString *)textToValidate isModeEmail:(BOOL)isEmailValidation{
    NSString *alphaNum;
    if (isEmailValidation){
        alphaNum = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    } else {
        alphaNum = @"[a-zA-z]+([ '-][a-zA-Z]+)*$";
    }
    
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alphaNum];
    return [regexTest evaluateWithObject:textToValidate];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}

#pragma mark - IBOutletAction
- (IBAction)closeButtonTapAction:(id)sender {
    if (self.isModeInitialUpdate){
        [self.navigationController popToRootViewControllerAnimated:true];
    } else {
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (IBAction)profileImageButtonTapAction:(id)sender {
    [self showImagePickerForPickImage];
}

- (IBAction)updateButtonTapAction:(id)sender {
    if (self.textFieldFirstName.text.length == 0){
        SHOW_ALERT_WITH_CAUTION(SET_ALERT_TEXT(@"please_enter_first_name"));
    } else if (self.textFieldLastName.text.length == 0){
        SHOW_ALERT_WITH_CAUTION(SET_ALERT_TEXT(@"Please enter your last name"));
    } else if ([self.textFieldEmail.text length] > 0 && ![self isValidText:self.textFieldEmail.text isModeEmail:true]){
        SHOW_ALERT_WITH_CAUTION(SET_ALERT_TEXT(@"please_enter_valid_email"));
    } else {
        [self.textFieldFirstName resignFirstResponder];
        [self.textFieldLastName resignFirstResponder];
        [self.textFieldEmail resignFirstResponder];
        START_HUD
        [APP_DELEGATE.apiManager updateUserProfileWithMobile:APP_DELEGATE.dictUserInfo[@"mobile_no"] FirstName:self.textFieldFirstName.text LastName:self.textFieldLastName.text EmailAddress:self.textFieldEmail.text UserProfile:selectedProfile withCallBack:^(BOOL success,NSDictionary * dictResponse, NSString *serverMessange) {
            if (success){
                
                NSString * imageURL = APP_DELEGATE.dictUserInfo[@"user_image"];
                [[SDImageCache sharedImageCache] removeImageForKey:imageURL fromDisk:YES];
                
                NSMutableDictionary * dictData = [[NSMutableDictionary alloc]initWithDictionary:APP_DELEGATE.dictUserInfo];
                [dictData setObject: self.textFieldFirstName.text forKey:@"first_name"];
                [dictData setObject: self.textFieldLastName.text forKey:@"last_name"];
                [dictData setObject: self.textFieldEmail.text forKey:@"email"];
                [dictData setObject:dictResponse[@"user_image"] forKey:@"user_image"];
                APP_DELEGATE.dictUserInfo = dictData;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    UIImageView * imgView = [[UIImageView alloc]init];
                    [imgView sd_setImageWithURL:[NSURL URLWithString:dictResponse[@"user_image"]] placeholderImage:[UIImage imageNamed:@"PHProfile"] options:SDWebImageRefreshCached];
                });
                
                if (self.isModeInitialUpdate){
                    APP_DELEGATE.isInitialProfileSetup = true;
                    if (!APP_DELEGATE.isUserLoggedIn){
                        APP_DELEGATE.isUserLoggedIn = true;
                    }
                    
                    HomeViewController * homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                    APP_DELEGATE.globalNavigation = [[UINavigationController alloc] initWithRootViewController:homeView];
                    
                    MainViewController *mainViewController = nil;
                    mainViewController = [[MainViewController alloc] initWithRootViewController:APP_DELEGATE.globalNavigation presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
                    [mainViewController setLeftViewSwipeGestureEnabled:false];
                    [mainViewController setRightViewSwipeGestureEnabled:false];
                    
                    APP_DELEGATE.window.rootViewController = mainViewController;
                    [APP_DELEGATE.window makeKeyAndVisible];
                    
                } else {
                    SHOW_ALERT_WITH_SUCCESS(SET_ALERT_TEXT(@"profile_update_successfully"));
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                }
            } else {
                SHOW_ALERT_WITH_CAUTION(serverMessange)
            }
            STOP_HUD
        }];
    }
}

#pragma mark - UIImagePickerController-Delegate
-(void)showImagePickerForPickImage{
    [UIImagePickerController showActionOfImagePicker:self];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.buttonProfile setImage:image forState:UIControlStateNormal];
    selectedProfile = image;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)termsButtonTapAction:(id)sender {
    LoadURLViewController * loadURLView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
    loadURLView.pageTitle = SET_ALERT_TEXT(@"terms");
    loadURLView.isModeLoadString = true;
    loadURLView.isPushed = false;
    loadURLView.cmsAPIMode = CMS_API_TERMS;
    [self presentViewController:loadURLView animated:true completion:nil];
}

- (IBAction)privacyButtonTapped:(id)sender {
    LoadURLViewController * loadURLView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
    loadURLView.pageTitle = SET_ALERT_TEXT(@"privacy");
    loadURLView.isModeLoadString = true;
    loadURLView.isPushed = false;
    loadURLView.cmsAPIMode = CMS_API_PRIVERCY;
    [self presentViewController:loadURLView animated:true completion:nil];
}

@end
