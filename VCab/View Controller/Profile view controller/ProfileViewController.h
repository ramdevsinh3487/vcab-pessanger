//
//  ProfileViewController.h
//  VCab
//
//  Created by Vishal Gohil on 19/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "UIImagePickerController+extensions.h"
#import "LoadURLViewController.h"
#import "HomeViewController.h"

@interface ProfileViewController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    UITextField * activeTextField;
    UIImage * selectedProfile;
}

@property BOOL isModeInitialUpdate;
@property BOOL canGoBack;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;
@property (weak, nonatomic) IBOutlet UIButton *buttonProfile;
@property (weak, nonatomic) IBOutlet UILabel *lableMobileNumberMessage;
@property (weak, nonatomic) IBOutlet UILabel *lableMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UIButton *buttonUpdate;

@property (weak, nonatomic) IBOutlet UILabel *lableTermsInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonTerms;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrivacyPolicy;

@end
