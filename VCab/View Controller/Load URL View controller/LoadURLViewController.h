//
//  LoadURLViewController.h
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum CMS_API_MODE{
    CMS_API_ABOUT_US,//1
    CMS_API_TERMS,//2
    CMS_API_PRIVERCY,//3
    CMS_API_HELP,//4
    CMS_API_CONTACT,//5
    CMS_API_COPYRIGHT//9
}CMSApiMode;

@interface LoadURLViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lablePageTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webViewURLLoader;

@property NSString * pageTitle;
@property NSString * pageURL;

@property CMSApiMode cmsAPIMode;
@property BOOL isModeLoadString;
@property BOOL isPushed;
@property (weak, nonatomic) IBOutlet UILabel *lableLoadingText;


@end
