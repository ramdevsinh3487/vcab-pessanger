//
//  LoadURLViewController.m
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "LoadURLViewController.h"
@interface LoadURLViewController ()

@end

@implementation LoadURLViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    self.lablePageTitle.text = self.pageTitle;
    self.lableLoadingText.text = SET_ALERT_TEXT(@"loading_data_please_wait");
    if (self.isModeLoadString){
        [self loadWebViewWithLoadableString];
    } else {
        NSURL * urlToLoad = [NSURL URLWithString:self.pageURL];
        [self.webViewURLLoader loadRequest:[NSURLRequest requestWithURL:urlToLoad]];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.lableLoadingText.text = @"";
}
- (IBAction)closeButtonTapAction:(id)sender {
    if (self.isPushed){
        [self.navigationController popViewControllerAnimated:true];
    } else {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebviewDelegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    self.lableLoadingText.text = SET_ALERT_TEXT(@"loading_data_please_wait");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    self.lableLoadingText.text = @"";
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    self.lableLoadingText.text = @"";
}

-(void)loadWebViewWithLoadableString{
    self.lableLoadingText.text = SET_ALERT_TEXT(@"loading_data_please_wait");
    NSString * loadAPIMode;
    if (self.cmsAPIMode == CMS_API_ABOUT_US){
        loadAPIMode = @"1";
    } else if (self.cmsAPIMode == CMS_API_TERMS){
        loadAPIMode = @"2";
    } else if (self.cmsAPIMode == CMS_API_PRIVERCY){
        loadAPIMode = @"3";
    } else if (self.cmsAPIMode == CMS_API_HELP){
        loadAPIMode = @"10";
    } else if (self.cmsAPIMode == CMS_API_CONTACT){
        loadAPIMode = @"5";
    } else if (self.cmsAPIMode == CMS_API_COPYRIGHT){
        loadAPIMode = @"9";
    }
    
    [APP_DELEGATE.apiManager getCMSLoadableStringWithMode:loadAPIMode lanuageId:@"1" withCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessange) {
        if (success){
            NSString * htmlLoadableString = [response[@"result"]firstObject][@"content"];
            NSString *htmlString = [NSString stringWithFormat:@"<font face='Avenir Next' size='5'>%@",[self stringByDecodingXMLEntities:htmlLoadableString]];
            
            
            [self.webViewURLLoader loadHTMLString:htmlString baseURL:nil];
        } else {
            //Will load static URL.
            if (DEBUG_MODE){
                NSLog(@"Found error : %@",errorMessange);
            }
        }
    }];
}

- (NSString *)stringByDecodingXMLEntities:(NSString *)inputString {
    
    NSUInteger myLength = [inputString length];
    NSUInteger ampIndex = [inputString rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return inputString;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:inputString];
    
    [scanner setCharactersToBeSkipped:nil];
    
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
    
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            
            if (gotNumber) {
                [result appendFormat:@"%C", (unichar)charCode];
                
                [scanner scanString:@";" intoString:NULL];
            }
            else {
                NSString *unknownEntity = @"";
                
                [scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
                
                
                [result appendFormat:@"&#%@%@", xForHex, unknownEntity];
                
                //[scanner scanUpToString:@";" intoString:&unknownEntity];
                //[result appendFormat:@"&#%@%@;", xForHex, unknownEntity];
                NSLog(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
                
            }
            
        }
        else {
            NSString *amp;
            
            [scanner scanString:@"&" intoString:&amp];  //an isolated & symbol
            [result appendString:amp];
            
            /*
             NSString *unknownEntity = @"";
             [scanner scanUpToString:@";" intoString:&unknownEntity];
             NSString *semicolon = @"";
             [scanner scanString:@";" intoString:&semicolon];
             [result appendFormat:@"%@%@", unknownEntity, semicolon];
             NSLog(@"Unsupported XML character entity %@%@", unknownEntity, semicolon);
             */
        }
        
    }
    while (![scanner isAtEnd]);
    
finish:
    return result;
}
@end
