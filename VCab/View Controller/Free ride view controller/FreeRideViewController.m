//
//  FreeRideViewController.m
//  VCab
//
//  Created by Vishal Gohil on 22/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "FreeRideViewController.h"

@interface FreeRideViewController ()

@end

@implementation FreeRideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)inviteNowButtoTapped:(id)sender {
    NSString *textToShare = @"Download the VCab App now for getting safe ride http:\\www.vrinsoft.com";
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[textToShare] applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //Exclude whichever aren't relevant
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)closeButtonTapAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)infoButtonTapAction:(id)sender {
    FreeRideInfoViewController * freeRideInfoView = [self.storyboard instantiateViewControllerWithIdentifier:@"FreeRideInfoViewController"];
    [self presentViewController:freeRideInfoView animated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
