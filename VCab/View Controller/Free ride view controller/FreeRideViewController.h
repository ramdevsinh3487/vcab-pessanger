//
//  FreeRideViewController.h
//  VCab
//
//  Created by Vishal Gohil on 22/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FreeRideInfoViewController.h"

@interface FreeRideViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;

@property (weak, nonatomic) IBOutlet UILabel *lableMessageTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableMessageDetails;
@property (weak, nonatomic) IBOutlet UILabel *lableInviteMessage;
@property (weak, nonatomic) IBOutlet UILabel *lableInviteCode;
@property (weak, nonatomic) IBOutlet UIButton *buttonInviteNow;

@end
