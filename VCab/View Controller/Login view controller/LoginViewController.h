//
//  LoginViewController.h
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerificationViewController.h"
#import <MRCountryPicker/MRCountryPicker-Swift.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>


@interface LoginViewController : UIViewController <UITextFieldDelegate, MRCountryPickerDelegate>{
    UITextField * activeTextField;
    MRCountryPicker * countryPicker;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAppLogo;
@property (weak, nonatomic) IBOutlet UIView *viewTextFieldContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableTitleText;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCountry;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMobile;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewFlag;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonGoAheadBottomSpace;
@property (weak, nonatomic) IBOutlet UIButton *buttonGoAhead;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *buttonChangeCountryCode;

@end
