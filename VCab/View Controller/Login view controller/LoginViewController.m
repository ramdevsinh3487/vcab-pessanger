//
//  LoginViewController.m
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self.navigationController setNavigationBarHidden:true];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureFound:)];
    [self.view addGestureRecognizer:tapGesture];
    
    
    
    countryPicker = [[MRCountryPicker alloc]init];
    countryPicker.countryPickerDelegate = self;
    countryPicker.showPhoneNumbers = true;
    
    countryPicker.backgroundColor = [UIColor whiteColor];
    
    CTCarrier *carrier = [[CTTelephonyNetworkInfo new] subscriberCellularProvider];
    NSString *countryCode = carrier.isoCountryCode;
    if (!countryCode || ![countryCode isEqualToString:@""]){
        countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    }
    
    NSLog(@"countryCode: %@", countryCode);
    [countryPicker setCountry:countryCode];
}

-(void)tapGestureFound:(UIGestureRecognizer *)recognizer{
    [self.textFieldMobile resignFirstResponder];
    [self.textFieldCountry resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.imageViewFlag.layer.cornerRadius = 3.0;
    self.imageViewFlag.layer.borderWidth = 0.5;
    self.imageViewFlag.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.imageViewFlag.layer.masksToBounds = true;
    
    self.containerViewHeight.constant = SCREEN_HEIGHT / 2 + 20;
    [UIView animateWithDuration:1.0 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.textFieldMobile becomeFirstResponder];
    }];
    
    self.lableTitleText.text = SET_ALERT_TEXT(@"enjoy_ride_with_vcab");
    self.textFieldMobile.placeholder = SET_ALERT_TEXT(@"mobile_no");
    [self.buttonGoAhead setTitle:SET_ALERT_TEXT(@"next_capital") forState:UIControlStateNormal];
}
- (IBAction)buttonGoAheadTapAction:(id)sender {
    [self.textFieldMobile resignFirstResponder];
    START_HUD
    [APP_DELEGATE.apiManager loginOrRegisterMobileWithCountryCode:self.textFieldCountry.text mobileNumber:self.textFieldMobile.text withCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessange) {
        if (success){
            
            VerificationViewController * verficationView = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationViewController"];
            NSMutableDictionary * dictResponse = [[NSMutableDictionary alloc]initWithDictionary:response];
            [dictResponse setObject:self.textFieldMobile.text forKey:@"mobile_no"];
            [dictResponse setObject:self.textFieldCountry.text forKey:@"country_code"];
            verficationView.dictLoginResponse = dictResponse;
            
            APP_DELEGATE.loggedUserId = response[@"user_id"];
            APP_DELEGATE.currencySymbol = [dictResponse objectForKey:@"currency_symbol"];
            APP_DELEGATE.adminMobile = [dictResponse objectForKey:@"admin_mobile"];
            APP_DELEGATE.adminEmail = [dictResponse objectForKey:@"admin_email"];
            APP_DELEGATE.tokenString = [dictResponse objectForKey:@"token"];
            APP_DELEGATE.decimapPoints = [dictResponse objectForKey:@"currency_decimal"];
            
            [self.navigationController pushViewController:verficationView animated:true];
        } else {
            SHOW_ALERT_WITH_CAUTION(errorMessange)
        }
        STOP_HUD
    }];
}
- (IBAction)changeCountryCodeButtonTapped:(id)sender {
    [self.textFieldCountry becomeFirstResponder];
}


#pragma mark - UItextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == 101){
       //Should open country picker
        textField.inputView = countryPicker;
        UIToolbar * toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50.0)];
        toolbar.backgroundColor = [UIColor lightGrayColor];
        toolbar.translucent = true;
        
        UIBarButtonItem * freeSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTappedFromToolBar:)];
        
        toolbar.items = @[freeSpace, doneButton];
        textField.inputAccessoryView = toolbar;
    } else if (textField.tag == 102){
        //This is mobile number text
    }
    activeTextField = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self isValidMobile:self.textFieldMobile.text]){
        return true;
    } else {
        return false;
    }
}

-(void)doneButtonTappedFromToolBar:(UIBarButtonItem *)doneButton{
    [activeTextField resignFirstResponder];
}

-(BOOL)isValidMobile:(NSString *)inputString{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [regexTest evaluateWithObject:inputString];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag == 101 || textField.tag == 102)
    {
        if (![string isEqualToString:@""] && textField == self.textFieldMobile && [textField.text length] >= 12){
            return false;
        }
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"01234567890"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        return YES;
    }
    
    return YES;
}

-(void)shouldShowNextButton:(BOOL)shouldShow{
    if (shouldShow == true){
        self.buttonGoAheadBottomSpace.constant = 20;
    } else {
        self.buttonGoAheadBottomSpace.constant = -70;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) { }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.containerViewHeight.constant = SCREEN_HEIGHT / 2;
    [self.view layoutIfNeeded];
    
    if (activeTextField.tag == 101){
        activeTextField.inputView = nil;
        activeTextField.inputAccessoryView = nil;
    } else {
        if ([textField.text length] > 7 && [textField.text length] < 13){
            [self shouldShowNextButton:true];
        } else {
            
            textField.textColor = [UIColor redColor];
            textField.placeholder = @"Enter Valid Mobile";
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                textField.placeholder = @"Mobile Number";
                textField.textColor = [UIColor blackColor];
                [textField becomeFirstResponder];
            });
            [self shouldShowNextButton:false];
        }
    }
}

#pragma mark - MRCountryPickerDelegate
-(void)countryPhoneCodePicker:(MRCountryPicker *)picker didSelectCountryWithName:(NSString *)name countryCode:(NSString *)countryCode phoneCode:(NSString *)phoneCode flag:(UIImage *)flag{
    self.textFieldCountry.text =  [@" " stringByAppendingString:phoneCode];
    self.imageViewFlag.image = flag;
}
@end
