//
//  HomeViewController.h
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "UIImageView+WebCache.h"
#import <GooglePlaces/GooglePlaces.h>

#import "AlertViewController.h"
#import "CurrentTripViewController.h"
#import "PaymentViewController.h"
#import "PromocodeViewController.h"
#import "PulstorViewController.h"
#import "RateCabViewController.h"
#import "RunningRideViewController.h"
#import "SearchPlaceViewController.h"
#import "SelectCarCell.h"
#import "FreeRideViewController.h"

@interface HomeViewController : UIViewController <GMSMapViewDelegate,GMSAutocompleteViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, PaymentViewControllerDelegate, PulstorViewControllerDelegate, CurrentTripViewControllerDelegate, PromocodeViewControllerDelegate, AlertViewControllerDelegate, RunningRideViewControllerDelegate>{
    
    BOOL isModePickBeginLocation;
    BOOL isInitialMarkerChanged;
    
    NSTimer * timerFindAddress;
    
    NSInteger selectedCarIndex;
    NSDictionary * dictDefaultBrand;
    NSArray * arrayCarCategories;

    NSMutableArray * arrNearByCarMarkers;
    
    GMSAutocompleteViewController *acController;
    
    NSString * selectedPromocodeId;
    
    BOOL isAnimationStarted;
    
    IBOutlet UILabel *lblNoCarAvailable;
}

@property int counter;
@property int timerStatus;
@property int tempCount;

@property (weak, nonatomic) IBOutlet UIButton *buttonTrackMyLocation;
@property (weak, nonatomic) IBOutlet UIView *viewTrackMyLocationShadow;

@property (weak, nonatomic) IBOutlet GMSMapView *viewGMapView;
@property (weak, nonatomic) IBOutlet UIButton *buttonMenu;

//Location Details Container
@property (weak, nonatomic) IBOutlet UIView *viewLocationDetailsContainer;

@property (weak, nonatomic) IBOutlet UIView *viewPickupContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableBeginLocationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableBeginLocationDetails;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBeginIcon;

@property (weak, nonatomic) IBOutlet UIView *viewDestinationContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableDestinationLocationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableDestinationLocationDetail;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDestinationIcon;

//Send Request Container
@property (weak, nonatomic) IBOutlet UIView *viewSendRequestContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *requestContainerBottomSpace;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAvailableCars;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPaymentIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDownArrow;
@property (weak, nonatomic) IBOutlet UILabel *lablePaymentMethod;
@property (weak, nonatomic) IBOutlet UIButton *buttonSendRequest;

@property (weak, nonatomic) IBOutlet UIView *viewApplyPromocode;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *applyPromocodeHeight;
@property (weak, nonatomic) IBOutlet UILabel *lableApplyPromoTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonApplyPromocode;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPromocodeIcon;

//Running Ride Container
@property (weak, nonatomic) IBOutlet UIView *viewRunningRideBanner;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *runningRideBannerBottomSpace;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRunningRideIcon;
@property (weak, nonatomic) IBOutlet UIView *viewRideInfoContainer;
@property (weak, nonatomic) IBOutlet UIButton *buttonContact;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverRatings;

@property (weak, nonatomic) IBOutlet UILabel *lableCarBrandNAme;
@property (weak, nonatomic) IBOutlet UILabel *lableCarPlate;

@property (weak, nonatomic) IBOutlet UIButton *buttonShowRunningRide;
@end
