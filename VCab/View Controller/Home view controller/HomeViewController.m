//
//  HomeViewController.m
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize counter,tempCount,timerStatus;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    [APP_DELEGATE updateDeviceTokenToServer];
    
    isInitialMarkerChanged = false; 
    
    [self performSelector:@selector(currentLocation) withObject:nil afterDelay:1.0];
    [self updateCustomerCurrentLocation];
    
    [self resloadTheMapViewWithClear];
    
    selectedPromocodeId = @"0";
    isAnimationStarted = false;
    
    lblNoCarAvailable.text = SET_ALERT_TEXT(@"no_car_available_right_now");
    
    [APP_DELEGATE updateUserProfileDetails];
} 

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initializeNotificationObservers];
    
    if (APP_DELEGATE.tripStatus != RIDE_STATUS_SEND_REQUEST){
        self.viewLocationDetailsContainer.hidden = APP_DELEGATE.isTripRunning;
        [self shouldShowRunnigRideBanner:APP_DELEGATE.isTripRunning];
    }
    APP_DELEGATE.selectedIndex = 0;
    [self restoreGoogleMapWithMarkersAndRoutes];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)InitializeRequestContainerViewAfterPromoCodeList{
    
    [self initializeSendRequestContainer];
    [self initializeRunningRideBannerView];
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - NSNotificationCenter
-(void)initializeNotificationObservers {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationClearMapView:) name:NOTI_CLEAR_HOME_MAP object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationReceivedForUserLocationUpdates:) name:NOTI_LOCATION_UPDATES object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateCurrentLocationFromPushNotification:) name:NOTI_UPDATE_CURRENT_LOCATION_FORM_PUSH object:nil];
    
    
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(InitializeRequestContainerViewAfterPromoCodeList) name:NOTI_PROMOCODE_RESPONCE object:nil];
    
}

-(void)updateCurrentLocationFromPushNotification:(NSNotification *)notif{
    [self updateCustomerCurrentLocation];
}

-(void)notificationClearMapView:(NSNotification *)notif{
    [self resloadTheMapViewWithClear];
    [self.viewGMapView animateToLocation:APP_DELEGATE.tripStartLocation];
    [self restoreGoogleMapWithMarkersAndRoutes];
}

#pragma mark - DriverCar & NearByCar Corner
-(void)updateNarbyCarArray:(NSArray *)resourceArray{
    [APP_DELEGATE.arrayNearByCars removeAllObjects];
    APP_DELEGATE.arrayNearByCars = [[NSMutableArray alloc]init];
    for (NSDictionary * driver in resourceArray){
        NSString * user_id = driver[@"user_id"];
        NSString * car_plate = driver[@"car_plat_no"];
        NSString * lattitude = driver[@"latitude"];
        NSString * longitude = driver[@"longitude"];
        
        [APP_DELEGATE.arrayNearByCars addObject:@{
                                                  @"driver_id":user_id,
                                                  @"car_plate":car_plate,
                                                  @"lattitude":lattitude,
                                                  @"longitude":longitude}];
    }
    
    if (DEBUG_MODE){
        NSLog(@"Car details updated");
    }
}

#pragma IBOutletCenter
- (IBAction)menuButtonTapped:(UIButton *)sender {
    if (sender.tag == 111){
        SHOW_MENU
    } else {
        [self shouldAdjustSednRequestConatinerShouldShow:true canShowButton:true];
        [APP_DELEGATE clearPrevousTripDataFromApp];
        [self resloadTheMapViewWithClear];
        [self restoreGoogleMapWithMarkersAndRoutes];
        [self shouldAdjustSednRequestConatinerShouldShow:false canShowButton:false];
    }
}

- (IBAction)addPinButtonTapAction:(UIButton *)sender {
    //We can add drop pin here
}

- (IBAction)enterBeginLocationButtonTapped:(id)sender {
    
    
    BOOL isLocationServiceOn = [APP_DELEGATE CheckLocationPermission];
    if (isLocationServiceOn)
    {
        //Will Open google place picker
        isModePickBeginLocation = true;
        [self changeShadowDynamicallyWithLocationModeShouldClear:false];
        
        if (APP_DELEGATE.tripStartLocation.latitude > 00.00000 && APP_DELEGATE.tripStartLocation.longitude > 00.00000){
            [self.viewGMapView animateToLocation:APP_DELEGATE.tripStartLocation];
            [self.viewGMapView animateToZoom:18.0];
        }
        RunningRideViewController * runningRideView = [self.storyboard instantiateViewControllerWithIdentifier:@"RunningRideViewController"];
        runningRideView.isModeSearchLocation = true;
        runningRideView.delegate = self;
        runningRideView.searchLocationMode = SLM_SET_START_LOCATION;
        [self presentViewController:runningRideView animated:true completion:nil];
    }
    
    
    
}

- (IBAction)enterDestinationLocationButtonTapped:(id)sender {
    
    BOOL isLocationServiceOn = [APP_DELEGATE CheckLocationPermission];
    if (isLocationServiceOn)
    {
        //Will Open google place picker
        isModePickBeginLocation = false;
        [self changeShadowDynamicallyWithLocationModeShouldClear:false];
        
        if (APP_DELEGATE.tripEndLocation.latitude > 00.00000 && APP_DELEGATE.tripEndLocation.longitude > 00.00000){
            [self.viewGMapView animateToLocation:APP_DELEGATE.tripEndLocation];
            [self.viewGMapView animateToZoom:18.0];
        }
        
        RunningRideViewController * runningRideView = [self.storyboard instantiateViewControllerWithIdentifier:@"RunningRideViewController"];
        runningRideView.isModeSearchLocation = true;
        runningRideView.delegate = self;
        runningRideView.searchLocationMode = SLM_SET_END_LOCATION;
        [self presentViewController:runningRideView animated:true completion:nil];
    }
    else
    {
        
    }
   
}

-(void)changeShadowDynamicallyWithLocationModeShouldClear:(BOOL)shouldClear{
    if (shouldClear){
        self.viewDestinationContainer.layer.shadowOpacity = 0.0;
        self.viewPickupContainer.layer.shadowOpacity = 0.0;
    } else {
        if (isModePickBeginLocation){
            self.viewDestinationContainer.layer.shadowOpacity = 0.0;
            self.viewPickupContainer.layer.shadowOpacity = 0.7;
            [self.viewLocationDetailsContainer bringSubviewToFront:self.viewPickupContainer];
        } else {
            self.viewPickupContainer.layer.shadowOpacity = 0.0;
            self.viewDestinationContainer.layer.shadowOpacity = 0.7;
            [self.viewLocationDetailsContainer bringSubviewToFront:self.viewDestinationContainer];
        }
    }
}

#pragma mark - GooglePlacePickerDelegate
- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    if (isModePickBeginLocation){
        APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
        [self.viewGMapView animateToLocation:APP_DELEGATE.tripStartLocation];
    } else {
        APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
        [self.viewGMapView animateToLocation:APP_DELEGATE.tripEndLocation];
    }
    [self restoreGoogleMapWithMarkersAndRoutes];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (DEBUG_MODE){
        NSLog(@"GMS Autocomplete Error : %@", [error description]);
    }
    
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {    
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - SendRequiestContainerViewView
-(void)initializeSendRequestContainer{
    
    if (DEBUG_MODE){
        NSLog(@"Car brand list : %@",APP_DELEGATE.arrayCarCategories);
    }
    
    arrayCarCategories = APP_DELEGATE.arrayCarCategories;
    dictDefaultBrand = APP_DELEGATE.dictDefaultBrand;

    if (APP_DELEGATE.arrayPromoCodes.count > 0){
        self.applyPromocodeHeight.constant = 50;
        self.viewApplyPromocode.hidden = false;
        
        if ([selectedPromocodeId isEqualToString:@"0"]){
            self.imageViewPromocodeIcon.hidden = true;
            self.imageViewDownArrow.hidden = true;
            self.lableApplyPromoTitle.hidden = true;
            [self.buttonApplyPromocode setTitle:SET_ALERT_TEXT(@"have_a_promocode") forState:UIControlStateNormal];
        } else {
            self.imageViewPromocodeIcon.hidden = false;
            self.imageViewDownArrow.hidden = false;
            self.lableApplyPromoTitle.hidden = false;
            [self.buttonApplyPromocode setTitle:@"" forState:UIControlStateNormal];
        }
    } else {
        self.applyPromocodeHeight.constant = 0;
        self.viewApplyPromocode.hidden = true;
    }
    
    self.lablePaymentMethod.text = APP_DELEGATE.dictDefaultPayment[@"title"];
    self.imageViewPaymentIcon.image = [UIImage imageNamed: APP_DELEGATE.dictDefaultPayment[@"image"]];
    
    if ((APP_DELEGATE.tripStartLocation.latitude == 00.000 && APP_DELEGATE.tripStartLocation.longitude == 00.000) || (APP_DELEGATE.tripEndLocation.latitude == 00.000 && APP_DELEGATE.tripEndLocation.longitude == 00.000)){
        //arrayCarCategories = [[NSArray alloc]init];
        self.requestContainerBottomSpace.constant = -235 - self.applyPromocodeHeight.constant;
        [self.view layoutIfNeeded];
    }
    
    if (dictDefaultBrand.count > 0){
        selectedCarIndex = [arrayCarCategories indexOfObject:dictDefaultBrand];
        if (selectedCarIndex > arrayCarCategories.count){
            selectedCarIndex = 0;
        }
    }
    
    [self.collectionViewAvailableCars reloadData];
    if (APP_DELEGATE.arrayCarCategories.count > 0) {
        //        NSIndexPath *indexpath = [NSIndexPath indexPathForItem:selectedCarIndex inSection:0];
        //        [self.collectionViewAvailableCars scrollToItemAtIndexPath:indexpath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

-(void)shouldAdjustSednRequestConatinerShouldShow:(BOOL)shouldShowContainer canShowButton:(BOOL)canShowButton{
    
    float viewHeight = self.viewSendRequestContainer.frame.size.height;
    
    if (shouldShowContainer){
        
        [self.buttonMenu setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
        [self.buttonMenu setTag:222];
        
        [self initializeSendRequestContainer];
        self.buttonSendRequest.tag = 101;
        
        [self.buttonSendRequest setTitle:SET_ALERT_TEXT(@"send_request") forState:UIControlStateNormal];
        if (self.requestContainerBottomSpace.constant < 0){
            self.requestContainerBottomSpace.constant = 0;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    } else {
        if (self.requestContainerBottomSpace.constant > (0 - viewHeight) + 50) {
            if (canShowButton){
                self.requestContainerBottomSpace.constant =(0 - viewHeight) + 50;
                self.buttonSendRequest.tag = 102;
                [self.buttonSendRequest setTitle:SET_ALERT_TEXT(@"show_request") forState:UIControlStateNormal];
            } else {
                
                [self.buttonMenu setImage:[UIImage imageNamed:@"menuBlack"] forState:UIControlStateNormal];
                [self.buttonMenu setTag:111];
                
                self.requestContainerBottomSpace.constant =(0 - viewHeight);
                self.buttonSendRequest.tag = 101;
                [self.buttonSendRequest setTitle:SET_ALERT_TEXT(@"send_request") forState:UIControlStateNormal];
            }
            
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}


- (IBAction)buttonSendRequestForCar:(UIButton *)sender {
    [self changeShadowDynamicallyWithLocationModeShouldClear:true];
    
    if (sender.tag == 101){
        

        MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
        MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.tripEndLocation);
        
        if (MKMetersBetweenMapPoints(point1, point2) < 800.0){
            SHOW_ALERT_WITH_CAUTION(SET_ALERT_TEXT(SET_ALERT_TEXT(@"required_distance_message")));
        } else  if (!APP_DELEGATE.arrayCarCategories || APP_DELEGATE.arrayCarCategories.count <= 0 || !APP_DELEGATE.arrayNearByCars || APP_DELEGATE.arrayNearByCars.count <= 0){
            SHOW_ALERT_WITH_CAUTION(SET_ALERT_TEXT(@"no_driver_found"));
        } else {
            [self shouldAdjustSednRequestConatinerShouldShow:false canShowButton:false];
            
            START_HUD
            NSString * carBrandId = APP_DELEGATE.dictDefaultBrand[@"car_brand_id"];
            NSString * pickupAddress = self.lableBeginLocationDetails.text;
            NSString * dropAddress = self.lableDestinationLocationDetail.text;
            NSString * pickupLattitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.tripStartLocation.latitude];
            NSString * pickupLongitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.tripStartLocation.longitude];
            NSString * dropLattitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.tripEndLocation.latitude];
            NSString * dropLongitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.tripEndLocation.longitude];
            
            NSDateFormatter *formater = [[NSDateFormatter alloc]init];
            [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString * pickup_date_time = [formater stringFromDate:[NSDate date]];
            
            [APP_DELEGATE.apiManager sendReuqestWithCarBrandId:carBrandId PickupAddress:pickupAddress DropAddress:dropAddress PickupLattitude:pickupLattitude PickupLongitude:pickupLongitude DropLattitude:dropLattitude DropLongitude:dropLongitude PickupDateTime:pickup_date_time tripDistance:[NSString stringWithFormat:@"%f",APP_DELEGATE.tripDistance] tripDuration:[NSString stringWithFormat:@"%f",APP_DELEGATE.tripDuration] promoCodeId:selectedPromocodeId paymentType:APP_DELEGATE.dictDefaultPayment[@"payment_type"]  withCallBack:^(BOOL success, NSDictionary *response, NSString *errorMessange) {
                if (success){
                    APP_DELEGATE.runningTripId = response[@"ride_id"];
                    APP_DELEGATE.isTripRunning = true;
                    APP_DELEGATE.tripStatus = RIDE_STATUS_SEND_REQUEST;
                    APP_DELEGATE.sendRequestDate = [NSDate date];
                    
                    if (DEBUG_MODE){
                        NSLog(@"Send Request Date : %@",APP_DELEGATE.sendRequestDate);
                        NSLog(@"Current Date : %@",[NSDate date]);
                    }
                    
                    PulstorViewController * pulstorView = [self.storyboard instantiateViewControllerWithIdentifier:@"PulstorViewController"];
                    pulstorView.delegate = self;
                    pulstorView.modalPresentationStyle = UIModalPresentationOverFullScreen;
                    [self presentViewController:pulstorView animated:true completion:nil];
                } else {
                    SHOW_ALERT_WITH_CAUTION(errorMessange)
                    [self restoreGoogleMapWithMarkersAndRoutes];
                }
                STOP_HUD
            }];
        }
    } else {
        [self shouldAdjustSednRequestConatinerShouldShow:true canShowButton:true];
    }
}

- (IBAction)buttonSelectPaymentMethodTapped:(id)sender {
    PaymentViewController * paymentView = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
    paymentView.isModeSelection = true;
    paymentView.delegate = self;
    [self.navigationController pushViewController:paymentView animated:true];
}

- (IBAction)changePromocodeButtonTapped:(id)sender {
    //Will open promo code
    PromocodeViewController * promocodeView = [self.storyboard instantiateViewControllerWithIdentifier:@"PromocodeViewController"];
    promocodeView.delegate = self;
    promocodeView.promocodeId = selectedPromocodeId;
    [self presentViewController:promocodeView animated:true completion:nil];
}

-(void)didSelectPromocodeToApply:(NSDictionary *)dictPromocode{
    
    if (dictPromocode && dictPromocode.count > 0 && ![dictPromocode[@"id"] isEqualToString:@"0"]){
        selectedPromocodeId = dictPromocode[@"id"];
        self.imageViewPromocodeIcon.hidden = false;
        self.imageViewDownArrow.hidden = false;
        self.lableApplyPromoTitle.hidden = false;
        self.lableApplyPromoTitle.text = [NSString stringWithFormat:@"%@ %% %@",dictPromocode[@"percentage"],SET_ALERT_TEXT(@"cashback")];
        [self.buttonApplyPromocode setTitle:@"" forState:UIControlStateNormal];
    } else {
        selectedPromocodeId = @"0";
        self.imageViewPromocodeIcon.hidden = true;
        self.imageViewDownArrow.hidden = true;
        self.lableApplyPromoTitle.hidden = true;
        self.lableApplyPromoTitle.text = @"";
        [self.buttonApplyPromocode setTitle:SET_ALERT_TEXT(@"have_a_promocode") forState:UIControlStateNormal];
    }
    
}

#pragma mark - UICollectionViewDelegate&DataSource
//SelectCarCollectionView tag == 201
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width / 3, 135);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (arrayCarCategories.count > 0){
            self.collectionViewAvailableCars.hidden = false;
            lblNoCarAvailable.hidden = true;
        } else {
            self.collectionViewAvailableCars.hidden = true;
            lblNoCarAvailable.hidden = false;
        }
    });
    return arrayCarCategories.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SelectCarCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SelectCarCell" forIndexPath:indexPath];
    cell.lableCarName.text = arrayCarCategories[indexPath.row][@"car_brand_name"];
    
    NSString * basefare = arrayCarCategories[indexPath.row][@"base_fare"];
    NSString * minimumCharges = arrayCarCategories[indexPath.row][@"min_charge"];
    NSString * minuteCharges = arrayCarCategories[indexPath.row][@"minute_price"];
    NSString * kmCharges = arrayCarCategories[indexPath.row][@"km_price"];
    
    cell.lableCarDetails.text = [APP_DELEGATE countEstimatedCostWithKM:APP_DELEGATE.tripDistance time:APP_DELEGATE.tripDuration minCharges:minimumCharges basefare:basefare minutePrice:minuteCharges kmPrice:kmCharges];

    NSString * imageURL= arrayCarCategories[indexPath.row][@"car_brand_image"];
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, cell.imageViewCar.frame.size.width, cell.imageViewCar.frame.size.height)];
    [cell.imageViewCar addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    [cell.imageViewCar sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
    }];
    
    cell.imageViewCar.layer.cornerRadius = cell.imageViewCar.frame.size.height / 2;
    cell.imageViewCar.layer.borderWidth = 2.5;
    

    if (indexPath.row == selectedCarIndex){
        cell.imageViewCar.layer.borderColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"themeColor"]].CGColor;
        cell.imageSelectCar.hidden = false;
    } else {
        cell.imageViewCar.layer.borderColor = [UIColor darkGrayColor].CGColor;
        cell.imageSelectCar.hidden = true;
    }
    cell.imageViewCar.layer.masksToBounds = true;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    int oldIndex = (int)selectedCarIndex;
    
    if (oldIndex != indexPath.row){
        selectedCarIndex = indexPath.row;
        [collectionView reloadData];
        
        APP_DELEGATE.dictDefaultBrand = arrayCarCategories[indexPath.row];
        dictDefaultBrand = arrayCarCategories[indexPath.row];
        
        START_HUD
        
        for (GMSMarker * driverMarker in arrNearByCarMarkers){
            if (DEBUG_MODE){
                NSLog(@"Marker Will Remove : %@", driverMarker.title);
            }
            driverMarker.map = nil;
        }
        
        [arrNearByCarMarkers removeAllObjects];
        [APP_DELEGATE.arrayNearByCars removeAllObjects];
        APP_DELEGATE.arrayNearByCars = nil;
        arrNearByCarMarkers = nil;
        
        [self updateCustomerCurrentLocation];
    }
}

- (IBAction)trackMyLocationButtonTapped:(id)sender {
    
    BOOL isLocationServiceOn = [APP_DELEGATE CheckLocationPermission];
    if (isLocationServiceOn)
    {
    
     [self.viewGMapView animateToLocation: self.viewGMapView.myLocation.coordinate];
     [self resloadTheMapViewWithClear];
        
        
        if (APP_DELEGATE.dictTripStartAddress[@"title"] && [APP_DELEGATE.dictTripStartAddress[@"title"] isEqualToString:@""] && APP_DELEGATE.dictTripStartAddress[@"address"] && [APP_DELEGATE.dictTripStartAddress[@"address"] isEqualToString:@""]){
            
            [self restoreGoogleMapWithMarkersAndRoutes];
        }
        
        
    }
    
    
}

#pragma mark - PaymentViewControllerDelegate
-(void)selectedPaymentMethodWithData:(NSDictionary *)dictPaymentMethod{
    self.imageViewPaymentIcon.image = [UIImage imageNamed:dictPaymentMethod[@"image"]];
    if ([dictPaymentMethod[@"type"] isEqualToString:@"card"]){
        self.lablePaymentMethod.text = dictPaymentMethod[@"title"];
        self.lablePaymentMethod.text = SET_ALERT_TEXT(@"cash_capital");
    } else {
        self.lablePaymentMethod.text = dictPaymentMethod[@"title"];
    }
}


#pragma mark - GoogleSDKSection
#pragma mark ******************
#pragma mark - GoogleMapViewInitializeAndUserOperations
- (void)resloadTheMapViewWithClear {
    [self.viewGMapView clear];
    
    self.lableDestinationLocationTitle.text = SET_ALERT_TEXT(@"destination_location");
    self.lableBeginLocationTitle.text = SET_ALERT_TEXT(@"my_location");
    self.lableBeginLocationDetails.text = @"";
    self.lableDestinationLocationDetail.text = @"";
    
    self.viewPickupContainer.layer.shadowOpacity = 0.0;
    self.viewDestinationContainer.layer.shadowOpacity = 0.0;
    isInitialMarkerChanged = false;
    
    GMSCameraPosition *camera;
    if (APP_DELEGATE.userLocation.latitude != 00.00000 && APP_DELEGATE.userLocation.longitude != 00.00000){
        camera = [GMSCameraPosition cameraWithLatitude:APP_DELEGATE.userLocation.latitude longitude:APP_DELEGATE.userLocation.longitude zoom:18];
        if (!APP_DELEGATE.isTripRunning){
            APP_DELEGATE.tripStartLocation = APP_DELEGATE.userLocation;
        }
    } else {
      //  camera = [GMSCameraPosition cameraWithLatitude:23.0752 longitude:72.5257 zoom:18];
    }

    self.viewGMapView.camera = camera;
    self.viewGMapView.myLocationEnabled = true;
    self.viewGMapView.delegate = self;
    self.viewGMapView.settings.rotateGestures = false;
    self.viewLocationDetailsContainer.hidden = APP_DELEGATE.isTripRunning;
    [self shouldShowRunnigRideBanner:APP_DELEGATE.isTripRunning];
}

-(void)restoreGoogleMapWithMarkersAndRoutes{
    [self.viewGMapView clear];
    
    if (DEBUG_MODE){
        NSLog(@"trip Start Location : %f and %f",APP_DELEGATE.tripStartLocation.latitude,APP_DELEGATE.tripStartLocation.longitude);
        NSLog(@"trip End Location : %f and %f",APP_DELEGATE.tripEndLocation.latitude,APP_DELEGATE.tripEndLocation.longitude);
        NSLog(@"Is trip running : %d",(int)APP_DELEGATE.isTripRunning);
        NSLog(@"Current Status : %u", APP_DELEGATE.tripStatus);
        NSLog(@"Trip id : %@", APP_DELEGATE.runningTripId);
        NSLog(@"Driver Id : %@", APP_DELEGATE.tripDriverId);
    }
    
    if (APP_DELEGATE.tripStartLocation.latitude != 00.000 && APP_DELEGATE.tripStartLocation.longitude != 00.000){
        [self.viewGMapView animateToLocation:APP_DELEGATE.tripStartLocation];
        if (APP_DELEGATE.dictTripStartAddress){
            if (APP_DELEGATE.dictTripStartAddress[@"title"] && ![APP_DELEGATE.dictTripStartAddress[@"title"] isEqualToString:@""] && APP_DELEGATE.dictTripStartAddress[@"address"] && ![APP_DELEGATE.dictTripStartAddress[@"address"] isEqualToString:@""]){
                self.lableBeginLocationTitle.text = APP_DELEGATE.dictTripStartAddress[@"title"];
                self.lableBeginLocationDetails.text = APP_DELEGATE.dictTripStartAddress[@"address"];
                [self addStartMarkerWithTitle:self.lableBeginLocationTitle.text];
            } else {
                [self findAddressForCoordinate:APP_DELEGATE.tripStartLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                    self.lableBeginLocationTitle.text = dictAddress[@"title"];
                    self.lableBeginLocationDetails.text = dictAddress[@"fullAddress"];
                    [self addStartMarkerWithTitle:self.lableBeginLocationTitle.text];
                    
                    APP_DELEGATE.dictTripStartAddress = @{
                                                          @"title":self.lableBeginLocationTitle.text,
                                                          @"address":self.lableBeginLocationDetails.text,
                                                          @"lattitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripStartLocation.latitude],
                                                          @"longitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripStartLocation.longitude]
                                                          };
                }];
            }
        } else {
            [self findAddressForCoordinate:APP_DELEGATE.tripStartLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                self.lableBeginLocationTitle.text = dictAddress[@"title"];
                self.lableBeginLocationDetails.text = dictAddress[@"fullAddress"];
                [self addStartMarkerWithTitle:self.lableBeginLocationTitle.text];
                
                APP_DELEGATE.dictTripStartAddress = @{
                                                      @"title":self.lableBeginLocationTitle.text,
                                                      @"address":self.lableBeginLocationDetails.text,
                                                      @"lattitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripStartLocation.latitude],
                                                      @"longitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripStartLocation.longitude]
                                                      };
            }];
        }
    }
    
    if (APP_DELEGATE.tripEndLocation.latitude != 00.000 && APP_DELEGATE.tripEndLocation.longitude != 00.000){
        [self.viewGMapView animateToLocation:APP_DELEGATE.tripEndLocation];
        if (APP_DELEGATE.dictTripEndAddress){
            if (APP_DELEGATE.dictTripEndAddress[@"title"] && ![APP_DELEGATE.dictTripEndAddress[@"title"] isEqualToString:@""] && APP_DELEGATE.dictTripEndAddress[@"address"] && ![APP_DELEGATE.dictTripEndAddress[@"address"] isEqualToString:@""]){
                self.lableDestinationLocationTitle.text = APP_DELEGATE.dictTripEndAddress[@"title"];
                self.lableDestinationLocationDetail.text = APP_DELEGATE.dictTripEndAddress[@"address"];
                [self addEndMarkerWithTitle:self.lableDestinationLocationTitle.text];
            } else {
                [self findAddressForCoordinate:APP_DELEGATE.tripEndLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                    self.lableDestinationLocationTitle.text = dictAddress[@"title"];
                    self.lableDestinationLocationDetail.text = dictAddress[@"fullAddress"];
                    [self addEndMarkerWithTitle:self.lableDestinationLocationTitle.text];
                    
                    APP_DELEGATE.dictTripEndAddress = @{
                                                          @"title":self.lableDestinationLocationTitle.text,
                                                          @"address":self.lableDestinationLocationDetail.text,
                                                          @"lattitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripEndLocation.latitude],
                                                          @"longitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripEndLocation.longitude]
                                                          };
                }];
            }
        } else {
            [self findAddressForCoordinate:APP_DELEGATE.tripEndLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                self.lableDestinationLocationTitle.text = dictAddress[@"title"];
                self.lableDestinationLocationDetail.text = dictAddress[@"fullAddress"];
                [self addEndMarkerWithTitle:self.lableDestinationLocationTitle.text];
                
                APP_DELEGATE.dictTripEndAddress = @{
                                                    @"title":self.lableDestinationLocationTitle.text,
                                                    @"address":self.lableDestinationLocationDetail.text,
                                                    @"lattitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripEndLocation.latitude],
                                                    @"longitude":[NSString stringWithFormat:@"%0.10f",APP_DELEGATE.tripEndLocation.longitude]
                                                    };
            }];
        }
    }
    
    if (APP_DELEGATE.tripStartLocation.latitude != 00.000 && APP_DELEGATE.tripStartLocation.longitude != 00.000 && APP_DELEGATE.tripEndLocation.latitude != 00.000 && APP_DELEGATE.tripEndLocation.longitude != 00.000){
        [self adjustMapviewToViewAllMarkers];
        isInitialMarkerChanged= true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.tripStartLocation Destination:APP_DELEGATE.tripEndLocation isDotedLine:false withCallback:^(BOOL success) {
            if (!APP_DELEGATE.isTripRunning){
                [self shouldAdjustSednRequestConatinerShouldShow:true canShowButton:true];
            }
        }];
    }

    if (APP_DELEGATE.isTripRunning){
        APP_DELEGATE.arrayNearByCars = [[NSMutableArray alloc]init];
        [self updateCarMarkersNotification];
        APP_DELEGATE.markerDriverLocation.map = self.viewGMapView;
    } else {
        
        if (APP_DELEGATE.arrayNearByCars.count > 0){
            [self updateCarMarkersNotification];
        }
    }
}

-(void)addStartMarkerWithTitle:(NSString *)title{
    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
    markerToAdd.position = APP_DELEGATE.tripStartLocation;
    markerToAdd.title = title;
    [markerToAdd setDraggable:!APP_DELEGATE.isTripRunning];
    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerPickup"];
    markerToAdd.map = self.viewGMapView;
}

-(void)addEndMarkerWithTitle:(NSString *)title{
    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
    markerToAdd.position = APP_DELEGATE.tripEndLocation;
    markerToAdd.title = title;
    [markerToAdd setDraggable:!APP_DELEGATE.isTripRunning];
    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerDestination"];
    markerToAdd.map = self.viewGMapView;
}

-(void)notificationReceivedForUserLocationUpdates:(NSNotification *)notif{
    [self.viewGMapView setMyLocationEnabled:true];
    
    if (!isInitialMarkerChanged){
        if (DEBUG_MODE){
            NSLog(@"User Location : %f and %f",APP_DELEGATE.userLocation.latitude,APP_DELEGATE.userLocation.longitude);
        }
        
        if (APP_DELEGATE.tripStartLocation.latitude != APP_DELEGATE.userLocation.latitude || APP_DELEGATE.tripStartLocation.longitude != APP_DELEGATE.userLocation.longitude){
            
            MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
            MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
            float distance = MKMetersBetweenMapPoints(point1, point2);
            if (distance >= 10.0){
                APP_DELEGATE.tripStartLocation = APP_DELEGATE.userLocation;
                [self restoreGoogleMapWithMarkersAndRoutes];
            }
        }
    }
    
    if (APP_DELEGATE.isTripRunning == false && ([self.lableBeginLocationDetails.text isEqualToString:@""] || (APP_DELEGATE.tripStartLocation.latitude == 0 && APP_DELEGATE.tripStartLocation.longitude == 0))){
        APP_DELEGATE.tripStartLocation = APP_DELEGATE.userLocation;
        [self restoreGoogleMapWithMarkersAndRoutes];
    }
}

-(void)adjustMapviewToViewAllMarkers{
    GMSMutablePath *pathMapPath = [GMSMutablePath path];
    if (APP_DELEGATE.tripStartLocation.latitude > 00.000 && APP_DELEGATE.tripStartLocation.longitude > 00.000){
        [pathMapPath addCoordinate:APP_DELEGATE.tripStartLocation];
    }
    
    if ( APP_DELEGATE.tripEndLocation.latitude > 00.000 && APP_DELEGATE.tripEndLocation.longitude > 00.000){
        [pathMapPath addCoordinate:APP_DELEGATE.tripEndLocation];
    }
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathMapPath];
    [self.viewGMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
}

-(void)findAddressForCoordinate:(CLLocationCoordinate2D)location WithCallback:(void (^) (BOOL success, NSDictionary * dictAddress))callback{
    [APP_DELEGATE.apiManager findAddressUsingLocationCoordinate:location withCallBack:^(BOOL success, NSDictionary *result, NSString *error) {
        if (success){
            callback(true,result);
        } else {
            [[GMSGeocoder geocoder] reverseGeocodeCoordinate:location completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
                if (response.results.count > 0){
                    
                    NSLog(@"%@",response);
                    
                    GMSAddress* addressObj =  response.firstResult;
                    NSString * fullAddress = @"";
                    for (NSString * title in addressObj.lines){
                        fullAddress = [fullAddress stringByAppendingString:title];
                    }
                    
                    NSLog(@"%@", addressObj.lines);
                    
                    NSString *title;
                    if (addressObj.thoroughfare){
                        title = addressObj.thoroughfare;
                    } else{
                        title = addressObj.addressLine1;
                    }
                    
                    callback(true, @{@"title":title,@"fullAddress":fullAddress});
                } else {
                    callback(false, @{@"title":@"Current Location",@"fullAddress":@""});
                }
            }];
        }
    }];
}

-(void)drawRouteWithMapViewForOrigin:(CLLocationCoordinate2D)origin Destination:(CLLocationCoordinate2D)destination isDotedLine:(BOOL)isDoted withCallback:(void (^) (BOOL success))callback{
    START_HUD
    [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:origin destination:destination withCallBack:^(BOOL success, NSArray *result, NSString * error) {
        if (success){
            for (NSDictionary *routeDict in result){
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                
                GMSPolyline *polyline = [GMSPolyline polylineWithPath: [GMSPath pathFromEncodedPath: points]];
                
                polyline.map = self.viewGMapView;
                polyline.strokeColor = [UIColor blackColor];
                
                polyline.strokeWidth = 2.0f;
                
                if (isDoted){
                    NSArray *styles = @[[GMSStrokeStyle solidColor:[UIColor blackColor]],
                                        [GMSStrokeStyle solidColor:[UIColor clearColor]]];
                    
                    NSArray *lengths = @[@50, @100];
                    polyline.spans = GMSStyleSpans(polyline.path, styles, lengths, kGMSLengthRhumb);
                    
                    NSString * textDistance = [routeDict objectForKey:@"legs"][0][@"distance"][@"text"];
                    if ([textDistance containsString:@"."]){
                        textDistance = [textDistance substringToIndex:3];
                    } else {
                        textDistance = [textDistance substringToIndex:1];
                    }
                    
                    NSString * textDuration = [routeDict objectForKey:@"legs"][0][@"duration"][@"text"];
                    if ([textDuration containsString:@"."]){
                        textDuration = [textDuration substringToIndex:3];
                    } else {
                        textDuration = [textDuration substringToIndex:1];
                    }
                    
                    APP_DELEGATE.tripDistance = [textDistance doubleValue];
                    APP_DELEGATE.tripDuration = [textDuration doubleValue];

                } else {
                    NSString * textDistance = [routeDict objectForKey:@"legs"][0][@"distance"][@"text"];
                    if ([textDistance containsString:@"."]){
                        textDistance = [textDistance substringToIndex:3];
                    } else {
                        textDistance = [textDistance substringToIndex:1];
                    }
                    
                    NSString * textDuration = [routeDict objectForKey:@"legs"][0][@"duration"][@"text"];
                    if ([textDuration containsString:@"."]){
                        textDuration = [textDuration substringToIndex:3];
                    } else {
                        textDuration = [textDuration substringToIndex:1];
                    }
                    
                    APP_DELEGATE.tripDistance = [textDistance doubleValue];
                    APP_DELEGATE.tripDuration = [textDuration doubleValue];
                }
                
                if (APP_DELEGATE.arrayCarCategories.count > 0)
                {
                    [self.collectionViewAvailableCars reloadData];
                    
//                    NSIndexPath *indexpath = [NSIndexPath indexPathForItem:selectedCarIndex inSection:0];
//                    [self.collectionViewAvailableCars scrollToItemAtIndexPath:indexpath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
                }
                
            }
            callback(success);
        } else {
            SHOW_ALERT_WITH_CAUTION(error);
            callback(false);
        }
        STOP_HUD
    }];
}

#pragma mark - GoogleMapDelegate
-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    [self changeShadowDynamicallyWithLocationModeShouldClear:true];
    [self shouldAdjustSednRequestConatinerShouldShow:false canShowButton:true];
    
    GMSVisibleRegion region = self.viewGMapView.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    if (![bounds containsCoordinate:APP_DELEGATE.userLocation]) {
        if(isAnimationStarted == false){
            isAnimationStarted = true;
            [self buttonBlinkAnimationShouldStart];
        }
    } else {
        if (isAnimationStarted == true){
            isAnimationStarted = false;
        }
    }
}

-(void)buttonBlinkAnimationShouldStart{
    self.viewTrackMyLocationShadow.backgroundColor = [UIColor whiteColor];
    if (isAnimationStarted == true){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.viewTrackMyLocationShadow.backgroundColor = [UIColor cyanColor];
        });
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self buttonBlinkAnimationShouldStart];
        });
    } else {
        self.viewTrackMyLocationShadow.alpha = 1.0;
        self.viewTrackMyLocationShadow.backgroundColor = [UIColor whiteColor];
    }
}

-(void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    if (marker.position.latitude == APP_DELEGATE.tripStartLocation.latitude && marker.position.longitude == APP_DELEGATE.tripStartLocation.longitude){
        isModePickBeginLocation = true;
        APP_DELEGATE.dictTripStartAddress = @{};
    } else if (marker.position.latitude == APP_DELEGATE.tripEndLocation.latitude && marker.position.longitude == APP_DELEGATE.tripEndLocation.longitude){
        isModePickBeginLocation = false;
        APP_DELEGATE.dictTripEndAddress = @{};
    }
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
}

-(void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    if (isModePickBeginLocation){
        APP_DELEGATE.tripStartLocation = marker.position;
        isInitialMarkerChanged= true;
    } else {
        APP_DELEGATE.tripEndLocation = marker.position;
    }
    [self restoreGoogleMapWithMarkersAndRoutes];
}



#pragma mark - RunningRideBanner
-(void)initializeRunningRideBannerView{
    self.viewRideInfoContainer.layer.cornerRadius = self.viewRideInfoContainer.frame.size.height / 2;
    self.viewRideInfoContainer.layer.masksToBounds = true;
    
    self.imageViewRunningRideIcon.layer.cornerRadius = self.imageViewRunningRideIcon.frame.size.height / 2;
    self.imageViewRunningRideIcon.layer.masksToBounds = true;
}

-(void)shouldShowRunnigRideBanner:(BOOL)shouldShow{
    if (shouldShow){
        if (APP_DELEGATE.dictTripDriverDetails.count <= 0 || APP_DELEGATE.dictTripDriverDetails == nil || [APP_DELEGATE.dictTripDriverDetails isKindOfClass:[NSNull class]]){
            return;
        }
        
        NSString * driverImageURL = APP_DELEGATE.dictTripDriverDetails[@"driver_user_image"];
        
        if ([self.imageViewRunningRideIcon viewWithTag:1111]){
            [(UIActivityIndicatorView *)[self.imageViewRunningRideIcon viewWithTag:1111]stopAnimating];
            [(UIActivityIndicatorView *)[self.imageViewRunningRideIcon viewWithTag:1111] removeFromSuperview];
        }
        
        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.imageViewRunningRideIcon.frame.size.width, self.imageViewRunningRideIcon.frame.size.height)];
        activity_indicator.tag = 1111;
        [self.imageViewRunningRideIcon addSubview:activity_indicator];
        [activity_indicator startAnimating];
        [activity_indicator setColor:[UIColor blackColor]];
        [self.imageViewRunningRideIcon sd_setImageWithURL:[NSURL URLWithString:driverImageURL] placeholderImage:[UIImage imageNamed:@"PHProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activity_indicator stopAnimating];
            [activity_indicator removeFromSuperview];
        }];
        
        self.lableDriverName.text = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictTripDriverDetails[@"driver_first_name"],APP_DELEGATE.dictTripDriverDetails[@"driver_last_name"]];
        self.lableCarBrandNAme.text = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictDefaultBrand[@"car_brand_name"],APP_DELEGATE.dictTripDriverDetails[@"car_model"]];
        self.lableCarPlate.text = APP_DELEGATE.dictTripDriverDetails[@"car_plat_no"];
        
        if ([APP_DELEGATE.dictTripDriverDetails[@"driver_ratings"] isEqualToString:@"0"]){
            self.lableDriverRatings.text = @"";
        } else {
            double rating = [APP_DELEGATE.dictTripDriverDetails[@"driver_ratings"] doubleValue];
            self.lableDriverRatings.text = [NSString stringWithFormat:@"%0.1f ★", rating];
        }
        

        self.runningRideBannerBottomSpace.constant = 0;
    } else {
        self.runningRideBannerBottomSpace.constant = -75;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)goRunnigTripViewControllerTapAction:(id)sender {
    
    CurrentTripViewController *currentTripView=[self.storyboard instantiateViewControllerWithIdentifier:@"CurrentTripViewController"];
    currentTripView.delegate = self;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currentTripView];
    if (self.lableDestinationLocationDetail.text != nil && ![self.lableDestinationLocationDetail.text isEqualToString:@""]){
        currentTripView.destinationDetails = self.lableDestinationLocationDetail.text;
        [APP_DELEGATE.globalNavigation presentViewController:navigationController animated:true completion:nil];
    } else {
        [self findAddressForCoordinate:APP_DELEGATE.tripEndLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
            if (success){
                currentTripView.destinationDetails = dictAddress[@"fullAddress"];
                [APP_DELEGATE.globalNavigation presentViewController:navigationController animated:true completion:nil];
            }
        }];
    }
}
- (IBAction)contactButtonTapAction:(id)sender {
    //Will Open dialer with driver mobile
    NSString * mobileNumber = APP_DELEGATE.dictTripDriverDetails[@"driver_mobile_no"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",mobileNumber]]];
}

#pragma mark - PulstorViewControllerDelegate
-(void)didSelectRetryForRequest{
    APP_DELEGATE.isTripRunning = false;
    [self shouldShowRunnigRideBanner:false];
    [self restoreGoogleMapWithMarkersAndRoutes];
}

-(void)didReciveAcceptNotification{
    RunningRideViewController * runningRideView = [self.storyboard instantiateViewControllerWithIdentifier:@"RunningRideViewController"];
    [self.navigationController pushViewController:runningRideView animated:true];
}

-(void)didCancelRequestByUser{
    [APP_DELEGATE clearPrevousTripDataFromApp];
    [self resloadTheMapViewWithClear];
    [self restoreGoogleMapWithMarkersAndRoutes];
}

#pragma mark - NearByCarCorner
-(void)updateCarMarkersNotification{
    /*
     NSString * user_id = driver[@"user_id"];
     NSString * car_plate = driver[@"car_plat_no"];
     NSString * lattitude = driver[@"latitude"];
     NSString * longitude = driver[@"longitude"];
     */
    
    if (!arrNearByCarMarkers){
        //This is for Initial State
        arrNearByCarMarkers = [[NSMutableArray alloc]init];
        for (NSDictionary * dictLocation in APP_DELEGATE.arrayNearByCars){
            
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake([dictLocation[@"latitude"] doubleValue], [dictLocation[@"longitude"] doubleValue]);
            
            GMSMarker * markerToAdd = [[GMSMarker alloc]init];
            markerToAdd.position = location;
            markerToAdd.icon = [UIImage imageNamed:@"PHDriverCar"];
            markerToAdd.title = dictLocation[@"car_plat_no"];
            markerToAdd.map = self.viewGMapView;
            [arrNearByCarMarkers addObject:markerToAdd];
        }
    } else {
        //Find Removable Marker or Update Driver Marker
        NSMutableArray * arrayManagableMarker = [[NSMutableArray alloc]init];
        for (GMSMarker * userMarker in arrNearByCarMarkers){
            NSString * searchableId = userMarker.title;
            NSUInteger index2 = [APP_DELEGATE.arrayNearByCars indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
                BOOL found = [[item objectForKey:@"car_plat_no"] isEqualToString:searchableId];
                return found;
            }];
            
            if (index2 == NSNotFound || index2 > APP_DELEGATE.arrayNearByCars.count) {
                [arrayManagableMarker addObject:userMarker];
            } else {
                CLLocationCoordinate2D oldPossition = userMarker.position;
                double newLattitude = [APP_DELEGATE.arrayNearByCars[index2][@"latitude"] doubleValue];
                double newLongitude = [APP_DELEGATE.arrayNearByCars[index2][@"longitude"] doubleValue];
                CLLocationCoordinate2D newPossition = CLLocationCoordinate2DMake(newLattitude, newLongitude);
                userMarker.map = self.viewGMapView;
                [self moveMarker:userMarker WithAnimationFromCoordinate:oldPossition toCoordinate:newPossition];
                //Animate The Marker to New Location
            }
        }
        
        //Remove Markers which are not found
        for (GMSMarker * markerToRemove in arrayManagableMarker){
            markerToRemove.map = nil;
            [arrNearByCarMarkers removeObject:markerToRemove];
        }
        [arrayManagableMarker removeAllObjects];
        arrayManagableMarker = [[NSMutableArray alloc]init];
        
        //Add New Markers which are Founds
        for (NSDictionary * dictNearyByUser in APP_DELEGATE.arrayNearByCars){
            NSString * searchableId = dictNearyByUser[@"car_plat_no"];
            NSUInteger index2 = [arrNearByCarMarkers indexOfObjectPassingTest:^BOOL(GMSMarker *item, NSUInteger idx, BOOL *stop) {
                BOOL found = [item.title isEqualToString:searchableId];
                return found;
            }];
            
            if (index2 == NSNotFound || index2 > APP_DELEGATE.arrayNearByCars.count) {
                [arrayManagableMarker addObject:dictNearyByUser];
            }
        }
        
        for (NSDictionary * dictNearyByUser in arrayManagableMarker){
            double newLattitude = [dictNearyByUser[@"latitude"] doubleValue];
            double newLongitude = [dictNearyByUser[@"longitude"] doubleValue];
            
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake(newLattitude,newLongitude);
            
            GMSMarker * markerToAdd = [[GMSMarker alloc]init];
            markerToAdd.position = location;
            markerToAdd.icon = [UIImage imageNamed:@"PHDriverCar"];
            markerToAdd.title = dictNearyByUser[@"car_plat_no"];
            markerToAdd.map = self.viewGMapView;
            [arrNearByCarMarkers addObject:markerToAdd];
        }
        [arrayManagableMarker removeAllObjects];
    }
}



-(void)moveMarker:(GMSMarker *)marker WithAnimationFromCoordinate:(CLLocationCoordinate2D)oldLocation toCoordinate:(CLLocationCoordinate2D)newLocation{
    
    int markerIndex = (int)[arrNearByCarMarkers indexOfObject:marker];
    if (markerIndex < 0 || markerIndex >= arrNearByCarMarkers.count){
        markerIndex = 0;
    }
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:1.0];
    marker.rotation = [self angleFromCoordinate:oldLocation toCoordinate:newLocation] * (180.0 / M_PI);
    
    [CATransaction commit];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        marker.position = newLocation;
        [CATransaction commit];
        if (arrNearByCarMarkers.count > markerIndex){
            [arrNearByCarMarkers replaceObjectAtIndex:markerIndex withObject:marker];
        }
    });
    //NOTE : Will display wrong directions
}

- (float)angleFromCoordinate:(CLLocationCoordinate2D)first toCoordinate:(CLLocationCoordinate2D)second {
    
    float deltaLongitude = second.longitude - first.longitude;
    float deltaLatitude = second.latitude - first.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    
    if (deltaLongitude > 0)      return angle;
    else if (deltaLongitude < 0) return angle + M_PI;
    else if (deltaLatitude < 0)  return M_PI;
    
    return 0.0f;
}

#pragma mark - CurrentTripDeatilsViewControllerDelegate
-(void)didSelectContactToDriverForCurrentTrip{
    NSString * mobileNumber = APP_DELEGATE.dictTripDriverDetails[@"driver_mobile_no"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",mobileNumber]]];
}

-(void)didRejectCurrentTripWithController{
    //The Trip will reject
    [APP_DELEGATE clearPrevousTripDataFromApp];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_CLEAR_HOME_MAP object:nil];
}

-(void)didChangeLocationWithIsDropPin:(BOOL)canDropPin{
    //Change destination location
    GMSAutocompleteViewController *accController = [[GMSAutocompleteViewController alloc] init];
    isModePickBeginLocation = false;
    accController.delegate = self;
    [self presentViewController:accController animated:YES completion:nil];
}

#pragma mark - AlertViewControllerDelegate
-(void)shouldShowAlertForCanceledTrip{
    AlertViewController * alertView = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewController"];
    alertView.delegate = self;
    alertView.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    alertView.alertTitle = SET_ALERT_TEXT(@"request_is_canceled");
    alertView.alertMessage = SET_ALERT_TEXT(@"canceled_by_driver_message");
    alertView.okayTitle = SET_ALERT_TEXT(@"okay");
    alertView.cancelTitle = @"";
    alertView.canDismissTap = false;
    alertView.isModeWithTextView = false;
    [self presentViewController:alertView animated:true completion:nil];
}
-(void)alertControllerOkayButtonTappedWithTextViewText:(NSString *)textViewText{
    START_HUD
    [APP_DELEGATE.apiManager confirmRideStatusUpdateWithRideId:APP_DELEGATE.runningTripId DriverId:APP_DELEGATE.tripDriverId isRideCanaceled:true WithCallBack:^(BOOL success, NSString *serverMessage) {
        if (success){
            [APP_DELEGATE clearPrevousTripDataFromApp];
            [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_CLEAR_HOME_MAP object:nil];
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessage);
            [self shouldShowAlertForCanceledTrip];
        }
        STOP_HUD
    }];
}

-(void)alertControllerCancelButtonTapped{
    //Cancel button
}

#pragma mark - ************************************************
#pragma mark - *********UPDATE USER CURRENT LOCATION***********
#pragma mark - ************************************************

#pragma mark - Continously Updating Driver Current Location
-(void)currentLocation
{
    
    timerStatus=0;
    counter=0;
    tempCount=0;
    
    APP_DELEGATE.timer1 =nil;
    [APP_DELEGATE.timer1 invalidate];
    
    UIBackgroundTaskIdentifier bgTask = 0;
    UIApplication  *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    [self updateCustomerCurrentLocation];
    
    APP_DELEGATE.timer1 =
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                   selector:@selector(callWebserviceInBackGround) userInfo:nil
                                    repeats:YES];
    [[NSRunLoop currentRunLoop]addTimer:APP_DELEGATE.timer1 forMode:UITrackingRunLoopMode];
    
}

-(void)callWebserviceInBackGround
{
    
    if (counter==10)
    {
        counter=0;
        [self updateCustomerCurrentLocation];
    }
    if (DEBUG_MODE){
        NSLog(@"Temp Count --->> %d",tempCount);
    }
    
    counter++;
    
    tempCount++;
    if (tempCount==100)
    {
        if (timerStatus==0)
        {
            [APP_DELEGATE.timer1 invalidate ];
            APP_DELEGATE.timer1=nil;
            timerStatus=1;
            counter=0;
            tempCount=0;
            
            UIBackgroundTaskIdentifier bgTask = 0;
            UIApplication  *app = [UIApplication sharedApplication];
            bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                [app endBackgroundTask:bgTask];
            }];
            
            
            APP_DELEGATE.timer2 =
            [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self selector:@selector(callWebserviceInBackGround)
                                           userInfo:nil repeats:YES];
            
            [[NSRunLoop currentRunLoop]addTimer:APP_DELEGATE.timer2 forMode:UITrackingRunLoopMode];
        }
        else
        {
            [APP_DELEGATE.timer2 invalidate ];
            APP_DELEGATE.timer2=nil;
            timerStatus=0;
            counter=0;
            tempCount=0;
            
            UIBackgroundTaskIdentifier bgTask = 0;
            UIApplication  *app = [UIApplication sharedApplication];
            bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                [app endBackgroundTask:bgTask];
            }];
            
            APP_DELEGATE.timer1 =
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                           selector:@selector(callWebserviceInBackGround) userInfo:nil
                                            repeats:YES];
            
            [[NSRunLoop currentRunLoop]addTimer:APP_DELEGATE.timer1 forMode:UITrackingRunLoopMode];
        }
    }
    
}



#pragma mark - UpdateUserLocationConstant
-(void)updateCustomerCurrentLocation
{
    
     [APP_DELEGATE getCurrentLocation];
    
    NSLog(@"%@",APP_DELEGATE.strLatitude);
    if (APP_DELEGATE.userLocation.latitude == 00.00000 || APP_DELEGATE.userLocation.longitude == 00.00000)
    {
        [APP_DELEGATE getCurrentLocation];
        STOP_HUD
    }
    else
    {
        NSString * strUserLattitude = [NSString stringWithFormat:@"%f", APP_DELEGATE.userLocation.latitude];
        NSString * strUserLongitude = [NSString stringWithFormat:@"%f", APP_DELEGATE.userLocation.longitude];
        
        NSString * carBarndId = (APP_DELEGATE.dictDefaultBrand[@"car_brand_id"] != nil && ![APP_DELEGATE.dictDefaultBrand[@"car_brand_id"]isKindOfClass:[NSNull class]]) ? APP_DELEGATE.dictDefaultBrand[@"car_brand_id"] : @"1";
        
       
        
        [APP_DELEGATE.apiManager updateCustomerCurrentLocation:APP_DELEGATE.loggedUserId latitude:strUserLattitude longitude:strUserLongitude brandCar:carBarndId withCallBack:^(BOOL success, NSArray *response, NSString *errorMessange) {
            STOP_HUD
            if (success){
                if (![response isKindOfClass:[NSNull class]] && response.count > 0)
                {
                    NSDictionary * dictResponse = response.firstObject;
                    
                    if ([dictResponse[@"code"] integerValue] == 1 || dictResponse[@"result"]){
                        NSMutableArray *arrDetails = [response mutableCopy];
                        if (DEBUG_MODE){
                            NSLog(@"Current API Response : %@",arrDetails);
                        }
                        
                        NSDictionary * dictResponse = arrDetails.firstObject;
                        if (dictResponse[@"currency_symbol"] && ![dictResponse[@"currency_symbol"]isEqualToString:@""]){
                            APP_DELEGATE.currencySymbol = [dictResponse objectForKey:@"currency_symbol"];
                            APP_DELEGATE.decimapPoints = [dictResponse objectForKey:@"currency_decimal"];
                        }
                        
                        [self manageViewFromStatus:arrDetails];
                    } else {
                        [APP_DELEGATE.arrayNearByCars removeAllObjects];
                        for (GMSMarker * marker in arrNearByCarMarkers){
                            marker.map = nil;
                        }
                        [arrNearByCarMarkers removeAllObjects];
                        
                        if ([dictResponse[@"code"] integerValue] == -1){
                            [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                            SHOW_ALERT_WITH_CAUTION(SET_ALERT_TEXT(@"login_expired_message"))
                        } else if ([dictResponse[@"code"] integerValue] == -3) {
                            [APP_DELEGATE logoutTheUserFromAppWithAlert:false];
                            SHOW_ALERT_WITH_CAUTION(SET_ALERT_TEXT(@"your_acount_inactive"))
                        }else {
                            if (DEBUG_MODE){
                                NSLog(@"Update Current Location : %@",dictResponse);
                            }
                        }
                    }
                }
            } else {
                if (DEBUG_MODE){
                    NSLog(@"Failed to update current locatio : %@",errorMessange);
                }
            }
        }];
    }
}

-(void)manageViewFromStatus:(NSArray *)arrDetails
{
    if ([[arrDetails objectAtIndex:0] valueForKey:@"status"] == nil || [[[arrDetails objectAtIndex:0] valueForKey:@"status"] isKindOfClass:[NSNull class]]){
        if (DEBUG_MODE){
            NSLog(@"Something getting wrong : %@",[[arrDetails objectAtIndex:0] valueForKey:@"status"]);
        }
        return;
    }
    
    //This condition will manages the views
    if ([[[arrDetails objectAtIndex:0] valueForKey:@"status"] integerValue] > 0)
    {
        APP_DELEGATE.isTripRunning = TRUE;
        
        NSDictionary * dictTripDetailsData = [[[arrDetails objectAtIndex:0] valueForKey:@"result_trip"]firstObject];
        
        APP_DELEGATE.runningTripId = [dictTripDetailsData objectForKey:@"ride_id"];
        APP_DELEGATE.tripDriverId = [dictTripDetailsData objectForKey:@"driver_id"];
        
        APP_DELEGATE.dictTripDriverDetails = [[NSDictionary alloc]initWithDictionary:dictTripDetailsData];
        
        self.viewLocationDetailsContainer.hidden = APP_DELEGATE.isTripRunning;
        [self shouldShowRunnigRideBanner:APP_DELEGATE.isTripRunning];
        [APP_DELEGATE updateDriverMarkerWithAnimation];
        
        if (APP_DELEGATE.markerDriverLocation != nil){
            APP_DELEGATE.markerDriverLocation.map = self.viewGMapView;
        }
        
        double startLocLattitude = [[dictTripDetailsData objectForKey:@"pickup_lat"]doubleValue];
        double startLocLongitude = [[dictTripDetailsData objectForKey:@"pickup_log"]doubleValue];
        
        double endLocLattitude = [[dictTripDetailsData objectForKey:@"drop_lat"]doubleValue];
        double endLocLongitude = [[dictTripDetailsData objectForKey:@"drop_log"]doubleValue];
        
        if ((APP_DELEGATE.tripStartLocation.latitude != startLocLattitude && APP_DELEGATE.tripStartLocation.longitude != startLocLongitude) || (APP_DELEGATE.tripEndLocation.latitude != endLocLattitude && APP_DELEGATE.tripEndLocation.longitude != endLocLongitude)){
            APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(startLocLattitude, startLocLongitude);
            APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(endLocLattitude, endLocLongitude);
            [self restoreGoogleMapWithMarkersAndRoutes];
        }
    } else {
        if (APP_DELEGATE.isTripRunning == true && APP_DELEGATE.tripStatus == RIDE_STATUS_SEND_REQUEST){
            UIViewController *controller = APP_DELEGATE.globalNavigation.viewControllers.lastObject;
            if (![controller isKindOfClass:[PulstorViewController class]]) {
                return;
                PulstorViewController * pulstorView = [self.storyboard instantiateViewControllerWithIdentifier:@"PulstorViewController"];
                pulstorView.delegate = self;
                pulstorView.modalPresentationStyle = UIModalPresentationOverFullScreen;
                [APP_DELEGATE.globalNavigation presentViewController:pulstorView animated:true completion:nil];
            }
            
            return;
            
        } else if (APP_DELEGATE.isTripRunning == true && APP_DELEGATE.tripStatus != RIDE_STATUS_SEND_REQUEST) {
            [APP_DELEGATE clearPrevousTripDataFromApp];
            
            [self resloadTheMapViewWithClear];
            [self restoreGoogleMapWithMarkersAndRoutes];
        }
    }
    
    if ([[[arrDetails objectAtIndex:0] valueForKey:@"status"] integerValue] ==0)
    {
        UIViewController *controller = APP_DELEGATE.globalNavigation.viewControllers.lastObject;
        if ([[APP_DELEGATE.arrStatusDetails objectAtIndex:0]integerValue ]== 1)
        {
            if (![controller isKindOfClass:[HomeViewController class]])
            {
                HomeViewController * homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                [self.navigationController pushViewController:homeView animated:true];
                
                [APP_DELEGATE clearPrevousTripDataFromApp];
                [self resloadTheMapViewWithClear];
                [self restoreGoogleMapWithMarkersAndRoutes];
            }
            
            [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:0 withObject:@"0"];
        }
        
        
        APP_DELEGATE.tripStatus = RIDE_STATUS_SEND_REQUEST;
        APP_DELEGATE.isTripRunning = FALSE;
        
        if ([[[arrDetails objectAtIndex:0] valueForKey:@"code"] integerValue] !=0)
        {
            NSArray * nearByCars = [[arrDetails objectAtIndex:0] valueForKey:@"result"];
            if (nearByCars && nearByCars.count > 0){
                APP_DELEGATE.arrayNearByCars = [[NSMutableArray alloc]initWithArray:nearByCars];
                [self updateCarMarkersNotification];
            } else {
                [APP_DELEGATE.arrayNearByCars removeAllObjects];
                for (GMSMarker * marker in arrNearByCarMarkers){
                    marker.map = nil;
                }
                [arrNearByCarMarkers removeAllObjects];
            }
        } else {
            [self updateNarbyCarArray:@[]];
            [self updateCarMarkersNotification];
        }
        
        self.viewLocationDetailsContainer.hidden = APP_DELEGATE.isTripRunning;
        [self shouldShowRunnigRideBanner:false];
    }
    else if ([[[arrDetails objectAtIndex:0] valueForKey:@"status"] integerValue] == 1)
    {
        if ([[APP_DELEGATE.arrStatusDetails objectAtIndex:1] integerValue] == 0){
            [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:0 withObject:@"1"];
            [APP_DELEGATE.arrayNearByCars removeAllObjects];
            [self updateNarbyCarArray:@[]];
            [self updateCarMarkersNotification];
        }
        
        [self shouldShowRunnigRideBanner:TRUE];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
        
        APP_DELEGATE.tripStatus = RIDE_STATUS_ACCEPTED;
        APP_DELEGATE.dictTripDriverDetails = [[NSDictionary alloc] initWithDictionary:[[[[arrDetails objectAtIndex:0] valueForKey:@"result_trip"] objectAtIndex:0] mutableCopy]];
    }
    else if ([[[arrDetails objectAtIndex:0] valueForKey:@"status"] integerValue] == 2)
    {
        
        [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:0 withObject:@"1"];
        
        [self shouldShowRunnigRideBanner:TRUE];
        
        APP_DELEGATE.tripStatus = RIDE_STATUS_ARRIVAL;
        
        
        if ([[APP_DELEGATE.arrStatusDetails objectAtIndex:2] integerValue] == 0)
        {
            [self dismissViewControllerAnimated:true completion:nil];
            return;
            
            PulstorViewController * pulstorView = [self.storyboard instantiateViewControllerWithIdentifier:@"PulstorViewController"];
            pulstorView.delegate = self;
            pulstorView.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [APP_DELEGATE.globalNavigation presentViewController:pulstorView animated:true completion:nil];
            
            [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:2 withObject:@"1"];
            
        }
        
        
    }
    else if ([[[arrDetails objectAtIndex:0] valueForKey:@"status"] integerValue] == 3)
    {
        
        [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:0 withObject:@"1"];
        
        [self shouldShowRunnigRideBanner:TRUE];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
        
        APP_DELEGATE.tripStatus = RIDE_STATUS_ON_GOING;
        
        
        if ([[APP_DELEGATE.arrStatusDetails objectAtIndex:3] integerValue] == 0)
        {
            [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:3 withObject:@"1"];
            
        }
    }
    else if ([[[arrDetails objectAtIndex:0] valueForKey:@"status"] integerValue] == 4)
    {
        [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:0 withObject:@"0"];
        [self shouldShowRunnigRideBanner:false];
        APP_DELEGATE.tripStatus = RIDE_STATUS_CANCELED;
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
        
        if ([[APP_DELEGATE.arrStatusDetails objectAtIndex:4] integerValue] == 0)
        {
            UIViewController *controller = APP_DELEGATE.globalNavigation.viewControllers.lastObject;
            if (![controller isKindOfClass:[HomeViewController class]]){
                if ([controller isKindOfClass:[CurrentTripViewController class]])
                {
                    [self dismissViewControllerAnimated:true completion:nil];
                } else {
                    [APP_DELEGATE.globalNavigation popViewControllerAnimated:true];
                }
            } else {
                [self dismissViewControllerAnimated:true completion:nil];
            }
            
            [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:4 withObject:@"1"];
            [self shouldShowAlertForCanceledTrip];
        }
    } else if ([[[arrDetails objectAtIndex:0] valueForKey:@"status"] integerValue] == 5)
    {
        [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:0 withObject:@"0"];
        [self shouldShowRunnigRideBanner:false];
        APP_DELEGATE.tripStatus = RIDE_STATUS_FINISHED;
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
        if ([[APP_DELEGATE.arrStatusDetails objectAtIndex:5] integerValue] == 0)
        {
            UIViewController *controller = APP_DELEGATE.globalNavigation.viewControllers.lastObject;
            if (![controller isKindOfClass:[HomeViewController class]]){
                if ([controller isKindOfClass:[CurrentTripViewController class]])
                {
                    [self dismissViewControllerAnimated:true completion:nil];
                } else {
                    [APP_DELEGATE.globalNavigation popViewControllerAnimated:true];
                }
            }
            
            [APP_DELEGATE.arrStatusDetails replaceObjectAtIndex:5 withObject:@"1"];
            TripDetailViewController * tripDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"TripDetailViewController"];
            tripDetails.isModeSubmit = true;
            tripDetails.trip_id = APP_DELEGATE.runningTripId;
            [self presentViewController:tripDetails animated:true completion:^{ }];
        }
    }
    
    
    STOP_HUD
    
}

#pragma mark - RunningRideViewControllerDelegate
-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo{
    if (DEBUG_MODE){
        NSLog(@"Selected Location : %@",locationInfo);
    }
    double lattitude = [locationInfo[@"lattitude"]doubleValue];
    double longitude = [locationInfo[@"longitude"]doubleValue];
    
    if (isModePickBeginLocation){
        APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(lattitude, longitude);
        APP_DELEGATE.dictTripStartAddress = locationInfo;
    } else {
        APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(lattitude, longitude);
        APP_DELEGATE.dictTripEndAddress = locationInfo;
    }
    isInitialMarkerChanged= true;
    [self restoreGoogleMapWithMarkersAndRoutes];
}

@end
