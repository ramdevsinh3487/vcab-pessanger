//
//  SelectCarCell.h
//  VCab
//
//  Created by Vishal Gohil on 29/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCarCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCar;
@property (weak, nonatomic) IBOutlet UILabel *lableCarName;
@property (weak, nonatomic) IBOutlet UILabel *lableCarDetails;
@property (weak, nonatomic) IBOutlet UIImageView *imageSelectCar;

@end
