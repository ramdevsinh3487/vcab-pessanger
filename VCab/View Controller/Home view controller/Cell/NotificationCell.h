//
//  NotificationCell.h
//  Wenah
//
//  Created by Himanshu on 11/4/16.
//  Copyright © 2016 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNotiHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblNotificationTime;

@end
