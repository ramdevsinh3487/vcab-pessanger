//
//  LanguageSelectionView.m
//  VCab
//
//  Created by Vishal Dhamecha on 11/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "LanguageSelectionView.h"
#import "HomeViewController.h"


@interface LanguageSelectionView ()

@end

@implementation LanguageSelectionView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self callWebserviceForGetLabel];
    
  
}
#pragma mark - call webservice -

-(void) callWebserviceForGetLabel
{
    NSString *strLabelTime = @"";
    if (GET_DEFAULT_VALUE(@"labelUpdatedTime"))
    {
        strLabelTime = GET_DEFAULT_VALUE(@"labelUpdatedTime");
    }
    
  
    
    NSString *str = [NSString stringWithFormat:@"http://www.adsasat.co/vcab/api/lang_label.php?updated_datetime=%@",strLabelTime];
    str = [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSLog(@"%@",str);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:str]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                
                if (!error) {
                    NSMutableArray *json = (NSMutableArray *) [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingMutableContainers error:nil];
                    
                    NSLog(@"%@",json);
                    
                    NSString *strcode=[NSString stringWithFormat:@"%@",[[json valueForKey:@"code"]objectAtIndex:0]];
                    
                      APP_DELEGATE.arrLanguage = [[NSMutableArray alloc] init];
                    
                    if ([strcode isEqualToString:@"1"])
                    {
                        SET_DEFAULT_VALUE([[json objectAtIndex:0] valueForKey:@"updated_datetime"], @"labelUpdatedTime");
                    
                        
                        if ([[[[json objectAtIndex:0] valueForKey:@"english"] objectAtIndex:0] count] > 0) {
                            NSArray *arr = [[[[json objectAtIndex:0] valueForKey:@"english"] objectAtIndex:0] allKeys];
                            
                            for (int i = 0; i < arr.count; i++)
                            {
                                
                                NSMutableArray *arrSelect = [[NSMutableArray alloc] init];
                                arrSelect = [APP_DELEGATE.db SelectAllFromTable:[NSString stringWithFormat:@"SELECT * from tblLanguageFields WHERE field_name='%@'",[arr objectAtIndex:i]]];
                                
                                if (arrSelect.count > 0) {
                                    
                                    NSString *strUpdate = [NSString stringWithFormat:@"UPDATE tblLanguageFields SET field_EN='%@' WHERE field_name='%@'",[[[[json objectAtIndex:0] valueForKey:@"english"] objectAtIndex:0] valueForKey:[arr objectAtIndex:i]],[arr objectAtIndex:i]];
                                    [APP_DELEGATE.db Update:strUpdate];
                                }
                                else
                                {
                                    NSString *strinsert = [NSString stringWithFormat:@"INSERT INTO tblLanguageFields (field_name, field_EN) VALUES ('%@','%@')",[arr objectAtIndex:i],[[[[json objectAtIndex:0] valueForKey:@"english"] objectAtIndex:0] valueForKey:[arr objectAtIndex:i]]];
                                    [APP_DELEGATE.db Insert:strinsert];
                                }
                                [[NSNotificationCenter defaultCenter]postNotificationName:@"reloadMenuTable" object:nil];
                            }
                            
                        }
                        
                        if ([[[[json objectAtIndex:0] valueForKey:@"german"] objectAtIndex:0] count] > 0)
                        {
                            NSArray *arr = [[[[json objectAtIndex:0] valueForKey:@"german"] objectAtIndex:0] allKeys];
                            
                            for (int i = 0; i < arr.count; i++)
                            {
                                
                                NSMutableArray *arrSelect = [[NSMutableArray alloc] init];
                                arrSelect = [APP_DELEGATE.db SelectAllFromTable:[NSString stringWithFormat:@"SELECT * from tblLanguageFields WHERE field_name='%@'",[arr objectAtIndex:i]]];
                                
                                if (arrSelect.count > 0) {
                                    
                                    NSString *strUpdate = [NSString stringWithFormat:@"UPDATE tblLanguageFields SET field_AR='%@' WHERE field_name='%@'",[[[[json objectAtIndex:0] valueForKey:@"german"] objectAtIndex:0] valueForKey:[arr objectAtIndex:i]],[arr objectAtIndex:i]];
                                    [APP_DELEGATE.db Update:strUpdate];
                                }
                                else
                                {
                                    NSString *strinsert = [NSString stringWithFormat:@"INSERT INTO tblLanguageFields (field_name, field_AR) VALUES ('%@','%@')",[arr objectAtIndex:i],[[[[json objectAtIndex:0] valueForKey:@"German"] objectAtIndex:0] valueForKey:[arr objectAtIndex:i]]];
                                    [APP_DELEGATE.db Insert:strinsert];
                                }
                                [[NSNotificationCenter defaultCenter]postNotificationName:@"reloadMenuTable" object:nil];
                            }
                        }
                        
                       
                        
                    }
                    
                    
                     APP_DELEGATE.arrLanguage = [[APP_DELEGATE.db SelectAllFromTable:@"SELECT * from tblLanguageFields"] mutableCopy];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (APP_DELEGATE.isUserLoggedIn){
                            HomeViewController * homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                            APP_DELEGATE.globalNavigation = [[UINavigationController alloc] initWithRootViewController:homeView];
                        } else {
                            LoginViewController * loginController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                            APP_DELEGATE.globalNavigation = [[UINavigationController alloc] initWithRootViewController:loginController];
                        }
                        
//                        APP_DELEGATE.window.rootViewController = APP_DELEGATE.globalNavigation;
                        
                        APP_DELEGATE.globalNavigation.navigationBarHidden = YES;
                        MainViewController *mainViewController = nil;
                        mainViewController = [[MainViewController alloc] initWithRootViewController:APP_DELEGATE.globalNavigation presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
                        [mainViewController setLeftViewSwipeGestureEnabled:false];
                        [mainViewController setRightViewSwipeGestureEnabled:false];
                        
                        APP_DELEGATE.window.rootViewController = mainViewController;
                        [APP_DELEGATE.window makeKeyAndVisible];
                        
                        [[IQKeyboardManager sharedManager]setToolbarDoneBarButtonItemText:SET_ALERT_TEXT(@"done_capital")];
                    });
                    
                    if (APP_DELEGATE.window.userInteractionEnabled == NO) {
                        
                        [APP_DELEGATE.window setUserInteractionEnabled:YES];
                        START_HUD
                    }
                    
                } else {
                    // update the UI to indicate error
                }
                // handle response
                
            }] resume];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
