//
//  CurrentTripViewController.h
//  VCab
//
//  Created by Krishna on 26/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentViewController.h"
#import "AlertViewController.h"
#import "RunningRideViewController.h"

@protocol CurrentTripViewControllerDelegate <NSObject>
@optional
-(void)didRejectCurrentTripWithController;

@optional
-(void)didSelectContactToDriverForCurrentTrip;

@optional
-(void)didChangeLocationWithIsDropPin:(BOOL)canDropPin;

@end

@interface CurrentTripViewController : UIViewController<UIGestureRecognizerDelegate, PaymentViewControllerDelegate, AlertViewControllerDelegate, RunningRideViewControllerDelegate>{

}

@property NSString * destinationDetails;
@property id <CurrentTripViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UILabel *lablePickupTime;

@property (weak, nonatomic) IBOutlet UIView *viewDriverProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDriverImage;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lableCarName;
@property (weak, nonatomic) IBOutlet UILabel *lableCarModel;
@property (weak, nonatomic) IBOutlet UILabel *lableDriverRatings;
@property (weak, nonatomic) IBOutlet UILabel *lableCarPlate;
@property (weak, nonatomic) IBOutlet UIButton *buttonContact;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancelTrip;

@property (weak, nonatomic) IBOutlet UIView *viewTripDetails;
@property (weak, nonatomic) IBOutlet UILabel *lableTripDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableTripName;
@property (weak, nonatomic) IBOutlet UIButton *buttonChangeTrip;
@property (weak, nonatomic) IBOutlet UILabel *lableTripEstimatedTime;
@property (weak, nonatomic) IBOutlet UILabel *lableTripDistance;

@property (weak, nonatomic) IBOutlet UIView *viewCostDetails;
@property (weak, nonatomic) IBOutlet UILabel *lableCostTitle;

@property (weak, nonatomic) IBOutlet UILabel *lableTotalCost;
@property (weak, nonatomic) IBOutlet UILabel *lablePaymentType;
@property (weak, nonatomic) IBOutlet UIButton *buttonChangePaymentType;

@property (weak, nonatomic) IBOutlet UIView *UIViewPromocodeDetails;
@property (weak, nonatomic) IBOutlet UILabel *lablePromocodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lablePromocodeDetails;


@end
