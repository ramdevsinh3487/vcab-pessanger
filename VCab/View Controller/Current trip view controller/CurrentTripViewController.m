//
//  CurrentTripViewController.m
//  VCab
//
//  Created by Krishna on 26/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "CurrentTripViewController.h"

@interface CurrentTripViewController ()

@end

@implementation CurrentTripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    self.lablePickupTime.hidden = YES;
    
    
    self.lableTitle.text = SET_ALERT_TEXT(@"current_trip");
    self.lableTripDetailsTitle.text = SET_ALERT_TEXT(@"your_current_trip");
    [self.buttonChangeTrip setTitle:SET_ALERT_TEXT(@"change") forState:UIControlStateNormal];
    self.lableCostTitle.text = SET_ALERT_TEXT(@"you_may_cost");
    self.lablePromocodeTitle.text = SET_ALERT_TEXT(@"promocode");
    
    [self.buttonContact setTitle:SET_ALERT_TEXT(@"contact_capital") forState:UIControlStateNormal];
    [self.buttonCancelTrip setTitle:SET_ALERT_TEXT(@"cancel_capital") forState:UIControlStateNormal];
    [self.buttonChangeTrip setTitle:SET_ALERT_TEXT(@"change") forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated{
    if (APP_DELEGATE.tripStatus == RIDE_STATUS_ON_GOING){
        self.buttonChangeTrip.hidden = true;
    } else {
        self.buttonChangeTrip.hidden = false;
    }
    
    self.lableTripName.text = self.destinationDetails;
    [self loadDataForTheTrip];
    [self loadTripEstimatedTimeAndDistance];
    [self notificationObservers];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.imageViewDriverImage.layer.cornerRadius = self.imageViewDriverImage.frame.size.height / 2;
    self.imageViewDriverImage.layer.masksToBounds = true;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)notificationObservers{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(rideStatusUpdatedFromCurrentTripView:) name:NOTI_RUNNUNG_RIDE_STATUS_CHANGED object:nil];
}

-(void)loadTripEstimatedTimeAndDistance{
    self.lableTripDistance.text = @"";
    self.lableTripEstimatedTime.text = @"";
    if (APP_DELEGATE.userLocation.latitude != 00.00000 && APP_DELEGATE.userLocation.longitude != 00.00000){
        [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:APP_DELEGATE.userLocation destination:APP_DELEGATE.tripEndLocation withCallBack:^(BOOL success, NSArray *result, NSString * error) {
            if (success){
                for (NSDictionary *routeDict in result){
                    int totalSeconds = [[routeDict objectForKey:@"legs"][0][@"duration"][@"value"] intValue];
                    int secs = totalSeconds % 60;
                    int mins = (totalSeconds / 60) % 60;
                    int hrs = ((totalSeconds / 60) / 60) % 60;
                    
                    if (hrs <= 0){
                        //Will Show Minut, secs
                        self.lableTripEstimatedTime.text = [NSString stringWithFormat:@"%02d : %2d %@", mins, secs, SET_ALERT_TEXT(@"minutes")];
                    } else {
                        //Will Show Hours
                        self.lableTripEstimatedTime.text = [NSString stringWithFormat:@"%02d : %2d HOURS", hrs, mins];
                    }
                    
                    self.lableTripDistance.text = [NSString stringWithFormat:@"%.01f KM",[[routeDict objectForKey:@"legs"][0][@"distance"][@"value"]floatValue] / 1000];
                }
            }
            STOP_HUD
        }];
    } else {
        self.lableTripEstimatedTime.text = [self formatedTimeFromTotalSeconds:APP_DELEGATE.tripDuration * 60];
        self.lableTripDistance.text = [NSString stringWithFormat:@"%0.1f KM",APP_DELEGATE.tripDistance];
    }
}

-(void)loadDataForTheTrip{
    
    if (APP_DELEGATE.dictTripDriverDetails){
        if (DEBUG_MODE){
            NSLog(@"Driver Details : %@",APP_DELEGATE.dictTripDriverDetails);
        }
        
        
        NSLog(@"%f",APP_DELEGATE.tripDuration);
        
        self.lableCarName.text = APP_DELEGATE.dictDefaultBrand[@"car_brand_name"];
        self.lableCarModel.text = APP_DELEGATE.dictTripDriverDetails[@"car_model"];
        self.lableCarPlate.text = [NSString stringWithFormat:@" %@ ",APP_DELEGATE.dictTripDriverDetails[@"car_plat_no"]];
        
        self.lableDriverName.text = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictTripDriverDetails[@"driver_first_name"],APP_DELEGATE.dictTripDriverDetails[@"driver_last_name"]];
        
        int totalSeconds = APP_DELEGATE.tripDuration;
        int mins = (totalSeconds / 60) % 60;
        int hrs = ((totalSeconds / 60) / 60) % 60;
        
        self.lableTripEstimatedTime.text = [NSString stringWithFormat:@"%02d : %2d Mins", hrs, mins];
        self.lableTripDistance.text = [NSString stringWithFormat:@"%0.1f KM",APP_DELEGATE.tripDistance];
        
        float driverRating = [APP_DELEGATE.dictTripDriverDetails[@"driver_ratings"] floatValue];
        if (driverRating > 0){
            self.lableDriverRatings.text = [NSString stringWithFormat:@"%@ ★",APP_DELEGATE.dictTripDriverDetails[@"driver_ratings"]];
        } else {
            self.lableDriverRatings.text = @"";
        }
    
        NSString * driverImageURL = APP_DELEGATE.dictTripDriverDetails[@"driver_user_image"];
        
        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.imageViewDriverImage.frame.size.width, self.imageViewDriverImage.frame.size.height)];
        [self.imageViewDriverImage addSubview:activity_indicator];
        [activity_indicator startAnimating];
        [activity_indicator setColor:[UIColor blackColor]];
        [self.imageViewDriverImage sd_setImageWithURL:[NSURL URLWithString:driverImageURL] placeholderImage:[UIImage imageNamed:@"PHProfile"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activity_indicator stopAnimating];
            [activity_indicator removeFromSuperview];
        }];
        
        if ([APP_DELEGATE.dictTripDriverDetails[@"payment_type"] integerValue] == 1)
        {
          self.lablePaymentType.text = SET_ALERT_TEXT(@"cash");
        }
        else
        {
           self.lablePaymentType.text = SET_ALERT_TEXT(@"card");
        }
        
        
        
        
        NSString * basefare = APP_DELEGATE.dictDefaultBrand[@"base_fare"];
        NSString * minimumCharges = APP_DELEGATE.dictDefaultBrand[@"min_charge"];
        NSString * minuteCharges = APP_DELEGATE.dictDefaultBrand[@"minute_price"];
        NSString * kmCharges = APP_DELEGATE.dictDefaultBrand[@"km_price"];
        
        self.lableTotalCost.text = [APP_DELEGATE countEstimatedCostWithKM:APP_DELEGATE.tripDistance time:APP_DELEGATE.tripDuration minCharges:minimumCharges basefare:basefare minutePrice:minuteCharges kmPrice:kmCharges];
        
        self.lablePaymentType.text = [APP_DELEGATE.dictDefaultPayment[@"type"]uppercaseString];
        
        NSString * promocodePercentages = APP_DELEGATE.dictTripDriverDetails[@"percentage"];
        if (promocodePercentages && ![promocodePercentages isEqualToString:@""] && [promocodePercentages integerValue] > 0){
            self.lablePromocodeDetails.text = [NSString stringWithFormat:@"%@ %% %@",promocodePercentages,SET_ALERT_TEXT(@"cashback")];
            self.UIViewPromocodeDetails.hidden = false;
        } else {
            self.UIViewPromocodeDetails.hidden = true;
        }
    }
}

- (IBAction)contactButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        [self.delegate didSelectContactToDriverForCurrentTrip];
    }];
}


- (IBAction)cancelTripButtonAction:(id)sender {
    //Will Show cancel trip view
    [self shouldShowAlertForCancelTrip];
}

- (IBAction)changeButtonTapped:(id)sender {
    RunningRideViewController * runningRideView = [self.storyboard instantiateViewControllerWithIdentifier:@"RunningRideViewController"];
    runningRideView.searchLocationMode = SLM_CHANGE_DESTINATION;
    runningRideView.isModeSearchLocation = true;
    runningRideView.delegate = self;
    [self presentViewController:runningRideView animated:true completion:nil];
}

- (IBAction)changePaymentTapped:(id)sender {
    //Will Open payment method selector
    PaymentViewController * paymentView = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
    paymentView.isModeSelection = true;
    paymentView.delegate = self;
    [self.navigationController pushViewController:paymentView animated:true];
}

- (IBAction)closeButtonTapAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}


-(NSString *)formatedTimeFromTotalSeconds:(int)seconds{
    int totalMinutes = (seconds / 60) % 60;
    int totalHours = ((seconds / 60) / 60) % 60;
    
    return [NSString stringWithFormat:@"%02d : %02d %@", totalHours, totalMinutes, SET_ALERT_TEXT(@"minutes")];
}

#pragma mark - PaymentViewControllerDelegate
-(void)selectedPaymentMethodWithData:(NSDictionary *)dictPaymentMethod{
      self.lablePaymentType.text = dictPaymentMethod[@"title"];
}

-(void)rideStatusUpdatedFromCurrentTripView:(NSNotification *)notif {
    if (APP_DELEGATE.tripStatus == RIDE_STATUS_CANCELED || APP_DELEGATE.tripStatus == RIDE_STATUS_FINISHED || APP_DELEGATE.tripStatus == RIDE_STATUS_ARRIVAL){
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

#pragma mark - AlertViewControllerDelegate
-(void)shouldShowAlertForCancelTrip{
    AlertViewController * alertView = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewController"];
    alertView.delegate = self;
    alertView.modalPresentationStyle = UIModalPresentationOverFullScreen;
    alertView.alertTitle = SET_ALERT_TEXT(@"cancel_trip_questionmark");
    alertView.alertMessage = [NSString stringWithFormat:@"%@ \n\n %@",SET_ALERT_TEXT(@"cancel_trip_message"),SET_ALERT_TEXT(@"please_provide_reason")];
    alertView.okayTitle = SET_ALERT_TEXT(@"yes_capital");
    alertView.cancelTitle = SET_ALERT_TEXT(@"no_capital");
    alertView.canDismissTap = true;
    alertView.isModeWithTextView = true;

    [self presentViewController:alertView animated:true completion:nil];
}
-(void)alertControllerOkayButtonTappedWithTextViewText:(NSString *)textViewText{
    //Should cancel the trip
    NSString * cancelReason = textViewText; //Later will passes with API
    START_HUD
    [APP_DELEGATE.apiManager cancelTheRideRequestWithRideID:APP_DELEGATE.runningTripId driverId:APP_DELEGATE.tripDriverId CancelReason:cancelReason withCallBack:^(BOOL success, NSString *serverMessange) {
        if (success){
            SHOW_ALERT_WITH_SUCCESS(serverMessange);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.3 animations:^{
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [self dismissViewControllerAnimated:true completion:^{
                        [self.delegate didRejectCurrentTripWithController];
                    }];
                }];
            });
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessange);
        }
        STOP_HUD
    }];
}

-(void)alertControllerCancelButtonTapped{
    //Cancel button
}

#pragma mark - RunningRideViewControllerDelegate
-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo{
    if (DEBUG_MODE){
        NSLog(@"Selected Location : %@",locationInfo);
    }
    START_HUD
    [APP_DELEGATE.apiManager updateDestinationDetailsWithRideId:APP_DELEGATE.runningTripId destinationAddress:locationInfo[@"address"] destinationLattitude:locationInfo[@"lattitude"] destinationLongitude:locationInfo[@"longitude"] WithCallBack:^(BOOL success, NSString *serverMessage) {
        if (success){
            self.lableTripName.text = locationInfo[@"address"];
            double latitude = [locationInfo[@"lattitude"] doubleValue];
            double longitude = [locationInfo[@"longitude"] doubleValue];
            APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(latitude, longitude);
            [self loadTripEstimatedTimeAndDistance];
            SHOW_ALERT_WITH_SUCCESS(serverMessage)
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessage)
        }
        STOP_HUD
    }];
}

@end
