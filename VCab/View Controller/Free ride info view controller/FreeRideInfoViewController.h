//
//  FreeRideInfoViewController.h
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FreeRideInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableMessageTitle;

@property (weak, nonatomic) IBOutlet UILabel *lableMessageText;

@end
