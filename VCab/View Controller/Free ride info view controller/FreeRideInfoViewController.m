//
//  FreeRideInfoViewController.m
//  VCab
//
//  Created by Vishal Gohil on 23/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "FreeRideInfoViewController.h"

@interface FreeRideInfoViewController ()

@end

@implementation FreeRideInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    
    self.lableMessageText.text = @"Every time new VCab user signs up with your invite code, They'll get  $50 off each of their first 3 ride \n\n Once they take their first ride, you'll automatically get $50 off each of your next 3 rides \n\n Discount apply automatically in your country and expire 90 days from their issue date.";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTapAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
